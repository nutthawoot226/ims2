/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP780 {

    private String IMSRPIMC;
    private String IMSRPPERIOD;
    private String IMSRPORDERNO;
    private String GRPORDER;
    private String IMSRPPIMPNO;
    private String IMSRPIMPNO;
    private String IMSRPVENDEPT;
    private String IMSRPINVNO;
    private String IMSRPPROD;
    private String IMSRPINCOTERM;
    private String IMSRPCCY;
    private String IMSRPFAMT;
    private String IMSRPRATE;
    private String IMSRPAMT;
    private String IMSRPHANDERING;
    private String IMSRPINSUR;
    private String IMSRPFEE;
    private String IMSRPRENT;
    private String IMSRPCLEARING;
    private String IMSRPCASH;
    private String IMSRPVAT;
    private String IMSRPSUMEXPENSE;

    public IMSRP780() {

    }

    public String getGRPORDER() {
        return GRPORDER;
    }

    public void setGRPORDER(String GRPORDER) {
        this.GRPORDER = GRPORDER;
    }

    public String getIMSRPIMC() {
        return IMSRPIMC;
    }

    public void setIMSRPIMC(String IMSRPIMC) {
        this.IMSRPIMC = IMSRPIMC;
    }

    public String getIMSRPPERIOD() {
        return IMSRPPERIOD;
    }

    public void setIMSRPPERIOD(String IMSRPPERIOD) {
        this.IMSRPPERIOD = IMSRPPERIOD;
    }

    public String getIMSRPORDERNO() {
        return IMSRPORDERNO;
    }

    public void setIMSRPORDERNO(String IMSRPORDERNO) {
        this.IMSRPORDERNO = IMSRPORDERNO;
    }

    public String getIMSRPPIMPNO() {
        return IMSRPPIMPNO;
    }

    public void setIMSRPPIMPNO(String IMSRPPIMPNO) {
        this.IMSRPPIMPNO = IMSRPPIMPNO;
    }

    public String getIMSRPIMPNO() {
        return IMSRPIMPNO;
    }

    public void setIMSRPIMPNO(String IMSRPIMPNO) {
        this.IMSRPIMPNO = IMSRPIMPNO;
    }

    public String getIMSRPVENDEPT() {
        return IMSRPVENDEPT;
    }

    public void setIMSRPVENDEPT(String IMSRPVENDEPT) {
        this.IMSRPVENDEPT = IMSRPVENDEPT;
    }

    public String getIMSRPINVNO() {
        return IMSRPINVNO;
    }

    public void setIMSRPINVNO(String IMSRPINVNO) {
        this.IMSRPINVNO = IMSRPINVNO;
    }

    public String getIMSRPPROD() {
        return IMSRPPROD;
    }

    public void setIMSRPPROD(String IMSRPPROD) {
        this.IMSRPPROD = IMSRPPROD;
    }

    public String getIMSRPINCOTERM() {
        return IMSRPINCOTERM;
    }

    public void setIMSRPINCOTERM(String IMSRPINCOTERM) {
        this.IMSRPINCOTERM = IMSRPINCOTERM;
    }

    public String getIMSRPCCY() {
        return IMSRPCCY;
    }

    public void setIMSRPCCY(String IMSRPCCY) {
        this.IMSRPCCY = IMSRPCCY;
    }

    public String getIMSRPFAMT() {
        return IMSRPFAMT;
    }

    public void setIMSRPFAMT(String IMSRPFAMT) {
        this.IMSRPFAMT = IMSRPFAMT;
    }

    public String getIMSRPRATE() {
        return IMSRPRATE;
    }

    public void setIMSRPRATE(String IMSRPRATE) {
        this.IMSRPRATE = IMSRPRATE;
    }

    public String getIMSRPAMT() {
        return IMSRPAMT;
    }

    public void setIMSRPAMT(String IMSRPAMT) {
        this.IMSRPAMT = IMSRPAMT;
    }

    public String getIMSRPHANDERING() {
        return IMSRPHANDERING;
    }

    public void setIMSRPHANDERING(String IMSRPHANDERING) {
        this.IMSRPHANDERING = IMSRPHANDERING;
    }

    public String getIMSRPINSUR() {
        return IMSRPINSUR;
    }

    public void setIMSRPINSUR(String IMSRPINSUR) {
        this.IMSRPINSUR = IMSRPINSUR;
    }

    public String getIMSRPFEE() {
        return IMSRPFEE;
    }

    public void setIMSRPFEE(String IMSRPFEE) {
        this.IMSRPFEE = IMSRPFEE;
    }

    public String getIMSRPRENT() {
        return IMSRPRENT;
    }

    public void setIMSRPRENT(String IMSRPRENT) {
        this.IMSRPRENT = IMSRPRENT;
    }

    public String getIMSRPCLEARING() {
        return IMSRPCLEARING;
    }

    public void setIMSRPCLEARING(String IMSRPCLEARING) {
        this.IMSRPCLEARING = IMSRPCLEARING;
    }

    public String getIMSRPCASH() {
        return IMSRPCASH;
    }

    public void setIMSRPCASH(String IMSRPCASH) {
        this.IMSRPCASH = IMSRPCASH;
    }

    public String getIMSRPVAT() {
        return IMSRPVAT;
    }

    public void setIMSRPVAT(String IMSRPVAT) {
        this.IMSRPVAT = IMSRPVAT;
    }

    public String getIMSRPSUMEXPENSE() {
        return IMSRPSUMEXPENSE;
    }

    public void setIMSRPSUMEXPENSE(String IMSRPSUMEXPENSE) {
        this.IMSRPSUMEXPENSE = IMSRPSUMEXPENSE;
    }

}
