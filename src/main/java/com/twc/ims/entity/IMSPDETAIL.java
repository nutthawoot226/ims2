/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSPDETAIL {

    private String importNo;
    private String imp;
    private String line;
    private String invoiceNo;
    private String ccy;
    private String product;
    private String invAmount;
    private String foreignInvAmount;
    private String paidInvAmount;
    private String paidForeignInvAmount;
    private String sumPaidInvAmount;
    private String sumPaidForeignInvAmount;
    private String sts;
    private String paidDate;
    private String bank;
    private String vendor;
    private String fee;
    private String edt;
    private String cdt;
    private String user;

    public IMSPDETAIL() {

    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEdt() {
        return edt;
    }

    public void setEdt(String edt) {
        this.edt = edt;
    }

    public String getCdt() {
        return cdt;
    }

    public void setCdt(String cdt) {
        this.cdt = cdt;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getSts() {
        return sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public String getImp() {
        return imp;
    }

    public void setImp(String imp) {
        this.imp = imp;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getSumPaidInvAmount() {
        return sumPaidInvAmount;
    }

    public void setSumPaidInvAmount(String sumPaidInvAmount) {
        this.sumPaidInvAmount = sumPaidInvAmount;
    }

    public String getSumPaidForeignInvAmount() {
        return sumPaidForeignInvAmount;
    }

    public void setSumPaidForeignInvAmount(String sumPaidForeignInvAmount) {
        this.sumPaidForeignInvAmount = sumPaidForeignInvAmount;
    }

    public String getImportNo() {
        return importNo;
    }

    public void setImportNo(String importNo) {
        this.importNo = importNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getInvAmount() {
        return invAmount;
    }

    public void setInvAmount(String invAmount) {
        this.invAmount = invAmount;
    }

    public String getForeignInvAmount() {
        return foreignInvAmount;
    }

    public void setForeignInvAmount(String foreignInvAmount) {
        this.foreignInvAmount = foreignInvAmount;
    }

    public String getPaidInvAmount() {
        return paidInvAmount;
    }

    public void setPaidInvAmount(String paidInvAmount) {
        this.paidInvAmount = paidInvAmount;
    }

    public String getPaidForeignInvAmount() {
        return paidForeignInvAmount;
    }

    public void setPaidForeignInvAmount(String paidForeignInvAmount) {
        this.paidForeignInvAmount = paidForeignInvAmount;
    }

}
