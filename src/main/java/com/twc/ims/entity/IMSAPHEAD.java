/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSAPHEAD {

    private String paymentNo;
    private String shipCom;
    private String checkNo;
    private String bank;
    private String dnNo;
    private String dnDate;
    private String balamt;
    private String user;
    private List<IMSAPDETAIL> detailList;

    public IMSAPHEAD() {

    }

    public List<IMSAPDETAIL> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<IMSAPDETAIL> detailList) {
        this.detailList = detailList;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getShipCom() {
        return shipCom;
    }

    public void setShipCom(String shipCom) {
        this.shipCom = shipCom;
    }

    public String getCheckNo() {
        return checkNo;
    }

    public void setCheckNo(String checkNo) {
        this.checkNo = checkNo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getDnNo() {
        return dnNo;
    }

    public void setDnNo(String dnNo) {
        this.dnNo = dnNo;
    }

    public String getDnDate() {
        return dnDate;
    }

    public void setDnDate(String dnDate) {
        this.dnDate = dnDate;
    }

    public String getBalamt() {
        return balamt;
    }

    public void setBalamt(String balamt) {
        this.balamt = balamt;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
