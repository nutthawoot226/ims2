/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP760 {

    private String imc;
    private String ship;
    private String pdno;
    private String cqno;
    private String bank;
    private String cqdt;
    private String adv;
    private String est;
    private String bill;
    private String tax3;
    private String tax1;
    private String net;
    private String sumnet;
    private String rem;

    public IMSRP760() {

    }

    public String getImc() {
        return imc;
    }

    public void setImc(String imc) {
        this.imc = imc;
    }

    public String getShip() {
        return ship;
    }

    public void setShip(String ship) {
        this.ship = ship;
    }

    public String getPdno() {
        return pdno;
    }

    public void setPdno(String pdno) {
        this.pdno = pdno;
    }

    public String getCqno() {
        return cqno;
    }

    public void setCqno(String cqno) {
        this.cqno = cqno;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCqdt() {
        return cqdt;
    }

    public void setCqdt(String cqdt) {
        this.cqdt = cqdt;
    }

    public String getAdv() {
        return adv;
    }

    public void setAdv(String adv) {
        this.adv = adv;
    }

    public String getEst() {
        return est;
    }

    public void setEst(String est) {
        this.est = est;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getTax3() {
        return tax3;
    }

    public void setTax3(String tax3) {
        this.tax3 = tax3;
    }

    public String getTax1() {
        return tax1;
    }

    public void setTax1(String tax1) {
        this.tax1 = tax1;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getSumnet() {
        return sumnet;
    }

    public void setSumnet(String sumnet) {
        this.sumnet = sumnet;
    }

    public String getRem() {
        return rem;
    }

    public void setRem(String rem) {
        this.rem = rem;
    }

}
