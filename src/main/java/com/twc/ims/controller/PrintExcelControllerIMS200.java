/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP800Dao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP800;
import com.twc.ims.entity.IMSSHC;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author wien
 */
public class PrintExcelControllerIMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintExcelControllerIMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String advPaidNo = request.getParameter("advPaidNo");

        IMSAPDETAILDao dao5 = new IMSAPDETAILDao();
        List<IMSAPDETAIL> dataList = dao5.reportExcelIMS200(advPaidNo);

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Advance Import Payment");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 10001;

        if (!dataList.isEmpty()) {
            data.put(Integer.toString(ni++), new Object[]{
                dataList.get(0).getImcD()
//                dataList.get(0).getDateTime()
            });
            data.put(Integer.toString(ni++), new Object[]{
                "Advance Import. Paid No. " + dataList.get(0).getPimno()
            });
            data.put(Integer.toString(ni++), new Object[]{
                ""
            });
            data.put(Integer.toString(ni++), new Object[]{
                "NO",
                "Cheqe No.",
                "BANK",
                "Cheque Date",
                "Advance",
                "Estimate Advance",
                "D/N NO.",
                "D/N Date",
                "Invoice No.",
                "Bill Amount(Baht)",
                "Tax 3%",
                "Tax 1%",
                "Net Amount",
                "Sum Net Amount",
                "Remain Chq Amount Balance"
            });

            for (int i = 0; i < dataList.size(); i++) {
                data.put(Integer.toString(ni++), new Object[]{
                    dataList.get(i).getNo(),
                    dataList.get(i).getCheckNo(),
                    dataList.get(i).getBank(),
                    dataList.get(i).getCheckDate(),
                    dataList.get(i).getAdvance(),
                    dataList.get(i).getEstAdv(),
                    dataList.get(i).getDnNo(),
                    dataList.get(i).getDnDate(),
                    dataList.get(i).getInvNoList(),
                    dataList.get(i).getBillAmt(),
                    dataList.get(i).getTax3(),
                    dataList.get(i).getTax1(),
                    dataList.get(i).getAmount(),
                    dataList.get(i).getTotAdv(),
                    dataList.get(i).getBalance()
                });
            }

            data.put(Integer.toString(ni++), new Object[]{
                ""
            });

            data.put(Integer.toString(ni++), new Object[]{
                "",
                "Grand Total",
                "",
                "",
                dataList.get(0).getSumAdv(),
                dataList.get(0).getSumEstAdv(),
                "",
                "",
                "",
                dataList.get(0).getSumBillAmt(),
                dataList.get(0).getSumTax3(),
                dataList.get(0).getSumTax1(),
                dataList.get(0).getSumAmt(),
                dataList.get(0).getSumTotAdv(),
                dataList.get(0).getSumBal()
            });

            data.put(Integer.toString(ni++), new Object[]{
                ""
            });
        }

        //Set Column Width
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 3000);
        sheet.setColumnWidth(3, 3000);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 4000);
        sheet.setColumnWidth(6, 3000);
        sheet.setColumnWidth(7, 3000);
        sheet.setColumnWidth(8, 6000);
        sheet.setColumnWidth(9, 4000);
        sheet.setColumnWidth(10, 4000);
        sheet.setColumnWidth(11, 4000);
        sheet.setColumnWidth(12, 4000);
        sheet.setColumnWidth(13, 4000);
        sheet.setColumnWidth(14, 4000);

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 3));

        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);
        my_font.setFontHeight(12);
//
        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum <= 4) {

                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);

                    if (rownum == 4) {
                        style_tmp.setBorderBottom(BorderStyle.THIN);
                    }

                    style_tmp.setFont(my_font);
                    cell.setCellStyle(style_tmp);

                } else {

                    CellStyle style_tmp = workbook.createCellStyle();

                    if (cellnum == 1 || cellnum == 2 || cellnum == 3 || cellnum == 4 || cellnum == 7 || cellnum == 8) {
                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    } else if (cellnum == 9) {
                        style_tmp.setAlignment(HorizontalAlignment.LEFT);
                    } else {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    }

                    cell.setCellStyle(style_tmp);

                }

            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "Advance_Import_Payment_".toUpperCase() + advPaidNo + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
