/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSRP710Dao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSRP710;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS710 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS710() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("All Diff Amount(Import Payment)");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        data.put(Integer.toString(1001), new Object[]{"Pay Month", "Dept", "Bank", "Pay Date",
            "Ccy", "Pay Rate", "Imp Rate", "Sum of Amt Ccy", "Sum of Imp Baht", "Sum of Pay Baht", "Sum of Difference"});

        String year = ym.substring(0, 4);

        IMSRP710Dao daofind = new IMSRP710Dao();
        List<IMSRP710> rp710List = daofind.findForPrint(year, imc);

        Double FA = 0.00;
        Double IA = 0.00;
        Double PA = 0.00;
        Double DA = 0.00;

        Double FA_Dept = 0.00;
        Double IA_Dept = 0.00;
        Double PA_Dept = 0.00;
        Double DA_Dept = 0.00;

        Double FA_Bank = 0.00;
        Double IA_Bank = 0.00;
        Double PA_Bank = 0.00;
        Double DA_Bank = 0.00;

        Double FA_Ccy = 0.00;
        Double IA_Ccy = 0.00;
        Double PA_Ccy = 0.00;
        Double DA_Ccy = 0.00;

        Double allFA = 0.00;
        Double allIA = 0.00;
        Double allPA = 0.00;
        Double allDA = 0.00;

        int ni = 1002;
        for (int i = 0; i < rp710List.size(); i++) {
            allFA += Double.parseDouble(rp710List.get(i).getForAmt().replace(",", ""));
            allIA += Double.parseDouble(rp710List.get(i).getInvAmt().replace(",", ""));
            allPA += Double.parseDouble(rp710List.get(i).getPayAmt().replace(",", ""));
            allDA += Double.parseDouble(rp710List.get(i).getDiffAmt().replace(",", ""));

            FA += Double.parseDouble(rp710List.get(i).getForAmt().replace(",", ""));
            IA += Double.parseDouble(rp710List.get(i).getInvAmt().replace(",", ""));
            PA += Double.parseDouble(rp710List.get(i).getPayAmt().replace(",", ""));
            DA += Double.parseDouble(rp710List.get(i).getDiffAmt().replace(",", ""));

            FA_Dept += Double.parseDouble(rp710List.get(i).getForAmt().replace(",", ""));
            IA_Dept += Double.parseDouble(rp710List.get(i).getInvAmt().replace(",", ""));
            PA_Dept += Double.parseDouble(rp710List.get(i).getPayAmt().replace(",", ""));
            DA_Dept += Double.parseDouble(rp710List.get(i).getDiffAmt().replace(",", ""));

            FA_Bank += Double.parseDouble(rp710List.get(i).getForAmt().replace(",", ""));
            IA_Bank += Double.parseDouble(rp710List.get(i).getInvAmt().replace(",", ""));
            PA_Bank += Double.parseDouble(rp710List.get(i).getPayAmt().replace(",", ""));
            DA_Bank += Double.parseDouble(rp710List.get(i).getDiffAmt().replace(",", ""));

            FA_Ccy += Double.parseDouble(rp710List.get(i).getForAmt().replace(",", ""));
            IA_Ccy += Double.parseDouble(rp710List.get(i).getInvAmt().replace(",", ""));
            PA_Ccy += Double.parseDouble(rp710List.get(i).getPayAmt().replace(",", ""));
            DA_Ccy += Double.parseDouble(rp710List.get(i).getDiffAmt().replace(",", ""));

            if (i == 0) {
                data.put(Integer.toString(ni++), new Object[]{rp710List.get(i).getPeriod(),
                    rp710List.get(i).getDept(), rp710List.get(i).getBank(), rp710List.get(i).getPayDate(),
                    rp710List.get(i).getCcy(), rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                    rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                    rp710List.get(i).getDiffAmt()});

            } else {
                //no repeat Pay Month
                if (rp710List.get(i).getPeriod().equals(rp710List.get(i - 1).getPeriod())) {
                    //no repeat Dept
                    if (rp710List.get(i).getDept().equals(rp710List.get(i - 1).getDept())) {
                        //no repeat Bank
                        if (rp710List.get(i).getBank().equals(rp710List.get(i - 1).getBank())) {
                            //no repeat Pay Date
                            if (rp710List.get(i).getPayDate().equals(rp710List.get(i - 1).getPayDate())) {
                                //no repeat Ccy
                                if (rp710List.get(i).getCcy().equals(rp710List.get(i - 1).getCcy())) {
                                    //no repeat Pay Rate
                                    if (rp710List.get(i).getPayRate().equals(rp710List.get(i - 1).getPayRate())) {
                                        //no repeat Imp Rate
                                        if (rp710List.get(i).getImpRate().equals(rp710List.get(i - 1).getImpRate())) {
                                            data.put(Integer.toString(ni++), new Object[]{"",
                                                "", "", "",
                                                "", "", "",
                                                rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                                                rp710List.get(i).getDiffAmt()});
                                        } else {
                                            data.put(Integer.toString(ni++), new Object[]{"",
                                                "", "", "",
                                                "", "", rp710List.get(i).getImpRate(),
                                                rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                                                rp710List.get(i).getDiffAmt()});
                                        }
                                    } else {
                                        data.put(Integer.toString(ni++), new Object[]{"",
                                            "", "", "",
                                            "", rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                                            rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                                            rp710List.get(i).getDiffAmt()});
                                    }
                                } else {
                                    data.put(Integer.toString(ni++), new Object[]{"",
                                        "", "", "",
                                        rp710List.get(i).getCcy(), rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                                        rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                                        rp710List.get(i).getDiffAmt()});
                                }
                            } else {
                                data.put(Integer.toString(ni++), new Object[]{"",
                                    "", "", rp710List.get(i).getPayDate(),
                                    rp710List.get(i).getCcy(), rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                                    rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                                    rp710List.get(i).getDiffAmt()});
                            }

                        } else {
                            data.put(Integer.toString(ni++), new Object[]{"",
                                "", rp710List.get(i).getBank(), rp710List.get(i).getPayDate(),
                                rp710List.get(i).getCcy(), rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                                rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                                rp710List.get(i).getDiffAmt()});

                        }
                    } else {
                        data.put(Integer.toString(ni++), new Object[]{"",
                            rp710List.get(i).getDept(), rp710List.get(i).getBank(), rp710List.get(i).getPayDate(),
                            rp710List.get(i).getCcy(), rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                            rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                            rp710List.get(i).getDiffAmt()});

                    }

                } else {
                    data.put(Integer.toString(ni++), new Object[]{rp710List.get(i).getPeriod(),
                        rp710List.get(i).getDept(), rp710List.get(i).getBank(), rp710List.get(i).getPayDate(),
                        rp710List.get(i).getCcy(), rp710List.get(i).getPayRate(), rp710List.get(i).getImpRate(),
                        rp710List.get(i).getForAmt(), rp710List.get(i).getInvAmt(), rp710List.get(i).getPayAmt(),
                        rp710List.get(i).getDiffAmt()});

                }
            }

            //******
            if (i != rp710List.size() - 1) {
                //Ccy
                if (!rp710List.get(i).getCcy().equals(rp710List.get(i + 1).getCcy())) {
                    data.put(Integer.toString(ni++), new Object[]{"",
                        "", "", "", rp710List.get(i).getCcy() + " Total", "Total", "Total",
                        "Total-" + formatDou.format(FA_Ccy), "Total-" + formatDou.format(IA_Ccy),
                        "Total-" + formatDou.format(PA_Ccy), "Total-" + formatDou.format(DA_Ccy)});

                    FA_Ccy = 0.00;
                    IA_Ccy = 0.00;
                    PA_Ccy = 0.00;
                    DA_Ccy = 0.00;
                } else {
                    //Bank
                    if (!rp710List.get(i).getBank().equals(rp710List.get(i + 1).getBank())) {
                        data.put(Integer.toString(ni++), new Object[]{"",
                            "", "", "", rp710List.get(i).getCcy() + " Total", "Total", "Total",
                            "Total-" + formatDou.format(FA_Ccy), "Total-" + formatDou.format(IA_Ccy),
                            "Total-" + formatDou.format(PA_Ccy), "Total-" + formatDou.format(DA_Ccy)});

                        FA_Ccy = 0.00;
                        IA_Ccy = 0.00;
                        PA_Ccy = 0.00;
                        DA_Ccy = 0.00;
                    } else {
                        //Dept
                        if (!rp710List.get(i).getDept().equals(rp710List.get(i + 1).getDept())) {
                            data.put(Integer.toString(ni++), new Object[]{"",
                                "", "", "", rp710List.get(i).getCcy() + " Total", "Total", "Total",
                                "Total-" + formatDou.format(FA_Ccy), "Total-" + formatDou.format(IA_Ccy),
                                "Total-" + formatDou.format(PA_Ccy), "Total-" + formatDou.format(DA_Ccy)});

                            FA_Ccy = 0.00;
                            IA_Ccy = 0.00;
                            PA_Ccy = 0.00;
                            DA_Ccy = 0.00;
                        } else {
                            //Pay Month
                            if (!rp710List.get(i).getPeriod().equals(rp710List.get(i + 1).getPeriod())) {
                                data.put(Integer.toString(ni++), new Object[]{"",
                                    "", "", "", rp710List.get(i).getCcy() + " Total", "Total", "Total",
                                    "Total-" + formatDou.format(FA_Ccy), "Total-" + formatDou.format(IA_Ccy),
                                    "Total-" + formatDou.format(PA_Ccy), "Total-" + formatDou.format(DA_Ccy)});

                                FA_Ccy = 0.00;
                                IA_Ccy = 0.00;
                                PA_Ccy = 0.00;
                                DA_Ccy = 0.00;
                            }
                        }
                    }
                }

                //Bank
                if (!rp710List.get(i).getBank().equals(rp710List.get(i + 1).getBank())) {
                    data.put(Integer.toString(ni++), new Object[]{"",
                        "", rp710List.get(i).getBank() + " Total", "Total", "Total", "Total", "Total",
                        "Total-" + formatDou.format(FA_Bank), "Total-" + formatDou.format(IA_Bank),
                        "Total-" + formatDou.format(PA_Bank), "Total-" + formatDou.format(DA_Bank)});

                    FA_Bank = 0.00;
                    IA_Bank = 0.00;
                    PA_Bank = 0.00;
                    DA_Bank = 0.00;
                } else {
                    //Dept
                    if (!rp710List.get(i).getDept().equals(rp710List.get(i + 1).getDept())) {
                        data.put(Integer.toString(ni++), new Object[]{"",
                            "", rp710List.get(i).getBank() + " Total", "Total", "Total", "Total", "Total",
                            "Total-" + formatDou.format(FA_Bank), "Total-" + formatDou.format(IA_Bank),
                            "Total-" + formatDou.format(PA_Bank), "Total-" + formatDou.format(DA_Bank)});

                        FA_Bank = 0.00;
                        IA_Bank = 0.00;
                        PA_Bank = 0.00;
                        DA_Bank = 0.00;
                    } else {
                        //Pay Month
                        if (!rp710List.get(i).getPeriod().equals(rp710List.get(i + 1).getPeriod())) {
                            data.put(Integer.toString(ni++), new Object[]{"",
                                "", rp710List.get(i).getBank() + " Total", "Total", "Total", "Total", "Total",
                                "Total-" + formatDou.format(FA_Bank), "Total-" + formatDou.format(IA_Bank),
                                "Total-" + formatDou.format(PA_Bank), "Total-" + formatDou.format(DA_Bank)});

                            FA_Bank = 0.00;
                            IA_Bank = 0.00;
                            PA_Bank = 0.00;
                            DA_Bank = 0.00;
                        }
                    }
                }

                //Dept
                if (!rp710List.get(i).getDept().equals(rp710List.get(i + 1).getDept())) {
                    data.put(Integer.toString(ni++), new Object[]{"",
                        rp710List.get(i).getDept() + " Total", "Total", "Total", "Total", "Total", "Total",
                        "Total-" + formatDou.format(FA_Dept), "Total-" + formatDou.format(IA_Dept),
                        "Total-" + formatDou.format(PA_Dept), "Total-" + formatDou.format(DA_Dept)});

                    FA_Dept = 0.00;
                    IA_Dept = 0.00;
                    PA_Dept = 0.00;
                    DA_Dept = 0.00;
                } else {
                    //Pay Month
                    if (!rp710List.get(i).getPeriod().equals(rp710List.get(i + 1).getPeriod())) {
                        data.put(Integer.toString(ni++), new Object[]{"",
                            rp710List.get(i).getDept() + " Total", "Total", "Total", "Total", "Total", "Total",
                            "Total-" + formatDou.format(FA_Dept), "Total-" + formatDou.format(IA_Dept),
                            "Total-" + formatDou.format(PA_Dept), "Total-" + formatDou.format(DA_Dept)});

                        FA_Dept = 0.00;
                        IA_Dept = 0.00;
                        PA_Dept = 0.00;
                        DA_Dept = 0.00;
                    }
                }

                //Pay Month
                if (!rp710List.get(i).getPeriod().equals(rp710List.get(i + 1).getPeriod())) {
                    data.put(Integer.toString(ni++), new Object[]{rp710List.get(i).getPeriod() + " Total",
                        "Total", "Total", "Total", "Total", "Total", "Total",
                        "Total-" + formatDou.format(FA), "Total-" + formatDou.format(IA),
                        "Total-" + formatDou.format(PA), "Total-" + formatDou.format(DA)});

                    FA = 0.00;
                    IA = 0.00;
                    PA = 0.00;
                    DA = 0.00;
                }

            } else {
                //Ccy
                data.put(Integer.toString(ni++), new Object[]{"",
                    "", "", rp710List.get(i).getCcy() + " Total", "Total", "Total", "Total",
                    "Total-" + formatDou.format(FA_Ccy), "Total-" + formatDou.format(IA_Ccy),
                    "Total-" + formatDou.format(PA_Ccy), "Total-" + formatDou.format(DA_Ccy)});

                FA_Ccy = 0.00;
                IA_Ccy = 0.00;
                PA_Ccy = 0.00;
                DA_Ccy = 0.00;

                //Bank
                data.put(Integer.toString(ni++), new Object[]{"",
                    "", rp710List.get(i).getBank() + " Total", "Total", "Total", "Total", "Total",
                    "Total-" + formatDou.format(FA_Bank), "Total-" + formatDou.format(IA_Bank),
                    "Total-" + formatDou.format(PA_Bank), "Total-" + formatDou.format(DA_Bank)});

                FA_Bank = 0.00;
                IA_Bank = 0.00;
                PA_Bank = 0.00;
                DA_Bank = 0.00;

                //Dept
                data.put(Integer.toString(ni++), new Object[]{"",
                    rp710List.get(i).getDept() + " Total", "Total", "Total", "Total", "Total", "Total",
                    "Total-" + formatDou.format(FA_Dept), "Total-" + formatDou.format(IA_Dept),
                    "Total-" + formatDou.format(PA_Dept), "Total-" + formatDou.format(DA_Dept)});

                FA_Dept = 0.00;
                IA_Dept = 0.00;
                PA_Dept = 0.00;
                DA_Dept = 0.00;

                //Pay Month
                data.put(Integer.toString(ni++), new Object[]{rp710List.get(i).getPeriod() + " Total",
                    "Total", "Total", "Total", "Total", "Total", "Total",
                    "Total-" + formatDou.format(FA), "Total-" + formatDou.format(IA),
                    "Total-" + formatDou.format(PA), "Total-" + formatDou.format(DA)});

                FA = 0.00;
                IA = 0.00;
                PA = 0.00;
                DA = 0.00;

            }

        }

        data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "", "",
            "Total-" + formatDou.format(allFA), "Total-" + formatDou.format(allIA),
            "Total-" + formatDou.format(allPA), "Total-" + formatDou.format(allDA)});

        data.put(Integer.toString(ni++), new Object[]{"GT", "GT", "GT", "GT", "GT",
            "GT", "GT", "GT", "GT", "GT", "GT"});

        String mt = ym.substring(4);
        String ml = "01";
        if (mt.equals(ml)) {
            ml = "M.1";
        } else {
            ml = "M.1-M." + Integer.parseInt(mt);
        }

        IMSRP710Dao daofind2 = new IMSRP710Dao();
        List<IMSRP710> rp710List2 = daofind2.findForPrint2(year, imc);

        for (int i = 0; i < rp710List2.size(); i++) {
            if (i == 0) {
                data.put(Integer.toString(ni++), new Object[]{"Grand Total", rp710List2.get(i).getPeriod(), "", "",
                    rp710List2.get(i).getCcy() + " Total",
                    "", "", rp710List2.get(i).getForAmt(), rp710List2.get(i).getInvAmt(),
                    rp710List2.get(i).getPayAmt(), rp710List2.get(i).getDiffAmt()});
            } else {
                data.put(Integer.toString(ni++), new Object[]{"", "", "", "", rp710List2.get(i).getCcy() + " Total",
                    "", "", rp710List2.get(i).getForAmt(), rp710List2.get(i).getInvAmt(),
                    rp710List2.get(i).getPayAmt(), rp710List2.get(i).getDiffAmt()});
            }
        }

        data.put(Integer.toString(ni++), new Object[]{"EGT", "EGT", "EGT", "EGT", "EGT",
            "EGT", "EGT", "EGT", "EGT", "EGT", "EGT"});

        //Set Column Width
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(1, 2500);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 3500);
        sheet.setColumnWidth(4, 2500);
        sheet.setColumnWidth(5, 3500);
        sheet.setColumnWidth(6, 3500);
        sheet.setColumnWidth(7, 5000);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);
        sheet.setColumnWidth(10, 5000);

        CellStyle styleF = workbook.createCellStyle();
        styleF.setAlignment(HorizontalAlignment.CENTER);
        styleF.setBorderRight(BorderStyle.THIN);
        styleF.setBorderLeft(BorderStyle.THIN);
//        styleF.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        styleF.setFillPattern(FillPatternType.LESS_DOTS);

        CellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setBorderBottom(BorderStyle.THICK);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
//        style.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        style.setFillPattern(FillPatternType.BIG_SPOTS);

        // Styling border and alignment of cell.  
        CellStyle style_ct = workbook.createCellStyle();
        style_ct.setAlignment(HorizontalAlignment.CENTER);

        // Styling border and alignment of cell.
        CellStyle style_rt = workbook.createCellStyle();
        style_rt.setAlignment(HorizontalAlignment.RIGHT);

        // Styling border and alignment of cell.  
        CellStyle style_bd = workbook.createCellStyle();
        style_bd.setBorderBottom(BorderStyle.THIN);
        style_bd.setBorderTop(BorderStyle.THIN);
        style_bd.setBorderRight(BorderStyle.THIN);
        style_bd.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_u = workbook.createCellStyle();
        style_bd_u.setBorderBottom(BorderStyle.THICK);
        style_bd_u.setBorderTop(BorderStyle.THIN);
        style_bd_u.setBorderRight(BorderStyle.THIN);
        style_bd_u.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_ct = workbook.createCellStyle();
        style_bd_ct.setAlignment(HorizontalAlignment.CENTER);
        style_bd_ct.setBorderBottom(BorderStyle.THIN);
        style_bd_ct.setBorderTop(BorderStyle.THIN);
        style_bd_ct.setBorderRight(BorderStyle.THIN);
        style_bd_ct.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_rt = workbook.createCellStyle();
        style_bd_rt.setAlignment(HorizontalAlignment.RIGHT);
        style_bd_rt.setBorderBottom(BorderStyle.THIN);
        style_bd_rt.setBorderTop(BorderStyle.THIN);
        style_bd_rt.setBorderRight(BorderStyle.THIN);
        style_bd_rt.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_rt_u = workbook.createCellStyle();
        style_bd_rt_u.setAlignment(HorizontalAlignment.RIGHT);
        style_bd_rt_u.setBorderBottom(BorderStyle.THICK);
        style_bd_rt_u.setBorderTop(BorderStyle.THIN);
        style_bd_rt_u.setBorderRight(BorderStyle.THIN);
        style_bd_rt_u.setBorderLeft(BorderStyle.THIN);
//        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
//        style_bd_rt_u.setFont(my_font);

        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);

// Styling border and alignment of cell.  
        CellStyle style_u_ct = workbook.createCellStyle();
        style_u_ct.setBorderBottom(BorderStyle.THICK);
        style_u_ct.setAlignment(HorizontalAlignment.CENTER);
        style_u_ct.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_u_ct2 = workbook.createCellStyle();
        style_u_ct2.setBorderTop(BorderStyle.THICK);
        style_u_ct2.setAlignment(HorizontalAlignment.CENTER);
        style_u_ct2.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_u_b = workbook.createCellStyle();
        style_u_b.setBorderBottom(BorderStyle.THIN);
        style_u_b.setAlignment(HorizontalAlignment.LEFT);
        style_u_b.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_u_rt = workbook.createCellStyle();
        style_u_rt.setBorderBottom(BorderStyle.THIN);
        style_u_rt.setAlignment(HorizontalAlignment.RIGHT);
//        style_u_rt.setFont(my_font);
//
        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 1) {
                    cell.setCellStyle(style_u_ct);
                } else {
                    if (cellnum >= 6 && cellnum <= 11) {
                        cell.setCellStyle(style_rt);
                        if (cellnum == 6 || cellnum == 7) {
                            if (cell.getStringCellValue().trim().equals("Total")) {
                                cell.setCellValue("");
                                cell.setCellStyle(style_u_rt);
                            }
                        } else {
                            if (cell.getStringCellValue().contains("Total")) {
                                cell.setCellStyle(style_u_rt);
                                cell.setCellValue(cell.getStringCellValue().replace("Total-", ""));
                            }
                        }

                        if (cell.getStringCellValue().trim().equals("GT")) {
                            cell.setCellValue("");
                            cell.setCellStyle(style_u_ct);
                        } else if (cell.getStringCellValue().trim().equals("EGT")) {
                            cell.setCellValue("");
                            cell.setCellStyle(style_u_ct2);
                        }
                    } else {
                        if (cellnum >= 1 && cellnum <= 5) {
                            if (cell.getStringCellValue().contains("Total")) {
                                cell.setCellStyle(style_u_b);
                                if (cell.getStringCellValue().trim().equals("Total")) {
                                    cell.setCellValue("");
                                }
                            }
                        }

                        if (cell.getStringCellValue().trim().equals("GT")) {
                            cell.setCellValue("");
                            cell.setCellStyle(style_u_ct);
                        } else if (cell.getStringCellValue().trim().equals("EGT")) {
                            cell.setCellValue("");
                            cell.setCellStyle(style_u_ct2);
                        }
                    }
                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "ALL_DIFF_AMOUNT(IMPORT_PAYMENT)_" + year + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS710/edit?ym=" + ym + "&imc=" + imc);
    }

}
