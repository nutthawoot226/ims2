/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteControllerIMS110 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS110.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public DeleteControllerIMS110() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String[] id = request.getParameterValues("selectCk");
        String advImpNo = request.getParameter("advImpNo");
        String dateF = request.getParameter("dateF");
        String dateT = request.getParameter("dateT");
        String shipcom = request.getParameter("shipCom");
        String checkNo = request.getParameter("checkNo");
        List<String> toDeleteList = new ArrayList<String>();
        StringBuilder allpimno = new StringBuilder();

        if (id != null) {
            for (String id1 : id) {
                if (id1.contains("INDB")) {
                    IMSAPDETAILDao daoCNT = new IMSAPDETAILDao();
                    int CNT = daoCNT.findCNT(checkNo, id1.split("-")[1]);

                    if (CNT <= 0) {
                        toDeleteList.add(id1.split("-")[1]);

                        IMSADETAILDao daoinv = new IMSADETAILDao();
                        String INV = daoinv.findInvNoList(advImpNo, id1.split("-")[1]);
                        IMSADETAILDao daoinv1 = new IMSADETAILDao();
                        String PIMNO = daoinv1.findPimNo(advImpNo, id1.split("-")[1]);

                        if (INV != null) {
                            String[] invL = INV.split(",");
                            for (String inv : invL) {
                                IMSSDETAILDao daosts = new IMSSDETAILDao();
                                daosts.setStatus("IMSSDSTS110", PIMNO, inv.trim(), "null");
                            }
                        }
                    }
                } else {
                    allpimno.append(",").append(id1);
                }
            }
        }

        for (int i = 0; i < toDeleteList.size(); i++) {
            IMSADETAILDao daodel = new IMSADETAILDao();

            IMSAPDETAILDao daoCNT = new IMSAPDETAILDao();
            int CNT = daoCNT.findCNT(checkNo, toDeleteList.get(i));

            if (CNT <= 0) {
                if (daodel.delete(advImpNo, toDeleteList.get(i))) {
                    IMSSHEADDao daohd = new IMSSHEADDao();
                    daohd.updateAdvIMS110RETURN(advImpNo);
                }
            }
        }

        request.setAttribute("PROGRAMNAME", "IMS110/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Setting. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        String[] pImpNo = request.getParameterValues("pImpNo");

        request.setAttribute("cur", advImpNo);

        IMSAHEADDao daoh = new IMSAHEADDao();
        IMSAHEAD head = daoh.findByCode(advImpNo);

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(shipcom);

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());
        request.setAttribute("dateF", dateF);
        request.setAttribute("dateT", dateT);
        request.setAttribute("checkDate", head.getCheckDate());

        IMSADETAILDao dao3 = new IMSADETAILDao();
        List<IMSSDETAIL> detList = dao3.findByCode(advImpNo);
        request.setAttribute("detList", detList);

        String[] pImpNo2 = new String[detList.size()];

        for (int i = 0; i < detList.size(); i++) {
            pImpNo2[i] = detList.get(i).getpImpNo();
        }

        IMSADETAILDao daoMax = new IMSADETAILDao();
        int line = daoMax.findNextLine(advImpNo);

        String tt = "";
        tt += allpimno;
        if (!tt.equals("")) {
            tt = allpimno.substring(1);
        }

        IMSSHEADDao dao4 = new IMSSHEADDao();
        List<IMSSDETAIL> detList2 = dao4.findAllPIMNO(line, dateF, dateT, shipcom, pImpNo2, tt);
        request.setAttribute("detList2", detList2);

        if (!detList.isEmpty() || !detList2.isEmpty()) {

//            IMSAPDETAILDao daoCNT = new IMSAPDETAILDao();
//            int CNT = daoCNT.findCNT(head.getCheckNo());
            IMSAPDETAILDao daoAPN = new IMSAPDETAILDao();
            String APN = daoAPN.findAPN(head.getCheckNo());

//            request.setAttribute("CNT", CNT);
            request.setAttribute("APN", APN);

            request.setAttribute("checkNo", head.getCheckNo());
            request.setAttribute("bnkCode", head.getBnkCode());

            IMSBAMDao dao6 = new IMSBAMDao();
            List<IMSSHC> bankList = dao6.findAll();

            IMSBAMDao dao5 = new IMSBAMDao();
            IMSSHC bank = dao5.findByCode(head.getBnkCode());

            String smol = "";
//            if (CNT > 0) {
//                smol = "onclick=\"document.getElementById('myModal-7').style.display = 'block';\"";
//                request.setAttribute("isIn", "<a onclick=\"document.getElementById('myModal-7').style.display = 'block';\"><i style=\" font-size: 30px;\" class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>");
//            } else {
            smol = "onclick=\"document.getElementById('myModal-3').style.display = 'block';\"";
            request.setAttribute("isIn", "<a onclick=\"checkDelete();\"><i style=\" font-size: 30px;\" class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>");

//            }
//            String opt = "<b>CHEQUE DATE</b>\n"
//                    + "<input type=\"date\" id=\"checkDateTmp\" name=\"checkDateTmp\" value=\"" + head.getCheckDate() + "\" onchange=\"document.getElementById('checkDate').value = this.value;\" style=\" height: 30px; width: 150px;\">";
            String totAmt = "<b>TOTAL AMOUNT</b>&nbsp;&nbsp;:&nbsp;&nbsp;<b id=\"finalTotalAmt\"></b>"
                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "<a class=\"btn btn-success\" "
                    + smol
                    + "><i class=\"fa fa-save\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Save</a>";

            StringBuilder cb = new StringBuilder();
            cb.append("<br>\n"
                    + "<b>CHEQUE DATE</b>\n"
                    + "<input type=\"date\" id=\"checkDateTmp\" name=\"checkDateTmp\" value=\"" + head.getCheckDate() + "\" onchange=\"document.getElementById('checkDate').value = this.value;\" style=\" height: 30px; width: 150px;\">"
                    + "                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n"
                    + "                            <b>CHEQUE NO.</b>\n"
                    + "                            <input type=\"text\" id=\"checkNotmp\" name=\"checkNotmp\" value=\"" + head.getCheckNo() + "\" onchange=\"document.getElementById('checkNo').value = this.value;\" style=\" height: 30px; width: 150px;\">\n"
                    + "                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n"
                    + "                            <b>BANK CODE</b>\n"
                    + "                            <select id=\"bnkCodetmp\" name=\"bnkCodetmp\" style=\" height: 30px; width: 450px;\" onchange=\"document.getElementById('bnkCode').value = this.value;\">\n"
                    + "                                <option value=\"" + bank.getCode() + "\" selected hidden>" + bank.getCode() + " : " + bank.getName() + " (" + bank.getNamet() + ")</option>");

            for (int i = 0; i < bankList.size(); i++) {
                cb.append("<option value=\"").append(bankList.get(i).getCode()).append("\">").append(bankList.get(i).getCode()).append(" : ").append(bankList.get(i).getName() == null ? "" : bankList.get(i).getName()).append(" (").append(bankList.get(i).getNamet() == null ? "" : bankList.get(i).getNamet()).append(")</option>");
            }
            cb.append("</select>");

//            request.setAttribute("detListSizeNotZero", opt);
            request.setAttribute("totalAmt", totAmt);
            request.setAttribute("cb", cb);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

}
