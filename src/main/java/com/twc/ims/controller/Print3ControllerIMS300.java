/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSINSDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPVDISDao;
import com.twc.ims.entity.IMSAHEAD;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;

/**
 *
 * @author wien
 */
public class Print3ControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/pdisplayIMS300_DIS.jsp";
    private static final String FILE_DEST = "/report/";

    public Print3ControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String impPaidNo = request.getParameter("impPaidNo");
        String[] famt = request.getParameterValues("famt");
        String[] rate = request.getParameterValues("rate");
        String[] amt = request.getParameterValues("amt");
        String userid = request.getParameter("userid");

        new IMSPVDISDao().delete(impPaidNo);

        if (famt != null) {

            for (int i = 0; i < famt.length; i++) {
                String famtA = famt[i].replace(",", "");
                String rateA = rate[i].replace(",", "");
                String amtA = amt[i].replace(",", "");

                if (famtA.trim().equals("")) {
                    famtA = "0";
                }
                if (rateA.trim().equals("")) {
                    rateA = "0";
                }
                if (amtA.trim().equals("")) {
                    amtA = "0";
                }

                new IMSPVDISDao().add(impPaidNo, Integer.toString(i + 1), famtA, rateA, amtA, userid);
            }
        }

        try {
            IMSPDETAILDao dao5 = new IMSPDETAILDao();
            ResultSet result = dao5.reportIMS300PVDIS(impPaidNo);

            IMSPDETAILDao daoIMC = new IMSPDETAILDao();
            String imc = daoIMC.findIMC(impPaidNo);

            String jspath = "/resources/jasper/IMS300_PV_DIS.jasper";
            if (imc.trim().equals("R")) {
                jspath = "/resources/jasper/IMS300_PV_TORA_DIS.jasper";
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath(jspath));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS300_PV_DIS.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        request.setAttribute("PROGRAMNAME", "IMS300/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
