/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSFFCDao;
import com.twc.ims.dao.IMSGRPDao;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerIMS012 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS012.jsp";

    public EditControllerIMS012() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS012/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Freight Forwarder Company. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String code = request.getParameter("code");
        IMSFFCDao dao = new IMSFFCDao();
        IMSSHC p = dao.findByCode(code);

        request.setAttribute("code", p.getCode());
        request.setAttribute("desc", p.getName());
        request.setAttribute("desc2", p.getNamet());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String code = request.getParameter("code");
        String desc = request.getParameter("desc");
        String desc2 = request.getParameter("desc2");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "IMS012/E");
        request.setAttribute("PROGRAMDESC", "Freight Forwarder Company. Display");

        IMSFFCDao dao = new IMSFFCDao();

        dao.edit(code, desc, desc2, userid);

        response.setHeader("Refresh", "0;/IMS2/IMS012/display");

    }
}
