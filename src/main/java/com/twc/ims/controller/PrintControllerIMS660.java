/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSFRGDao;
import com.twc.ims.entity.IMSAHEAD;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;

/**
 *
 * @author wien
 */
public class PrintControllerIMS660 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/pdisplayIMS660.jsp";
    private static final String FILE_DEST = "/report/";

    public PrintControllerIMS660() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

        try {
            IMSFRGDao dao5 = new IMSFRGDao();
            ResultSet result = dao5.reportIMS660D(ym, imc);

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/IMS660_Detail.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS660_Detail_" + ym + "_" + imc + ".pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
//         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        try {
            IMSFRGDao dao5 = new IMSFRGDao();
            ResultSet result = dao5.reportIMS660JV(ym, imc);

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/IMS660_JV_TORA.jasper"));
            byte[] bytes;
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS660_JournalVoucher_" + ym + "_" + imc + ".pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        request.setAttribute("PROGRAMNAME", "IMS660/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Freight Invoice Summary Report");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("fname", "_" + ym + "_" + imc);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
