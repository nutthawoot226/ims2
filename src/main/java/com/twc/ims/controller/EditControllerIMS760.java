/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP760Dao;
import com.twc.ims.entity.IMSRP760;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerIMS760 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS760.jsp";

    public EditControllerIMS760() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS760/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Balance Advance Import Amount. Report");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String imc = request.getParameter("imc");
        String ym = request.getParameter("ym");
        request.setAttribute("imc", imc);
        request.setAttribute("ym", ym);

        IMSSHC imcOP = new IMSIMCDao().findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        List<IMSRP760> rp760List = new IMSRP760Dao().findByCode(imc, ym);
        request.setAttribute("rp760List", rp760List);
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
