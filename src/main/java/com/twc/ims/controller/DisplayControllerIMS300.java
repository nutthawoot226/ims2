/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSPHEAD;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayIMS300.jsp";

    public DisplayControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS300");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        IMSPHEADDao dao = new IMSPHEADDao();
        List<IMSPHEAD> aheadList = dao.findAll();

        for (int i = 0; i < aheadList.size(); i++) {
            IMSPDETAILDao dao3 = new IMSPDETAILDao();
            List<IMSPDETAIL> detList = dao3.findByCode(aheadList.get(i).getImpPaidNo());

            aheadList.get(i).setDetailList(detList);
        }

        request.setAttribute("aheadList", aheadList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
