/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSRP720Dao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP740Dao;
import com.twc.ims.entity.IMSRP720;
import com.twc.ims.entity.IMSRP740;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS720 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS720() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Expense Report");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        int ni = 101;

        data.put(Integer.toString(ni++), new Object[]{"PRODUCT", "CCY", "Month", "AMOUNT(CCY)",
            "Amount(Baht)", "Freight", "IMPORT DUTY", "VAT", "TOTAL(-)VAT", "TOTAL(-)VAT(-)FREIGHT", "EXPENSE %"});

        IMSRP720Dao daofind = new IMSRP720Dao();
        List<IMSRP720> rp720List = daofind.findByCodePT(ym, imc);

        for (int i = 0; i < rp720List.size(); i++) {
            data.put(Integer.toString(ni++), new Object[]{rp720List.get(i).getProd(),
                rp720List.get(i).getCcy(), rp720List.get(i).getYam(), rp720List.get(i).getForAmt(),
                rp720List.get(i).getInvAmt(), rp720List.get(i).getFreight(), rp720List.get(i).getImpDuty(),
                rp720List.get(i).getVatAmt(), rp720List.get(i).getTotAmtEV(), rp720List.get(i).getTotAmtEVF(),
                rp720List.get(i).getExp()});
        }

        //Set Column Width
        sheet.setColumnWidth(0, 3500);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 2000);
        sheet.setColumnWidth(3, 5000);
        sheet.setColumnWidth(4, 5000);
        sheet.setColumnWidth(5, 5000);
        sheet.setColumnWidth(6, 5000);
        sheet.setColumnWidth(7, 5000);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);
        sheet.setColumnWidth(10, 5000);

        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 1) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    style_tmp.setBorderBottom(BorderStyle.THIN);
                    style_tmp.setBorderTop(BorderStyle.THIN);
                    style_tmp.setBorderRight(BorderStyle.THIN);
                    style_tmp.setBorderLeft(BorderStyle.THIN);
                    style_tmp.setFont(my_font);
                    cell.setCellStyle(style_tmp);

                } else {
                    CellStyle style_tmp = workbook.createCellStyle();
                    if (cellnum >= 4) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", ""));
                            style_tmp.setBorderBottom(BorderStyle.THIN);
                        }
                    } else {
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", ""));
                            style_tmp.setBorderBottom(BorderStyle.THIN);
                        }
                    }
                    cell.setCellStyle(style_tmp);

                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "SUMMARY_EXPENSE_REPORT_" + ym + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS720/edit?ym=" + ym + "&imc=" + imc);
    }

}
