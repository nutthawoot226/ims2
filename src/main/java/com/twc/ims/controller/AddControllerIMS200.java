/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class AddControllerIMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public AddControllerIMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String advPaidNo = request.getParameter("advPaidNo");
        String shipCom = request.getParameter("shipCom");
        String[] pimno = request.getParameterValues("pimno");
        String[] no = request.getParameterValues("no");
        String[] checkNo = request.getParameterValues("checkNoData");
        String[] bank = request.getParameterValues("bank");
        String[] checkDate = request.getParameterValues("checkDate");
        String[] advance = request.getParameterValues("advance");
        String[] estAdv = request.getParameterValues("estAdv");
        String[] dnNo = request.getParameterValues("dnNo");
        String[] dnDate = request.getParameterValues("dnDate");
        String[] imcD = request.getParameterValues("imcD");
        String[] invNoList = request.getParameterValues("invNoList");
        String[] billAmt = request.getParameterValues("billAmt");
        String[] tax3 = request.getParameterValues("tax3");
        String[] tax1 = request.getParameterValues("tax1");
        String[] amount = request.getParameterValues("amount");
        String[] totAdv = request.getParameterValues("totAdv");
        String[] balance = request.getParameterValues("balance");

        String userid = request.getParameter("userid");

        IMSAPHEADDao daock = new IMSAPHEADDao();
        if (daock.check(advPaidNo).equals("t")) {
            IMSAPHEADDao dao = new IMSAPHEADDao();
            if (dao.add(advPaidNo, shipCom, userid)) {
                QNBSERDao daonb = new QNBSERDao();
                daonb.editCurrent(advPaidNo.trim().substring(4), advPaidNo.trim().substring(0, 4), "AI", "02");

                for (int i = 0; i < no.length; i++) {
                    IMSAPDETAILDao daode = new IMSAPDETAILDao();
                    if (daode.add(advPaidNo, no[i], checkNo[i], checkDate[i], bank[i],
                            advance[i].replace(",", ""), estAdv[i].replace(",", ""), dnNo[i],
                            dnDate[i], invNoList[i], billAmt[i].replace(",", ""), tax3[i].replace(",", ""),
                            tax1[i].replace(",", ""), amount[i].replace(",", ""), totAdv[i].replace(",", ""),
                            balance[i].replace(",", "").replace("(", "").replace(")", ""), userid, imcD[i].split(" : ")[0])) {
                        IMSSHEADDao daohd = new IMSSHEADDao();
                        if (daohd.updateAdvIMS200(pimno[i], advPaidNo)) {
                            if (invNoList[i] != null) {
                                String[] invL = invNoList[i].split(",");
                                for (String inv : invL) {
                                    IMSSDETAILDao daosts = new IMSSDETAILDao();
                                    daosts.setStatus("IMSSDSTS200", pimno[i], inv.trim(), "'Y'");
                                }
                            }
                        }
                    }
                }
            }
        } else {
            IMSAPHEADDao dao = new IMSAPHEADDao();
            if (dao.edit(advPaidNo, shipCom, userid)) {
                for (int i = 0; i < no.length; i++) {
                    IMSAPDETAILDao daockd = new IMSAPDETAILDao();
                    if (daockd.check(advPaidNo, no[i]).equals("t")) {
                        IMSAPDETAILDao daode = new IMSAPDETAILDao();
                        if (daode.add(advPaidNo, no[i], checkNo[i], checkDate[i], bank[i],
                                advance[i].replace(",", ""), estAdv[i].replace(",", ""), dnNo[i],
                                dnDate[i], invNoList[i], billAmt[i].replace(",", ""), tax3[i].replace(",", ""),
                                tax1[i].replace(",", ""), amount[i].replace(",", ""), totAdv[i].replace(",", ""),
                                balance[i].replace(",", "").replace("(", "").replace(")", ""), userid, imcD[i].split(" : ")[0])) {
                            IMSSHEADDao daohd = new IMSSHEADDao();
                            if (daohd.updateAdvIMS200(pimno[i], advPaidNo)) {
                                if (invNoList[i] != null) {
                                    String[] invL = invNoList[i].split(",");
                                    for (String inv : invL) {
                                        IMSSDETAILDao daosts = new IMSSDETAILDao();
                                        daosts.setStatus("IMSSDSTS200", pimno[i], inv.trim(), "'Y'");
                                    }
                                }
                            }
                        }
                    } else {
                        IMSAPDETAILDao daode = new IMSAPDETAILDao();
                        if (daode.edit(advPaidNo, no[i], checkNo[i], checkDate[i], bank[i],
                                advance[i].replace(",", ""), estAdv[i].replace(",", ""), dnNo[i],
                                dnDate[i], invNoList[i], billAmt[i].replace(",", ""), tax3[i].replace(",", ""),
                                tax1[i].replace(",", ""), amount[i].replace(",", ""), totAdv[i].replace(",", ""),
                                balance[i].replace(",", "").replace("(", "").replace(")", ""), userid, imcD[i].split(" : ")[0])) {
                            IMSSHEADDao daohd = new IMSSHEADDao();
                            daohd.updateAdvIMS200(pimno[i], advPaidNo);
                        }
                    }

                }
            }
        }

        response.setHeader("Refresh", "0;/IMS2/IMS200/display");

    }

}
