/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP750Dao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP740Dao;
import com.twc.ims.dao.IMSRP760Dao;
import com.twc.ims.entity.IMSRP750;
import com.twc.ims.entity.IMSRP740;
import com.twc.ims.entity.IMSRP760;
import com.twc.ims.entity.IMSSHC;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS760 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS760() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String imc = request.getParameter("imc");
        String ym = request.getParameter("ym");

        IMSSHC imcOP = new IMSIMCDao().findByCode(imc);

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Balance Advance Import Amount");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 101;

        data.put(Integer.toString(ni++), new Object[]{"Import Company", imcOP.getName()});

        data.put(Integer.toString(ni++), new Object[]{"Shipping Company", "Adv Imp Paid no.",
            "Cheque No", "Bank", "Cheque Date", "Advance", "Estimate Advance", "Bill Amount",
            "Tax 3%", "Tax 1%", "Net Amount", "Sum Net Amount", "Remain Chq Amount"});

        List<IMSRP760> rp760List = new IMSRP760Dao().findByCodePT(imc, ym);

        for (int i = 0; i < rp760List.size(); i++) {
            data.put(Integer.toString(ni++), new Object[]{rp760List.get(i).getShip(), rp760List.get(i).getPdno(),
                rp760List.get(i).getCqno(), rp760List.get(i).getBank(), rp760List.get(i).getCqdt(),
                rp760List.get(i).getAdv(), rp760List.get(i).getEst(), rp760List.get(i).getBill(),
                rp760List.get(i).getTax3(), rp760List.get(i).getTax1(), rp760List.get(i).getNet(),
                rp760List.get(i).getSumnet(), rp760List.get(i).getRem()});
        }

        //Set Column Width
        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 5000);
        sheet.setColumnWidth(6, 5000);
        sheet.setColumnWidth(7, 5000);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);
        sheet.setColumnWidth(10, 5000);
        sheet.setColumnWidth(11, 5000);
        sheet.setColumnWidth(12, 5000);

        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 1) {
                    CellStyle style_tmp = workbook.createCellStyle();
//                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    if (cellnum == 1) {
                        style_tmp.setFont(my_font);
                    }
                    cell.setCellStyle(style_tmp);
                } else if (rownum == 2) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setFont(my_font);
                    if (cellnum >= 2 && cellnum <= 5) {
                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    } else if (cellnum >= 6) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    }
                    cell.setCellStyle(style_tmp);
                } else {
                    CellStyle style_tmp = workbook.createCellStyle();
                    if (cell.getStringCellValue().contains("Total")) {
                        style_tmp.setFont(my_font);
                    }
                    if (cellnum >= 2 && cellnum <= 5) {
                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    } else if (cellnum >= 6) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    }
                    cell.setCellStyle(style_tmp);
                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "SUMMARY_BALANCE_ADVANCE_IMPORT_AMOUNT_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS760/edit?imc=" + imc);
    }

}
