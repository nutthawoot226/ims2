/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerIMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS200.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public EditControllerIMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS200/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        String advPaidNo = request.getParameter("advPaidNo");

        request.setAttribute("cur", advPaidNo);

        IMSAPHEADDao daoh = new IMSAPHEADDao();
        IMSAPHEAD head = daoh.findByCode(advPaidNo);

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(head.getShipCom());

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());

        IMSAPDETAILDao dao3 = new IMSAPDETAILDao();
        List<IMSAPDETAIL> detList = dao3.findByCode(advPaidNo);
        request.setAttribute("detList", detList);

        if (!detList.isEmpty()) {
            double sumadv = 0;
            double sumestAdv = 0;
            double sumbillAmt = 0;
            double sumtax3 = 0;
            double sumtax1 = 0;
            double sumamount = 0;
            double sumtotAdv = 0;
            double sumbalance = 0;

            double sumtotAdvTMP = 0;
            double sumbalanceTMP = 0;

            String checkno = "";
            boolean isOne = true;

            for (int i = 0; i < detList.size(); i++) {
                String sa = "0";
                if (detList.get(i).getAdvance() != null) {
                    sa = detList.get(i).getAdvance().replace(",", "");
                }

                sumadv += Double.parseDouble(sa);
                sumestAdv += Double.parseDouble(detList.get(i).getEstAdv().replace(",", ""));
                sumbillAmt += Double.parseDouble(detList.get(i).getBillAmt().replace(",", ""));
                sumtax3 += Double.parseDouble(detList.get(i).getTax3().replace(",", ""));
                sumtax1 += Double.parseDouble(detList.get(i).getTax1().replace(",", ""));
                sumamount += Double.parseDouble(detList.get(i).getAmount().replace(",", ""));

                if (checkno.equals("")) {
                    sumtotAdvTMP += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList.get(i).getCheckNoD();
                } else if (checkno.equals(detList.get(i).getCheckNoD())) {
                    sumtotAdvTMP = 0;
                    sumbalanceTMP = 0;

                    sumtotAdvTMP += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList.get(i).getCheckNoD();
                } else {
                    isOne = false;

                    sumtotAdv += sumtotAdvTMP;
                    sumbalance += sumbalanceTMP;

                    checkno = detList.get(i).getCheckNoD();
                }
            }

            request.setAttribute("grt", "Grand Total");
            request.setAttribute("sumadv", formatDou.format(sumadv));
            request.setAttribute("sumestAdv", formatDou.format(sumestAdv));
            request.setAttribute("sumbillAmt", formatDou.format(sumbillAmt));
            request.setAttribute("sumtax3", formatDou.format(sumtax3));
            request.setAttribute("sumtax1", formatDou.format(sumtax1));
            request.setAttribute("sumamount", formatDou.format(sumamount));

            if (isOne) {
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdvTMP));
                request.setAttribute("sumbalance", formatDou.format(sumbalanceTMP));
            } else {
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdv));
                request.setAttribute("sumbalance", formatDou.format(sumbalance));
            }

            String save = "<br><br>"
                    + "<a class=\"btn btn-success\" onclick=\"document.getElementById('myModal-3').style.display = 'block';\"><i class=\"fa fa-save\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Save</a>"
                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "<a class=\"btn btn-primary\" onclick=\"document.getElementById('myModal-5').style.display = 'block';\"><i class=\"fa fa-refresh\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Re Calculate</a>";
            request.setAttribute("save", save);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS200/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        String advPaidNo = request.getParameter("no");
        String checkNo = request.getParameter("checkNo");
        String allCheckNo = request.getParameter("allCheckNo");
        String shipcom = request.getParameter("shipcom");
        String userid = request.getParameter("userid2");

        new IMSAPHEADDao().edit(advPaidNo, shipcom, userid);

        request.setAttribute("cur", advPaidNo);

        IMSAPHEADDao daoh = new IMSAPHEADDao();
        IMSAPHEAD head = daoh.findByCode(advPaidNo);

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(head.getShipCom());

        if (!allCheckNo.contains(checkNo.trim())) {
            allCheckNo += ",'" + checkNo + "'";
        }

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());
        request.setAttribute("checkNo", checkNo);
        request.setAttribute("allCheckNo", allCheckNo);

        IMSAPDETAILDao dao3 = new IMSAPDETAILDao();
        List<IMSAPDETAIL> detList = dao3.findByCode(advPaidNo);
        request.setAttribute("detList", detList);

        IMSAPDETAILDao daoMax = new IMSAPDETAILDao();
        int line = daoMax.findNextLine(advPaidNo);

        String[] id = new String[detList.size()];
        for (int i = 0; i < id.length; i++) {
            id[i] = detList.get(i).getCheckNoD() + "+" + detList.get(i).getNo();
        }

        String[] invList = new String[detList.size()];
        for (int i = 0; i < invList.length; i++) {
            invList[i] = detList.get(i).getInvNoList();
        }

        IMSADETAILDao dao4 = new IMSADETAILDao();
        List<IMSAPDETAIL> detList2 = dao4.findAll(line, allCheckNo.substring(1), head.getShipCom(), id, invList);
        request.setAttribute("detList2", detList2);

        if (!detList.isEmpty() || !detList2.isEmpty()) {
            double sumadv = 0;
            double sumestAdv = 0;
            double sumbillAmt = 0;
            double sumtax3 = 0;
            double sumtax1 = 0;
            double sumamount = 0;
            double sumtotAdv = 0;
            double sumbalance = 0;

            double sumtotAdvTMP = 0;
            double sumbalanceTMP = 0;

            String checkno = "";
            boolean isOne = true;

            for (int i = 0; i < detList.size(); i++) {
                String sa = "0";
                if (detList.get(i).getAdvance() != null) {
                    sa = detList.get(i).getAdvance().replace(",", "");
                }

                sumadv += Double.parseDouble(sa);
                sumestAdv += Double.parseDouble(detList.get(i).getEstAdv().replace(",", ""));
                sumbillAmt += Double.parseDouble(detList.get(i).getBillAmt().replace(",", ""));
                sumtax3 += Double.parseDouble(detList.get(i).getTax3().replace(",", ""));
                sumtax1 += Double.parseDouble(detList.get(i).getTax1().replace(",", ""));
                sumamount += Double.parseDouble(detList.get(i).getAmount().replace(",", ""));

                if (checkno.equals("")) {
                    sumtotAdvTMP += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList.get(i).getCheckNoD();
                } else if (checkno.equals(detList.get(i).getCheckNoD())) {
                    sumtotAdvTMP = 0;
                    sumbalanceTMP = 0;

                    sumtotAdvTMP += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList.get(i).getCheckNoD();
                } else {
                    isOne = false;

                    sumtotAdv += sumtotAdvTMP;
                    sumbalance += sumbalanceTMP;

                    checkno = detList.get(i).getCheckNoD();
                }
            }

            for (int i = 0; i < detList2.size(); i++) {
                detList2.get(i).setINDB("INDB");
                String sa = "0";
                if (detList2.get(i).getAdvance() != null) {
                    sa = detList2.get(i).getAdvance().replace(",", "");
                }

                sumadv += Double.parseDouble(sa);
                sumestAdv += Double.parseDouble(detList2.get(i).getEstAdv().replace(",", ""));
                sumbillAmt += Double.parseDouble(detList2.get(i).getBillAmt().replace(",", ""));
                sumtax3 += Double.parseDouble(detList2.get(i).getTax3().replace(",", ""));
                sumtax1 += Double.parseDouble(detList2.get(i).getTax1().replace(",", ""));
                sumamount += Double.parseDouble(detList2.get(i).getAmount().replace(",", ""));

                if (checkno.equals("")) {
                    sumtotAdvTMP += Double.parseDouble(detList2.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList2.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList2.get(i).getCheckNoD();
                } else if (checkno.equals(detList2.get(i).getCheckNoD())) {
                    sumtotAdvTMP = 0;
                    sumbalanceTMP = 0;

                    sumtotAdvTMP += Double.parseDouble(detList2.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList2.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList2.get(i).getCheckNoD();
                } else {
                    isOne = false;

                    sumtotAdv += sumtotAdvTMP;
                    sumbalance += sumbalanceTMP;

                    checkno = detList2.get(i).getCheckNoD();
                }
            }

            request.setAttribute("grt", "Grand Total");
            request.setAttribute("sumadv", formatDou.format(sumadv));
            request.setAttribute("sumestAdv", formatDou.format(sumestAdv));
            request.setAttribute("sumbillAmt", formatDou.format(sumbillAmt));
            request.setAttribute("sumtax3", formatDou.format(sumtax3));
            request.setAttribute("sumtax1", formatDou.format(sumtax1));
            request.setAttribute("sumamount", formatDou.format(sumamount));

            if (isOne) {
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdvTMP));
                request.setAttribute("sumbalance", formatDou.format(sumbalanceTMP));
            } else {
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdv));
                request.setAttribute("sumbalance", formatDou.format(sumbalance));
            }

            String save = "<br><br><a class=\"btn btn-success\" onclick=\"document.getElementById('myModal-3').style.display = 'block';\"><i class=\"fa fa-save\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Save</a>";
            request.setAttribute("save", save);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
