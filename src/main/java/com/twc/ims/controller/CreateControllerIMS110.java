/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CreateControllerIMS110 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createIMS110.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public CreateControllerIMS110() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS110/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Setting. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        QNBSERDao dao = new QNBSERDao();
        String cur = dao.findCurrent("AI", "01");
        request.setAttribute("cur", cur);

        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS110/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Setting. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        QNBSERDao dao = new QNBSERDao();
        String cur = dao.findCurrent("AI", "01");
        request.setAttribute("cur", cur);

        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        String dateF = request.getParameter("dateF");
        String dateT = request.getParameter("dateT");
        String shipcom = request.getParameter("shipcom");

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(shipcom);

        if (dateF.trim().equals("")) {
            dateF = dateT;
        }

        if (dateT.trim().equals("")) {
            dateT = dateF;
        }

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());
        request.setAttribute("dateF", dateF);
        request.setAttribute("dateT", dateT);

        IMSSHEADDao dao3 = new IMSSHEADDao();
        List<IMSSDETAIL> detList = dao3.findAll(1, dateF, dateT, shipcom, "");
        request.setAttribute("detList", detList);

        if (!detList.isEmpty()) {

            IMSBAMDao dao4 = new IMSBAMDao();
            List<IMSSHC> bankList = dao4.findAll();

//            String opt = "<b>CHEQUE DATE</b>\n"
//                    + "<input type=\"date\" id=\"checkDateTmp\" name=\"checkDateTmp\" onchange=\"document.getElementById('checkDate').value = this.value;\" style=\" height: 30px; width: 150px;\">";
            String totAmt = "<b>TOTAL AMOUNT</b>&nbsp;&nbsp;:&nbsp;&nbsp;<b id=\"finalTotalAmt\"></b>"
                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "<a class=\"btn btn-success\" onclick=\"document.getElementById('myModal-3').style.display = 'block';\"><i class=\"fa fa-save\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Save</a>";

            StringBuilder cb = new StringBuilder();
            cb.append("<br>\n"
                    + "<b>CHEQUE DATE</b>\n"
                    + "<input type=\"date\" id=\"checkDateTmp\" name=\"checkDateTmp\" onchange=\"document.getElementById('checkDate').value = this.value;\" style=\" height: 30px; width: 150px;\">"
                    + "                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n"
                    + "                            <b>CHEQUE NO.</b>\n"
                    + "                            <input type=\"text\" id=\"checkNotmp\" name=\"checkNotmp\" onchange=\"document.getElementById('checkNo').value = this.value;\" style=\" height: 30px; width: 150px;\">\n"
                    + "                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n"
                    + "                            <b>BANK CODE</b>\n"
                    + "                            <select id=\"bnkCodetmp\" name=\"bnkCodetmp\" style=\" height: 30px; width: 450px;\" onchange=\"document.getElementById('bnkCode').value = this.value;\">\n"
                    + "                                <option value=\"\" selected hidden></option>");

            for (int i = 0; i < bankList.size(); i++) {
                cb.append("<option value=\"").append(bankList.get(i).getCode()).append("\">").append(bankList.get(i).getCode()).append(" : ").append(bankList.get(i).getName() == null ? "" : bankList.get(i).getName()).append(" (").append(bankList.get(i).getNamet() == null ? "" : bankList.get(i).getNamet()).append(")</option>");
            }
            cb.append("</select>");

//            request.setAttribute("detListSizeNotZero", opt);
            request.setAttribute("totalAmt", totAmt);
            request.setAttribute("cb", cb);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
