/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String impPaidNo = request.getParameter("impPaidNo");

        IMSPHEADDao daoup = new IMSPHEADDao();
        daoup.updateSTS(impPaidNo, "D");

        new IMSPDETAILDao().delete2(impPaidNo);

        new IMSSDETAILDao().clearFlag(impPaidNo);

        response.setHeader("Refresh", "0;/IMS2/IMS300/display");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
