/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class ReCalControllerIMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public ReCalControllerIMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String advPaidNo = request.getParameter("advPaidNo");

        List<IMSAPDETAIL> detList = new IMSAPDETAILDao().findByCodeReCal(advPaidNo);
        new IMSAPDETAILDao().deleteReCal(advPaidNo);

        String CQNO = "first";
        Double sumNetAmt = 0.00;

        for (int i = 0; i < detList.size(); i++) {
            IMSAPDETAIL ele = detList.get(i);

            ele.setIMSADLINO(Integer.toString(i + 1));
            if (CQNO.equals("first") || !CQNO.equals(ele.getIMSADCQNO())) {
                sumNetAmt = 0.00;
                sumNetAmt += Double.parseDouble(ele.getIMSADNETAMT());
                ele.setIMSADTNTAMT(Double.toString(sumNetAmt));

                Double remChqAmt = 0.00;
                remChqAmt = Double.parseDouble(ele.getIMSADADAMT()) - sumNetAmt;
                ele.setIMSADBALAMT(Double.toString(remChqAmt));

            } else if (CQNO.equals(ele.getIMSADCQNO())) {
                sumNetAmt += Double.parseDouble(ele.getIMSADNETAMT());
                ele.setIMSADTNTAMT(Double.toString(sumNetAmt));

                Double remChqAmt = 0.00;
                remChqAmt = Double.parseDouble(ele.getIMSADADAMT()) - sumNetAmt;
                ele.setIMSADBALAMT(Double.toString(remChqAmt));
            }
            CQNO = ele.getIMSADCQNO();

//            System.out.println(ele.getIMSADLINO() + " || " + ele.getIMSADCQNO() + " || " + ele.getIMSADNETAMT() + " || " + ele.getIMSADTNTAMT() + " || " + ele.getIMSADBALAMT());
            new IMSAPDETAILDao().addReCal(ele);
        }

        new IMSAPDETAILDao().updateEstReCal(advPaidNo);

        response.setHeader("Refresh", "0;/IMS2/IMS200/edit?advPaidNo=" + advPaidNo);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
