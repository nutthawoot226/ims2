/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSFRGDao extends database {

    public ResultSet reportIMS660D(String ym, String imc) {

        String sql = "SELECT IMSFRPIMNO, IMSHIMC, IMFFRINV, IMSFRFFC, IMSFFDEST, IMSFRBL, IMSFRIMNO, IMSFRAMT, IMSFRVAT\n"
                + "FROM IMSFRG\n"
                + "LEFT JOIN IMSFFC ON IMSFFC.IMSFFCODE = IMSFRG.IMSFRFFC\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSFRG.IMSFRPIMNO\n"
                + "WHERE FORMAT(IMSFRDATE,'yyyyMM') = '" + ym + "'\n"
                + "AND IMSHIMC = '" + imc + "'\n"
                + "ORDER BY IMFFRINV, IMSFRFFC, IMSFRBL, IMSFRIMNO";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS660JV(String ym, String imc) {

        String sql = "SELECT DISTINCT IMSHIMC, FRINV, FFC, IMSFFDEST, TOT_FA, TOT_VA, TOT, TOTAL_THAI\n"
                + "FROM(\n"
                + "SELECT IMSFRPIMNO, IMSHIMC, FRINV, FFC, IMSFFDEST\n"
                + ",(SELECT SUM(IMSFRAMT) FROM IMSFRG WHERE IMFFRINV = FRINV AND IMSFRFFC = FFC) AS TOT_FA\n"
                + ",(SELECT SUM(IMSFRVAT) FROM IMSFRG WHERE IMFFRINV = FRINV AND IMSFRFFC = FFC) AS TOT_VA\n"
                + ",(\n"
                + "ISNULL((SELECT SUM(IMSFRAMT) FROM IMSFRG WHERE IMFFRINV = FRINV AND IMSFRFFC = FFC),0)\n"
                + "+\n"
                + "ISNULL((SELECT SUM(IMSFRVAT) FROM IMSFRG WHERE IMFFRINV = FRINV AND IMSFRFFC = FFC),0)\n"
                + ") AS TOT\n"
                + ",DBO.NUM2THAI((\n"
                + "ISNULL((SELECT SUM(IMSFRAMT) FROM IMSFRG WHERE IMFFRINV = FRINV AND IMSFRFFC = FFC),0)\n"
                + "+\n"
                + "ISNULL((SELECT SUM(IMSFRVAT) FROM IMSFRG WHERE IMFFRINV = FRINV AND IMSFRFFC = FFC),0)\n"
                + ")) AS TOTAL_THAI\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSFRPIMNO, IMFFRINV AS FRINV, IMSFRFFC AS FFC\n"
                + "FROM IMSFRG\n"
                + "WHERE FORMAT(IMSFRDATE,'yyyyMM') = '" + ym + "'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSFFC ON IMSFFC.IMSFFCODE = TMP.FFC\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = TMP.IMSFRPIMNO\n"
                + "WHERE IMSHIMC = '" + imc + "'\n"
                + ")TTT";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
