/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP720;
import com.twc.ims.entity.IMSRP730;
import com.twc.ims.entity.IMSRP740;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP720Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyy') = '" + ym + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP720> findByCode(String ym, String imc) {

        List<IMSRP720> ul = new ArrayList<IMSRP720>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPYAM]\n"
                + "      ,FORMAT(ISNULL([IMSRPFORAMT],0),'#,#0.00') AS [IMSRPFORAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPINVAMT],0),'#,#0.00') AS [IMSRPINVAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPFREIGHT],0),'#,#0.00') AS [IMSRPFREIGHT]\n"
                + "      ,FORMAT(ISNULL([IMSRPIMPDUTY],0),'#,#0.00') AS [IMSRPIMPDUTY]\n"
                + "      ,FORMAT(ISNULL([IMSRPVATAMT],0),'#,#0.00') AS [IMSRPVATAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPTOTAMTEV],0),'#,#0.00') AS [IMSRPTOTAMTEV]\n"
                + "      ,FORMAT(ISNULL([IMSRPTOTAMTEVF],0),'#,#0.00') AS [IMSRPTOTAMTEVF]\n"
                + "      ,FORMAT(ISNULL([IMSRPEXPENSE],0),'#,#0.00') AS [IMSRPEXPENSE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP720]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPROD],[IMSRPCUR],[IMSRPYAM]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String ccy = "ST";
            String yam = "ST";

            Double FA = 0.00;
            Double IA = 0.00;
            Double FR = 0.00;
            Double ID = 0.00;
            Double VA = 0.00;
            Double TA = 0.00;
            Double TA2 = 0.00;

            Double cFA = 0.00;
            Double cIA = 0.00;
            Double cFR = 0.00;
            Double cID = 0.00;
            Double cVA = 0.00;
            Double cTA = 0.00;
            Double cTA2 = 0.00;

            Double aFA = 0.00;
            Double aIA = 0.00;
            Double aFR = 0.00;
            Double aID = 0.00;
            Double aVA = 0.00;
            Double aTA = 0.00;
            Double aTA2 = 0.00;

            while (result.next()) {

                IMSRP720 p = new IMSRP720();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                ym = result.getString("IMSRPPERIOD");

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPPROD") + "</b>");

                    if (ccy.equals(result.getString("IMSRPCUR"))) {
                        ccy = result.getString("IMSRPCUR");
                        p.setCcy("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPCUR") + "</b>");

                        if (yam.equals(result.getString("IMSRPYAM"))) {
                            yam = result.getString("IMSRPYAM");
                            p.setYam("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPYAM") + "</b>");

                        } else {
                            yam = result.getString("IMSRPYAM");
                            p.setYam(result.getString("IMSRPYAM"));
                        }
                    } else {
                        if (!ccy.equals("ST")) {
                            IMSRP720 sp = new IMSRP720();
                            sp.setCcy("<b>TOTAL BY CCY : " + ccy + "</b>");
                            sp.setForAmt("<b>" + formatDou.format(cFA) + "</b>");
                            sp.setInvAmt("<b>" + formatDou.format(cIA) + "</b>");
                            sp.setFreight("<b>" + formatDou.format(cFR) + "</b>");
                            sp.setImpDuty("<b>" + formatDou.format(cID) + "</b>");
                            sp.setVatAmt("<b>" + formatDou.format(cVA) + "</b>");
                            sp.setTotAmtEV("<b>" + formatDou.format(cTA) + "</b>");
                            sp.setTotAmtEVF("<b>" + formatDou.format(cTA2) + "</b>");
                            sp.setExp("<b>" + formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%</b>");
                            ul.add(sp);

                            cFA = 0.00;
                            cIA = 0.00;
                            cFR = 0.00;
                            cID = 0.00;
                            cVA = 0.00;
                            cTA = 0.00;
                            cTA2 = 0.00;
                        }

                        ccy = result.getString("IMSRPCUR");
                        p.setCcy(result.getString("IMSRPCUR"));
                        yam = result.getString("IMSRPYAM");
                        p.setYam(result.getString("IMSRPYAM"));
                    }

                } else {
                    if (!prod.equals("ST")) {
                        IMSRP720 sp = new IMSRP720();
                        sp.setCcy("<b>TOTAL BY CCY : " + ccy + "</b>");
                        sp.setForAmt("<b>" + formatDou.format(cFA) + "</b>");
                        sp.setInvAmt("<b>" + formatDou.format(cIA) + "</b>");
                        sp.setFreight("<b>" + formatDou.format(cFR) + "</b>");
                        sp.setImpDuty("<b>" + formatDou.format(cID) + "</b>");
                        sp.setVatAmt("<b>" + formatDou.format(cVA) + "</b>");
                        sp.setTotAmtEV("<b>" + formatDou.format(cTA) + "</b>");
                        sp.setTotAmtEVF("<b>" + formatDou.format(cTA2) + "</b>");
                        sp.setExp("<b>" + formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%</b>");
                        ul.add(sp);

                        cFA = 0.00;
                        cIA = 0.00;
                        cFR = 0.00;
                        cID = 0.00;
                        cVA = 0.00;
                        cTA = 0.00;
                        cTA2 = 0.00;

                        IMSRP720 sp2 = new IMSRP720();
                        sp2.setProd("<b>TOTAL BY PRODUCT : " + prod + "</b>");
                        sp2.setForAmt("<b>" + formatDou.format(FA) + "</b>");
                        sp2.setInvAmt("<b>" + formatDou.format(IA) + "</b>");
                        sp2.setFreight("<b>" + formatDou.format(FR) + "</b>");
                        sp2.setImpDuty("<b>" + formatDou.format(ID) + "</b>");
                        sp2.setVatAmt("<b>" + formatDou.format(VA) + "</b>");
                        sp2.setTotAmtEV("<b>" + formatDou.format(TA) + "</b>");
                        sp2.setTotAmtEVF("<b>" + formatDou.format(TA2) + "</b>");
                        sp2.setExp("<b>" + formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%</b>");
                        ul.add(sp2);

                        FA = 0.00;
                        IA = 0.00;
                        FR = 0.00;
                        ID = 0.00;
                        VA = 0.00;
                        TA = 0.00;
                        TA2 = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    ccy = result.getString("IMSRPCUR");
                    p.setCcy(result.getString("IMSRPCUR"));
                    yam = result.getString("IMSRPYAM");
                    p.setYam(result.getString("IMSRPYAM"));

                }

                p.setForAmt(result.getString("IMSRPFORAMT"));
                p.setInvAmt(result.getString("IMSRPINVAMT"));
                p.setFreight(result.getString("IMSRPFREIGHT"));
                p.setImpDuty(result.getString("IMSRPIMPDUTY"));
                p.setVatAmt(result.getString("IMSRPVATAMT"));
                p.setTotAmtEV(result.getString("IMSRPTOTAMTEV"));
                p.setTotAmtEVF(result.getString("IMSRPTOTAMTEVF"));
                p.setExp(result.getString("IMSRPEXPENSE") + "%");

                FA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                IA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                FR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                ID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                VA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                TA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                TA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                cFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                cIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                cFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                cID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                cVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                cTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                cTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                aFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                aIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                aFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                aID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                aVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                aTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                aTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                ul.add(p);

            }

            IMSRP720 sp = new IMSRP720();
            sp.setCcy("<b>TOTAL BY CCY : " + ccy + "</b>");
            sp.setForAmt("<b>" + formatDou.format(cFA) + "</b>");
            sp.setInvAmt("<b>" + formatDou.format(cIA) + "</b>");
            sp.setFreight("<b>" + formatDou.format(cFR) + "</b>");
            sp.setImpDuty("<b>" + formatDou.format(cID) + "</b>");
            sp.setVatAmt("<b>" + formatDou.format(cVA) + "</b>");
            sp.setTotAmtEV("<b>" + formatDou.format(cTA) + "</b>");
            sp.setTotAmtEVF("<b>" + formatDou.format(cTA2) + "</b>");
            sp.setExp("<b>" + formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%</b>");
            ul.add(sp);

            IMSRP720 sp2 = new IMSRP720();
            sp2.setProd("<b>TOTAL BY PRODUCT : " + prod + "</b>");
            sp2.setForAmt("<b>" + formatDou.format(FA) + "</b>");
            sp2.setInvAmt("<b>" + formatDou.format(IA) + "</b>");
            sp2.setFreight("<b>" + formatDou.format(FR) + "</b>");
            sp2.setImpDuty("<b>" + formatDou.format(ID) + "</b>");
            sp2.setVatAmt("<b>" + formatDou.format(VA) + "</b>");
            sp2.setTotAmtEV("<b>" + formatDou.format(TA) + "</b>");
            sp2.setTotAmtEVF("<b>" + formatDou.format(TA2) + "</b>");
            sp2.setExp("<b>" + formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%</b>");
            ul.add(sp2);

            IMSRP720 sp3 = new IMSRP720();
            sp3.setProd("<b>GRAND TOTAL BY YEAR : " + ym + "</b>");
            sp3.setForAmt("<b>" + formatDou.format(aFA) + "</b>");
            sp3.setInvAmt("<b>" + formatDou.format(aIA) + "</b>");
            sp3.setFreight("<b>" + formatDou.format(aFR) + "</b>");
            sp3.setImpDuty("<b>" + formatDou.format(aID) + "</b>");
            sp3.setVatAmt("<b>" + formatDou.format(aVA) + "</b>");
            sp3.setTotAmtEV("<b>" + formatDou.format(aTA) + "</b>");
            sp3.setTotAmtEVF("<b>" + formatDou.format(aTA2) + "</b>");
            sp3.setExp("<b>" + formatDou.format((aIA == 0 ? 100 : (aTA / aIA) * 100)) + "%</b>");
            ul.add(sp3);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP720> findByCodePT(String ym, String imc) {

        List<IMSRP720> ul = new ArrayList<IMSRP720>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPYAM]\n"
                + "      ,FORMAT(ISNULL([IMSRPFORAMT],0),'#,#0.00') AS [IMSRPFORAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPINVAMT],0),'#,#0.00') AS [IMSRPINVAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPFREIGHT],0),'#,#0.00') AS [IMSRPFREIGHT]\n"
                + "      ,FORMAT(ISNULL([IMSRPIMPDUTY],0),'#,#0.00') AS [IMSRPIMPDUTY]\n"
                + "      ,FORMAT(ISNULL([IMSRPVATAMT],0),'#,#0.00') AS [IMSRPVATAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPTOTAMTEV],0),'#,#0.00') AS [IMSRPTOTAMTEV]\n"
                + "      ,FORMAT(ISNULL([IMSRPTOTAMTEVF],0),'#,#0.00') AS [IMSRPTOTAMTEVF]\n"
                + "      ,FORMAT(ISNULL([IMSRPEXPENSE],0),'#,#0.00') AS [IMSRPEXPENSE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP720]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPROD],[IMSRPCUR],[IMSRPYAM]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String ccy = "ST";
            String yam = "ST";

            Double FA = 0.00;
            Double IA = 0.00;
            Double FR = 0.00;
            Double ID = 0.00;
            Double VA = 0.00;
            Double TA = 0.00;
            Double TA2 = 0.00;

            Double cFA = 0.00;
            Double cIA = 0.00;
            Double cFR = 0.00;
            Double cID = 0.00;
            Double cVA = 0.00;
            Double cTA = 0.00;
            Double cTA2 = 0.00;

            Double aFA = 0.00;
            Double aIA = 0.00;
            Double aFR = 0.00;
            Double aID = 0.00;
            Double aVA = 0.00;
            Double aTA = 0.00;
            Double aTA2 = 0.00;

            while (result.next()) {

                IMSRP720 p = new IMSRP720();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                ym = result.getString("IMSRPPERIOD");

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("");

                    if (ccy.equals(result.getString("IMSRPCUR"))) {
                        ccy = result.getString("IMSRPCUR");
                        p.setCcy("");

                        if (yam.equals(result.getString("IMSRPYAM"))) {
                            yam = result.getString("IMSRPYAM");
                            p.setYam("");

                        } else {
                            yam = result.getString("IMSRPYAM");
                            p.setYam(result.getString("IMSRPYAM"));
                        }
                    } else {
                        if (!ccy.equals("ST")) {
                            IMSRP720 sp = new IMSRP720();
                            sp.setCcy(ccy + " Total+ULINE");
                            sp.setYam("+ULINE");
                            sp.setForAmt(formatDou.format(cFA) + "+ULINE");
                            sp.setInvAmt(formatDou.format(cIA) + "+ULINE");
                            sp.setFreight(formatDou.format(cFR) + "+ULINE");
                            sp.setImpDuty(formatDou.format(cID) + "+ULINE");
                            sp.setVatAmt(formatDou.format(cVA) + "+ULINE");
                            sp.setTotAmtEV(formatDou.format(cTA) + "+ULINE");
                            sp.setTotAmtEVF(formatDou.format(cTA2) + "+ULINE");
                            sp.setExp(formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%+ULINE");
                            ul.add(sp);

                            cFA = 0.00;
                            cIA = 0.00;
                            cFR = 0.00;
                            cID = 0.00;
                            cVA = 0.00;
                            cTA = 0.00;
                            cTA2 = 0.00;
                        }

                        ccy = result.getString("IMSRPCUR");
                        p.setCcy(result.getString("IMSRPCUR"));
                        yam = result.getString("IMSRPYAM");
                        p.setYam(result.getString("IMSRPYAM"));
                    }

                } else {
                    if (!prod.equals("ST")) {
                        IMSRP720 sp = new IMSRP720();
                        sp.setCcy(ccy + " Total+ULINE");
                        sp.setYam("+ULINE");
                        sp.setForAmt(formatDou.format(cFA) + "+ULINE");
                        sp.setInvAmt(formatDou.format(cIA) + "+ULINE");
                        sp.setFreight(formatDou.format(cFR) + "+ULINE");
                        sp.setImpDuty(formatDou.format(cID) + "+ULINE");
                        sp.setVatAmt(formatDou.format(cVA) + "+ULINE");
                        sp.setTotAmtEV(formatDou.format(cTA) + "+ULINE");
                        sp.setTotAmtEVF(formatDou.format(cTA2) + "+ULINE");
                        sp.setExp(formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%+ULINE");
                        ul.add(sp);

                        cFA = 0.00;
                        cIA = 0.00;
                        cFR = 0.00;
                        cID = 0.00;
                        cVA = 0.00;
                        cTA = 0.00;
                        cTA2 = 0.00;

                        IMSRP720 sp2 = new IMSRP720();
                        sp2.setProd(prod + " Total+ULINE");
                        sp2.setCcy("+ULINE");
                        sp2.setYam("+ULINE");
                        sp2.setForAmt(formatDou.format(FA) + "+ULINE");
                        sp2.setInvAmt(formatDou.format(IA) + "+ULINE");
                        sp2.setFreight(formatDou.format(FR) + "+ULINE");
                        sp2.setImpDuty(formatDou.format(ID) + "+ULINE");
                        sp2.setVatAmt(formatDou.format(VA) + "+ULINE");
                        sp2.setTotAmtEV(formatDou.format(TA) + "+ULINE");
                        sp2.setTotAmtEVF(formatDou.format(TA2) + "+ULINE");
                        sp2.setExp(formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%+ULINE");
                        ul.add(sp2);

                        FA = 0.00;
                        IA = 0.00;
                        FR = 0.00;
                        ID = 0.00;
                        VA = 0.00;
                        TA = 0.00;
                        TA2 = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    ccy = result.getString("IMSRPCUR");
                    p.setCcy(result.getString("IMSRPCUR"));
                    yam = result.getString("IMSRPYAM");
                    p.setYam(result.getString("IMSRPYAM"));

                }

                p.setForAmt(result.getString("IMSRPFORAMT"));
                p.setInvAmt(result.getString("IMSRPINVAMT"));
                p.setFreight(result.getString("IMSRPFREIGHT"));
                p.setImpDuty(result.getString("IMSRPIMPDUTY"));
                p.setVatAmt(result.getString("IMSRPVATAMT"));
                p.setTotAmtEV(result.getString("IMSRPTOTAMTEV"));
                p.setTotAmtEVF(result.getString("IMSRPTOTAMTEVF"));
                p.setExp(result.getString("IMSRPEXPENSE") + "%");

                FA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                IA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                FR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                ID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                VA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                TA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                TA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                cFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                cIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                cFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                cID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                cVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                cTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                cTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                aFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                aIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                aFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                aID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                aVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                aTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                aTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                ul.add(p);

            }

            IMSRP720 sp = new IMSRP720();
            sp.setCcy(ccy + " Total+ULINE");
            sp.setYam("+ULINE");
            sp.setForAmt(formatDou.format(cFA) + "+ULINE");
            sp.setInvAmt(formatDou.format(cIA) + "+ULINE");
            sp.setFreight(formatDou.format(cFR) + "+ULINE");
            sp.setImpDuty(formatDou.format(cID) + "+ULINE");
            sp.setVatAmt(formatDou.format(cVA) + "+ULINE");
            sp.setTotAmtEV(formatDou.format(cTA) + "+ULINE");
            sp.setTotAmtEVF(formatDou.format(cTA2) + "+ULINE");
            sp.setExp(formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%+ULINE");
            ul.add(sp);

            IMSRP720 sp2 = new IMSRP720();
            sp2.setProd(prod + " Total+ULINE");
            sp2.setCcy("+ULINE");
            sp2.setYam("+ULINE");
            sp2.setForAmt(formatDou.format(FA) + "+ULINE");
            sp2.setInvAmt(formatDou.format(IA) + "+ULINE");
            sp2.setFreight(formatDou.format(FR) + "+ULINE");
            sp2.setImpDuty(formatDou.format(ID) + "+ULINE");
            sp2.setVatAmt(formatDou.format(VA) + "+ULINE");
            sp2.setTotAmtEV(formatDou.format(TA) + "+ULINE");
            sp2.setTotAmtEVF(formatDou.format(TA2) + "+ULINE");
            sp2.setExp(formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%+ULINE");
            ul.add(sp2);

            IMSRP720 sp3 = new IMSRP720();
            sp3.setProd("Grand Total+ULINE");
            sp3.setCcy("+ULINE");
            sp3.setYam("+ULINE");
            sp3.setForAmt(formatDou.format(aFA) + "+ULINE");
            sp3.setInvAmt(formatDou.format(aIA) + "+ULINE");
            sp3.setFreight(formatDou.format(aFR) + "+ULINE");
            sp3.setImpDuty(formatDou.format(aID) + "+ULINE");
            sp3.setVatAmt(formatDou.format(aVA) + "+ULINE");
            sp3.setTotAmtEV(formatDou.format(aTA) + "+ULINE");
            sp3.setTotAmtEVF(formatDou.format(aTA2) + "+ULINE");
            sp3.setExp(formatDou.format((aIA == 0 ? 100 : (aTA / aIA) * 100)) + "%+ULINE");
            ul.add(sp3);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCode22Print(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT CCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFORAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBILLAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMBA\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSRPCUR] AS CCY\n"
                + "FROM [IMSRP730]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            IMSRP730 p1 = new IMSRP730();
            p1.setCcy("");
            ul.add(p1);

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setCcy(result.getString("CCY") + "+TLB");
                p.setCustRate("+TB");
                p.setSum("+TB");
                p.setForAmt(result.getString("SUMFA") + "+TLRB");
                p.setBillAmt(result.getString("SUMBA") + "+TLRB");

                ul.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCode22(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT CCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFORAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBILLAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMBA\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSRPCUR] AS CCY\n"
                + "FROM [IMSRP730]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            IMSRP730 p1 = new IMSRP730();
            p1.setCcy("");
            ul.add(p1);

            int i = 0;

            while (result.next()) {

                i++;

                IMSRP730 p = new IMSRP730();

                if (i == 1) {
                    p.setProd("<b>GRAND TOTAL BY CCY</b>");
                }

                p.setCcy("<b>" + result.getString("CCY") + "</b>");
                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setBillAmt("<b>" + result.getString("SUMBA") + "</b>");

                ul.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findForPrint(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT]\n"
                + "      ,[IMSVDNAME]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSTRNAME] as [IMSRPTRANSPORT]\n"
                + "      ,FORMAT([IMSRPINVDATE],'dd-MM-yyy') as [IMSRPINVDATE]\n"
                + "      ,FORMAT([IMSRPARRDATE],'dd-MM-yyy') as [IMSRPARRDATE]\n"
                + "      ,FORMAT([IMSRPDEBITDATE],'dd-MM-yyy') as [IMSRPDEBITDATE]\n"
                + "  FROM [IMSRP740]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP740].[IMSRPVENDEPT]\n"
                + "  LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP740].[IMSRPTRANSPORT]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  ORDER BY [IMSRPPERIOD], [IMSRPIMPNO], [IMSRPVENDEPT], [IMSRPINVNO], [IMSRPPROD], [IMSRPTRANSPORT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setProd(result.getString("IMSRPIMPNO"));
                p.setVen(result.getString("IMSRPVENDEPT"));
                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setCcy(result.getString("IMSRPPROD"));
                p.setCustRate(result.getString("IMSRPTRANSPORT"));
                p.setForAmt(result.getString("IMSRPINVDATE"));
                p.setBillAmt(result.getString("IMSRPARRDATE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findForPrint2(String year) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT (SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD) AS MF\n"
                + ",(SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD DESC) AS MT\n"
                + ",*\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPINVAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPPAYAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPDIFFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE LEFT(IMSRPPERIOD,4) = '" + year + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod("M." + result.getString("MF") + "-M." + result.getString("MT"));
                p.setCcy(result.getString("CCY"));

                p.setForAmt(result.getString("SUMFA"));
                p.setInvAmt(result.getString("SUMIA"));
                p.setPayAmt(result.getString("SUMPA"));
                p.setDiffAmt(result.getString("SUMDA"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP720 ([IMSRPCOM]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPYAM]\n"
                + "	  ,[IMSRPIMC]\n"
                + "      ,[IMSRPFORAMT]\n"
                + "      ,[IMSRPINVAMT]\n"
                + "      ,[IMSRPFREIGHT]\n"
                + "      ,[IMSRPIMPDUTY]\n"
                + "      ,[IMSRPVATAMT]\n"
                + "      ,[IMSRPTOTAMTEV]\n"
                + "      ,[IMSRPTOTAMTEVF]\n"
                + "      ,[IMSRPEXPENSE]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER])\n"
                + "SELECT *, CASE WHEN ISNULL(IMSSDINVAMT,0) = 0 THEN 100 ELSE (ISNULL(TOTALNOVAT,0)/ISNULL(IMSSDINVAMT,0)*100) END\n"
                + ",CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "'\n"
                + "FROM(\n"
                + "SELECT 'TWC' AS COM, YEARS, IMSSDPROD, IMSSDCUR, IMSHTRDT, IMSHIMC\n"
                + ",SUM(IMSSDFINVAMT) AS IMSSDFINVAMT, SUM(IMSSDINVAMT) AS IMSSDINVAMT\n"
                + ",SUM(IMSCAFRAMT) AS IMSCAFRAMT, SUM(IMSCACTAMT) AS IMSCACTAMT\n"
                + ",SUM(VAT) AS VAT, SUM(TOTALNOVAT) AS TOTALNOVAT, SUM(TOTALNOVATNOFR) AS TOTALNOVATNOFR\n"
                + "FROM(\n"
                + "SELECT FORMAT(IMSHTRDT,'yyyy') AS YEARS, IMSSDPROD, IMSSDCUR, FORMAT(IMSHTRDT,'yyyy/MM') AS IMSHTRDT\n"
                + ",IMSSDFINVAMT, IMSSDINVAMT, IMSCAFRAMT, IMSCACTAMT\n"
                + ",(ISNULL(IMSCAFRVAT,0) + ISNULL(IMSCAINVAT,0) + ISNULL(IMSCAREVAT,0) + ISNULL(IMSCACLVAT,0) \n"
                + "+ ISNULL(IMSCACTVAT,0)) AS VAT\n"
                + ",(ISNULL(IMSCAFRAMT,0) + ISNULL(IMSCAINAMT,0) + ISNULL(IMSCAREAMT,0) + ISNULL(IMSCACLAMT,0) \n"
                + "+ ISNULL(IMSCABKAMT,0) + ISNULL(IMSCACTAMT,0)) AS TOTALNOVAT\n"
                + ",(ISNULL(IMSCAINAMT,0) + ISNULL(IMSCAREAMT,0) + ISNULL(IMSCACLAMT,0) \n"
                + "+ ISNULL(IMSCABKAMT,0) + ISNULL(IMSCACTAMT,0)) AS TOTALNOVATNOFR\n"
                + ",IMSHIMC\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN IMSCAC ON IMSCAC.IMSCAPIMNO = IMSSDETAIL.IMSSDPIMNO \n"
                + "AND IMSCAC.IMSCAINVNO = IMSSDETAIL.IMSSDINVNO\n"
                + "AND IMSCAC.IMSCALINO = IMSSDETAIL.IMSSDLINE\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyy') = '" + ym + "'\n"
                + "AND IMSHIMC = '" + imc + "'\n"
                + ")TES\n"
                + "GROUP BY YEARS, IMSSDPROD, IMSSDCUR, IMSHTRDT, IMSHIMC\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String imc) {

        String sql = "";

        if (code.trim().equals("2019")) {
            sql = "DELETE FROM IMSRP720 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "' "
                    + "AND IMSRPYAM IN ('2019/08','2019/09','2019/10','2019/11','2019/12')";
        } else {
            sql = "DELETE FROM IMSRP720 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "'";
        }

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
