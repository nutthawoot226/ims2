/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSCURDao extends database {

    public List<IMSSHC> findAll() {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT IMSCUCODE, IMSCUNAME, IMSCUNAMT "
                + "FROM IMSCUR "
                + "ORDER BY IMSCUCODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSCUCODE"));

                p.setName(result.getString("IMSCUNAME"));

                p.setNamet(result.getString("IMSCUNAMT"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSSHC findByCode(String code) {

        IMSSHC p = new IMSSHC();

        String sql = "SELECT IMSCUCODE, IMSCUNAME, IMSCUNAMT "
                + "FROM IMSCUR "
                + "WHERE IMSCUCODE = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setCode(result.getString("IMSCUCODE"));

                p.setName(result.getString("IMSCUNAME") == null ? "" : result.getString("IMSCUNAME"));

                p.setNamet(result.getString("IMSCUNAMT") == null ? "" : result.getString("IMSCUNAMT"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

}
