/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import com.twc.ims.entity.IMSSHEAD;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSSHEADDao extends database {

    public List<IMSSHEAD> findSHead(String ym, String ym2, String sortBy, String dataU) {

        List<IMSSHEAD> UAList = new ArrayList<IMSSHEAD>();

        String conHead = "";
        String conDetail = "";
        String conDetailIN = "";

        if (sortBy.equals("impCom")) {
            conHead = "AND IMSHIMC = '" + dataU + "'\n";

        } else if (sortBy.equals("venCom")) {
            conHead = "AND IMSHVDCODE = '" + dataU + "'\n";

        } else if (sortBy.equals("venDep")) {
            conDetail = "WHERE VEN_DEP > 0\n";
            conDetailIN = ",(SELECT COUNT(IMSSDVCODE) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDVCODE = '" + dataU + "') AS VEN_DEP\n";

        } else if (sortBy.equals("shipCom")) {
            conHead = "AND IMSHSHCODE = '" + dataU + "'\n";

        } else if (sortBy.equals("proGrp")) {
            conDetail = "WHERE PRO_GRP > 0\n";
            conDetailIN = ",(SELECT COUNT(IMSSDPROD) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDPROD = '" + dataU + "') AS PRO_GRP\n";

        } else if (sortBy.equals("prv")) {
            conHead = "AND IMSHPRV = '" + dataU + "'\n";

        } else if (sortBy.equals("sts")) {
            conDetail = "WHERE STS > 0\n";

            if (dataU.equals("IMS100")) {
                conDetailIN = ",(SELECT COUNT(IMSSDSTS100) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDSTS100 = 'Y') AS STS\n";

            } else if (dataU.equals("IMS101")) {
                conDetailIN = ",(SELECT COUNT(IMSSDSTS101) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDSTS101 = 'Y') AS STS\n";

            } else if (dataU.equals("IMS110")) {
                conDetailIN = ",(SELECT COUNT(IMSSDSTS110) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDSTS110 = 'Y') AS STS\n";

            } else if (dataU.equals("IMS200")) {
                conDetailIN = ",(SELECT COUNT(IMSSDSTS200) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDSTS200 = 'Y') AS STS\n";

            } else if (dataU.equals("IMS300")) {
                conDetailIN = ",(SELECT COUNT(IMSSDSTS300) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDSTS300 = 'Y') AS STS\n";

            } else if (dataU.equals("IMS301")) {
                conDetailIN = ",(SELECT COUNT(IMSSDSTS301) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO AND IMSSDSTS301 = 'Y') AS STS\n";

            } else if (dataU.equals("ALL")) {
                conDetailIN = ",(SELECT COUNT(*) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO) AS STS\n";

            }

        }

        String sql = "SELECT *\n"
                + "FROM(\n"
                + "SELECT IMSHPIMNO, IMSHIMNO, \n"
                + "FORMAT(IMSHTRDT,'yyyyMM') AS IMSHTRDT,\n"
                + "IMSFRBL,\n"
                + "IMSCUETNO,\n"
                + "IMSHIMC, IMSIMNAME, \n"
                + "IMSHPRV, IMSPRNAME, \n"
                + "IMSHVDCODE, IMSVDNAME, \n"
                + "IMSHSHCODE, IMSSCNAME, \n"
                + "IMSHUSER, USERS \n"
                + conDetailIN
                + "FROM IMSSHEAD\n"
                + "LEFT JOIN IMSIMC ON IMSIMC.IMSIMCODE = IMSSHEAD.IMSHIMC\n"
                + "LEFT JOIN IMSPRV ON IMSPRV.IMSPRCODE = IMSSHEAD.IMSHPRV\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = IMSSHEAD.IMSHVDCODE\n"
                + "LEFT JOIN IMSSHC ON IMSSHC.IMSSCCODE = IMSSHEAD.IMSHSHCODE\n"
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = IMSSHEAD.IMSHUSER\n"
                + "LEFT JOIN IMSFRG ON IMSFRG.IMSFRPIMNO = IMSSHEAD.IMSHPIMNO\n"
                + "LEFT JOIN IMSCUT ON IMSCUT.IMSCUPIMNO = IMSSHEAD.IMSHPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + conHead
                + ")TMP\n"
                + conDetail;
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHEAD p = new IMSSHEAD();

                p.setPimNo(result.getString("IMSHPIMNO"));
                p.setImpNo(result.getString("IMSHIMNO"));

                p.setAwb(result.getString("IMSFRBL"));
                p.setEnno(result.getString("IMSCUETNO"));

                p.setImc(result.getString("IMSHIMC") + " :");
                p.setPrv(result.getString("IMSHPRV") + " :");
                p.setVen(result.getString("IMSHVDCODE") + " :");
                p.setShip(result.getString("IMSHSHCODE") + " :");
                p.setUser(result.getString("IMSHUSER") + " :");

                p.setImcD(result.getString("IMSIMNAME"));
                p.setPrvD(result.getString("IMSPRNAME"));
                p.setVenD(result.getString("IMSVDNAME"));
                p.setShipD(result.getString("IMSSCNAME"));
                p.setUserD(result.getString("USERS"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findPrvIMS900(String ym, String ym2) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSHPRV, IMSPRNAME\n"
                + "FROM IMSSHEAD\n"
                + "LEFT JOIN IMSPRV ON IMSPRV.IMSPRCODE = IMSSHEAD.IMSHPRV\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSHPRV"));

                p.setName(result.getString("IMSPRNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findShipComIMS900(String ym, String ym2) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSHSHCODE, IMSSCNAME\n"
                + "FROM IMSSHEAD\n"
                + "LEFT JOIN IMSSHC ON IMSSHC.IMSSCCODE = IMSSHEAD.IMSHSHCODE\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSHSHCODE"));

                p.setName(result.getString("IMSSCNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findVenComIMS900(String ym, String ym2) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSHVDCODE, IMSVDNAME\n"
                + "FROM IMSSHEAD\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = IMSSHEAD.IMSHVDCODE\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSHVDCODE"));

                p.setName(result.getString("IMSVDNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findImpComIMS900(String ym, String ym2) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSHIMC, IMSIMNAME\n"
                + "FROM IMSSHEAD\n"
                + "LEFT JOIN IMSIMC ON IMSIMC.IMSIMCODE = IMSSHEAD.IMSHIMC\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSHIMC"));

                p.setName(result.getString("IMSIMNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateAdv(String code, String adv) {

        boolean result = false;

        String sql = "UPDATE IMSSHEAD "
                + "SET IMSHAVNO = '" + adv + "', IMSHEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSHPIMNO = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateAdvIMS200(String code, String adv) {

        boolean result = false;

        String sql = "UPDATE IMSSHEAD "
                + "SET IMSHAPNO = '" + adv + "', IMSHEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSHPIMNO = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateAdvIMS200RETURN(String adv) {

        boolean result = false;

        String sql = "UPDATE IMSSHEAD "
                + "SET IMSHAPNO = null, IMSHEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSHAPNO = '" + adv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateAdvIMS110RETURN(String adv) {

        boolean result = false;

        String sql = "UPDATE IMSSHEAD "
                + "SET IMSHAVNO = null, IMSHEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSHAVNO = '" + adv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSSDETAIL> findAll(int line, String dateF, String dateT, String shipcom, String allpimno) {

        List<IMSSDETAIL> UAList = new ArrayList<IMSSDETAIL>();

        String PIMNO = "";
        if (!allpimno.trim().equals("")) {
            PIMNO = "AND IMSHPIMNO NOT IN (" + allpimno + ")\n";
        }

        DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

        String sql = "SELECT IMSHPIMNO AS PIMNO, IMSHIMNO AS IMNO, (select [IMSIMCODE]+' : '+[IMSIMNAME] from [IMSIMC] where [IMSIMCODE] = [IMSHIMC]) as[IMSHIMC], dbo.GET_ALL_INV_NO(IMSHPIMNO) AS ALL_INV,\n"
                + "	   IMSHTRDT AS TRAN_DATE,\n"
                + "	   (SELECT SUM(IMSSDINVAMT) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO) AS SUM_INV_AMT,\n"
                + "	   IMSHSHCODE AS SHIP_COM, SP.IMSSCNAME AS SHIP_COM_NAME, null AS IMP_DUTY, null AS ADV_IMP\n"
                + "FROM IMSSHEAD HD\n"
                + "LEFT JOIN IMSSHC SP ON HD.IMSHSHCODE = SP.IMSSCCODE\n"
                + "WHERE SUBSTRING(CONVERT(VARCHAR(25), IMSHTRDT, 126), 1, 10) BETWEEN '" + dateF + "' AND '" + dateT + "'\n"
                + "AND IMSHSHCODE = '" + shipcom + "'\n"
                + "AND (IMSHAVNO IS NULL OR IMSHAVNO = '')\n"
                + PIMNO
                + "ORDER BY IMSHSHCODE, IMSHTRDT ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSDETAIL p = new IMSSDETAIL();

                p.setNo(Integer.toString(line));
                p.setpImpNo(result.getString("PIMNO"));
                p.setImcD(result.getString("IMSHIMC"));

                p.setInvNoList(result.getString("ALL_INV"));

                if (result.getString("TRAN_DATE") != null) {
                    p.setTransDate(result.getString("TRAN_DATE").split(" ")[0]);
                }

                double tot = Double.parseDouble(result.getString("SUM_INV_AMT"));
                String tqt = formatDou.format(tot);

                p.setInvAmt(tqt);

                String checkbox = "<input style=\"width: 20px; height: 20px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("PIMNO") + "\">";
                p.setCheckBox(checkbox);

                UAList.add(p);
                line++;

            }

//            IMSSDETAIL p = new IMSSDETAIL();
//
//            p.setNo(Integer.toString(line));
//            p.setpImpNo("2");
//            p.setInvNoList("TEST, TEST");
//            p.setTransDate("2019-03-27");
//
//            double tot = Double.parseDouble("2300230.00");
//            String tqt = formatDou.format(tot);
//
//            p.setInvAmt(tqt);
//
//            UAList.add(p);
//            line++;
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSDETAIL> findAllPIMNO(int line, String dateF, String dateT, String shipcom, String[] pImpNo, String allpimno) {

        List<IMSSDETAIL> UAList = new ArrayList<IMSSDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

        StringBuilder list = new StringBuilder();
        if (pImpNo != null) {
            for (int i = 0; i < pImpNo.length; i++) {
                if (i == 0) {
                    list.append("'").append(pImpNo[i]).append("'");
                } else {
                    list.append(",'").append(pImpNo[i]).append("'");
                }
            }
        }
//        System.out.println(list);

        String PNLIST = "AND IMSHPIMNO not in (" + list + ")\n";
        if (PNLIST.equals("AND IMSHPIMNO not in ()\n")) {
            PNLIST = "";
        }

        String PIMNO = "";
        if (!allpimno.trim().equals("")) {
            PIMNO = "AND IMSHPIMNO NOT IN (" + allpimno + ")\n";
        }

        String sql = "SELECT IMSHIMC+' : '+IMSIMNAME AS IMSHIMC, IMSHPIMNO AS PIMNO, IMSHIMNO AS IMNO, (select [IMSIMCODE]+' : '+[IMSIMNAME] from [IMSIMC] where [IMSIMCODE] = [IMSHIMC]) as[IMSHIMC], dbo.GET_ALL_INV_NO(IMSHPIMNO) AS ALL_INV,\n"
                + "	   IMSHTRDT AS TRAN_DATE,\n"
                + "	   (SELECT ISNULL(SUM(IMSSDINVAMT),0) FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSHPIMNO) AS SUM_INV_AMT,\n"
                + "	   IMSHSHCODE AS SHIP_COM, SP.IMSSCNAME AS SHIP_COM_NAME, null AS IMP_DUTY, null AS ADV_IMP\n"
                + "FROM IMSSHEAD HD\n"
                + "LEFT JOIN IMSSHC SP ON HD.IMSHSHCODE = SP.IMSSCCODE\n"
                + "LEFT JOIN IMSIMC ON IMSIMC.IMSIMCODE = HD.IMSHIMC\n"
                + "WHERE SUBSTRING(CONVERT(VARCHAR(25), IMSHTRDT, 126), 1, 10) BETWEEN '" + dateF + "' AND '" + dateT + "'\n"
                + "AND IMSHSHCODE = '" + shipcom + "'\n"
                + PNLIST
                + "AND (IMSHAVNO IS NULL OR IMSHAVNO = '')\n"
                + PIMNO
                + "ORDER BY IMSHSHCODE, IMSHTRDT ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSDETAIL p = new IMSSDETAIL();

                p.setNo(Integer.toString(line));
                p.setpImpNo(result.getString("PIMNO"));
                p.setImcD(result.getString("IMSHIMC"));
                p.setInvNoList(result.getString("ALL_INV"));

                if (result.getString("TRAN_DATE") != null) {
                    p.setTransDate(result.getString("TRAN_DATE").split(" ")[0]);
                }

                double tot = Double.parseDouble(result.getString("SUM_INV_AMT"));
                String tqt = formatDou.format(tot);

                p.setInvAmt(tqt);

                UAList.add(p);
                line++;

            }

//            IMSSDETAIL p = new IMSSDETAIL();
//
//            p.setNo(Integer.toString(line));
//            p.setpImpNo("2");
//            p.setInvNoList("TEST, TEST");
//            p.setTransDate("2019-03-27");
//
//            double tot = Double.parseDouble("2300230.00");
//            String tqt = formatDou.format(tot);
//
//            p.setInvAmt(tqt);
//
//            UAList.add(p);
//            line++;
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

}
