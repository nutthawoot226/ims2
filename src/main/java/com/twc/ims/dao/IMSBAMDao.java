/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSBAMDao extends database {

    public List<IMSSHC> findAllPay() {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT IMSBACODE, IMSBADESC, IMSBADEST "
                + "FROM IMSBMAS "
                + "WHERE IMSBTYPE = 'PAYMENT' "
                + "ORDER BY IMSBACODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSBACODE"));

                p.setName(result.getString("IMSBADESC"));

                p.setNamet(result.getString("IMSBADEST"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findAll() {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT IMSBACODE, IMSBADESC, IMSBADEST "
                + "FROM IMSBMAS "
                + "ORDER BY IMSBACODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSBACODE"));

                p.setName(result.getString("IMSBADESC"));

                p.setNamet(result.getString("IMSBADEST"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSSHC findByCode(String code) {

        IMSSHC p = new IMSSHC();

        String sql = "SELECT IMSBACODE, IMSBADESC, IMSBADEST "
                + "FROM IMSBMAS "
                + "WHERE IMSBACODE = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setCode(result.getString("IMSBACODE"));

                p.setName(result.getString("IMSBADESC") == null ? "" : result.getString("IMSBADESC"));

                p.setNamet(result.getString("IMSBADEST") == null ? "" : result.getString("IMSBADEST"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

}
