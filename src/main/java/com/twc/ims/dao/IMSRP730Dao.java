/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP730;
import com.twc.ims.entity.IMSRP740;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP730Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyyMM') = '" + ym + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP730> findByCode(String ym, String imc) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT LEFT([IMSRPPERIOD],4)+'/'+SUBSTRING(RIGHT([IMSRPPERIOD],2), \n"
                + "PATINDEX('%[^0]%', RIGHT([IMSRPPERIOD],2)+'.'), \n"
                + "LEN(RIGHT([IMSRPPERIOD],2))) AS [IMSRPPERIOD]\n"
                + "    ,ISNULL([IMSRPPROD],'') AS IMSRPPROD\n"
                + "    ,ISNULL([IMSRPTPCODE]+' : '+[IMSTRNAME],'') AS [IMSRPTPCODE]\n"
                + "    ,ISNULL([IMSRPVENDER]+' : '+[IMSVDNAME],'') AS [IMSRPVENDER]\n"
                + "    ,[IMSRPINVNO]\n"
                + "    ,[IMSRPCUR]\n"
                + "    ,FORMAT([IMSRPCUSTRATE],'#,#0.000000') as [IMSRPCUSTRATE]\n"
                + "    ,FORMAT([IMSRPFORAMT],'#,#0.00') as [IMSRPFORAMT]\n"
                + "    ,FORMAT([IMSRPBILLAMT],'#,#0.00') as [IMSRPBILLAMT]\n"
                + "FROM [IMSRP730]\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP730].[IMSRPVENDER]\n"
                + "LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP730].[IMSRPTPCODE]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + "ORDER BY [IMSRPPROD],[IMSRPTPCODE],[IMSRPVENDER],[IMSRPINVNO],[IMSRPCUR]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String transport = "ST";
            String ven = "";

            int cin = 0;
            int cin2 = 0;
            Double FA = 0.00;
            Double BA = 0.00;
            Double FA2 = 0.00;
            Double BA2 = 0.00;
            Double FA3 = 0.00;
            Double BA3 = 0.00;

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                ym = result.getString("IMSRPPERIOD");

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPPROD") + "</b>");

                    if (transport.equals(result.getString("IMSRPTPCODE"))) {
                        transport = result.getString("IMSRPTPCODE");
                        p.setTransport("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPTPCODE") + "</b>");

                    } else {
                        if (!transport.equals("ST")) {
                            IMSRP730 sp = new IMSRP730();
                            sp.setTransport("<b>TOTAL BY TRANSPORT : " + transport + "</b>");
                            sp.setCcy("<b>INVOICE COUNT : " + cin2 + "</b>");
                            sp.setForAmt("<b>" + formatDou.format(FA3) + "</b>");
                            sp.setBillAmt("<b>" + formatDou.format(BA3) + "</b>");
                            ul.add(sp);

                            cin2 = 0;
                            FA3 = 0.00;
                            BA3 = 0.00;
                        }

                        transport = result.getString("IMSRPTPCODE");
                        p.setTransport(result.getString("IMSRPTPCODE"));

                    }

                    if (ven.equals(result.getString("IMSRPVENDER"))) {
                        ven = result.getString("IMSRPVENDER");
                        p.setVen("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPVENDER") + "</b>");

                    } else {
                        ven = result.getString("IMSRPVENDER");
                        p.setVen(result.getString("IMSRPVENDER"));
                    }

                } else {
                    if (!transport.equals("ST")) {
                        IMSRP730 sp = new IMSRP730();
                        sp.setTransport("<b>TOTAL BY TRANSPORT : " + transport + "</b>");
                        sp.setCcy("<b>INVOICE COUNT : " + cin2 + "</b>");
                        sp.setForAmt("<b>" + formatDou.format(FA3) + "</b>");
                        sp.setBillAmt("<b>" + formatDou.format(BA3) + "</b>");
                        ul.add(sp);

                        cin2 = 0;
                        FA3 = 0.00;
                        BA3 = 0.00;
                    }

                    if (!prod.equals("ST")) {
                        IMSRP730 sp = new IMSRP730();
                        sp.setProd("<b>TOTAL BY PRODUCT : " + prod + "</b>");
                        sp.setInvNo("<b>INVOICE COUNT : " + cin + "</b>");
                        sp.setForAmt("<b>" + formatDou.format(FA) + "</b>");
                        sp.setBillAmt("<b>" + formatDou.format(BA) + "</b>");
                        ul.add(sp);

                        cin = 0;
                        FA = 0.00;
                        BA = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    transport = result.getString("IMSRPTPCODE");
                    p.setTransport(result.getString("IMSRPTPCODE"));
                    ven = result.getString("IMSRPVENDER");
                    p.setVen(result.getString("IMSRPVENDER"));

                }

                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setCcy(result.getString("IMSRPCUR"));
                p.setCustRate(result.getString("IMSRPCUSTRATE"));
                p.setForAmt(result.getString("IMSRPFORAMT"));
                p.setBillAmt(result.getString("IMSRPBILLAMT"));

                cin += 1;
                cin2 += 1;
                FA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                BA += Double.parseDouble(result.getString("IMSRPBILLAMT").replace(",", ""));
                FA2 += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                BA2 += Double.parseDouble(result.getString("IMSRPBILLAMT").replace(",", ""));
                FA3 += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                BA3 += Double.parseDouble(result.getString("IMSRPBILLAMT").replace(",", ""));

                ul.add(p);

            }

            IMSRP730 sp3 = new IMSRP730();
            sp3.setTransport("<b>TOTAL BY TRANSPORT : " + transport + "</b>");
            sp3.setCcy("<b>INVOICE COUNT : " + cin2 + "</b>");
            sp3.setForAmt("<b>" + formatDou.format(FA3) + "</b>");
            sp3.setBillAmt("<b>" + formatDou.format(BA3) + "</b>");
            ul.add(sp3);

            IMSRP730 sp = new IMSRP730();
            sp.setProd("<b>TOTAL BY PRODUCT : " + prod + "</b>");
            sp.setInvNo("<b>INVOICE COUNT : " + cin + "</b>");
            sp.setForAmt("<b>" + formatDou.format(FA) + "</b>");
            sp.setBillAmt("<b>" + formatDou.format(BA) + "</b>");
            ul.add(sp);

            IMSRP730 sp2 = new IMSRP730();
            sp2.setProd("<b>GRAND TOTAL BY MONTH : " + ym + "</b>");
            sp2.setForAmt("<b>" + formatDou.format(FA2) + "</b>");
            sp2.setBillAmt("<b>" + formatDou.format(BA2) + "</b>");
            ul.add(sp2);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCodePrint(String ym, String imc) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT LEFT([IMSRPPERIOD],4)+'/'+SUBSTRING(RIGHT([IMSRPPERIOD],2), \n"
                + "PATINDEX('%[^0]%', RIGHT([IMSRPPERIOD],2)+'.'), \n"
                + "LEN(RIGHT([IMSRPPERIOD],2))) AS [IMSRPPERIOD]\n"
                + "    ,ISNULL([IMSRPPROD],'') AS IMSRPPROD\n"
                + "    ,ISNULL([IMSRPTPCODE]+' : '+[IMSTRNAME],'') AS [IMSRPTPCODE]\n"
                + "    ,ISNULL([IMSRPVENDER]+' : '+[IMSVDNAME],'') AS [IMSRPVENDER]\n"
                + "    ,[IMSRPINVNO]\n"
                + "    ,[IMSRPCUR]\n"
                + "    ,FORMAT([IMSRPCUSTRATE],'#,#0.000000') as [IMSRPCUSTRATE]\n"
                + "    ,FORMAT([IMSRPFORAMT],'#,#0.00') as [IMSRPFORAMT]\n"
                + "    ,FORMAT([IMSRPBILLAMT],'#,#0.00') as [IMSRPBILLAMT]\n"
                + "FROM [IMSRP730]\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP730].[IMSRPVENDER]\n"
                + "LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP730].[IMSRPTPCODE]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + "ORDER BY [IMSRPPROD],[IMSRPTPCODE],[IMSRPVENDER],[IMSRPINVNO],[IMSRPCUR]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String transport = "ST";
            String ven = "";

            int cin = 0;
            int cin2 = 0;
            int cin3 = 0;
            Double FA = 0.00;
            Double BA = 0.00;
            Double FA2 = 0.00;
            Double BA2 = 0.00;
            Double FA3 = 0.00;
            Double BA3 = 0.00;

            int i = 0;

            while (result.next()) {

                i++;

                IMSRP730 p = new IMSRP730();

                if (i == 1) {
                    p.setPeriod(result.getString("IMSRPPERIOD"));
                }
                ym = result.getString("IMSRPPERIOD");

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("");

                    if (transport.equals(result.getString("IMSRPTPCODE"))) {
                        transport = result.getString("IMSRPTPCODE");
                        p.setTransport("");

                    } else {
                        if (!transport.equals("ST")) {
                            IMSRP730 sp = new IMSRP730();
                            sp.setTransport(transport + " Total" + "+ULINE");
                            sp.setVen("+ULINE");
                            sp.setInvNo("+ULINE");
                            sp.setCcy("+ULINE");
                            sp.setCustRate("+ULINE");
                            sp.setSum(Integer.toString(cin3) + "+ULINE");
                            sp.setForAmt(formatDou.format(FA3) + "+ULINE");
                            sp.setBillAmt(formatDou.format(BA3) + "+ULINE");
                            ul.add(sp);

                            cin3 = 0;
                            FA3 = 0.00;
                            BA3 = 0.00;
                        }

                        transport = result.getString("IMSRPTPCODE");
                        p.setTransport(result.getString("IMSRPTPCODE"));
                    }

                    if (ven.equals(result.getString("IMSRPVENDER"))) {
                        ven = result.getString("IMSRPVENDER");
                        p.setVen("");

                    } else {
                        ven = result.getString("IMSRPVENDER");
                        p.setVen(result.getString("IMSRPVENDER"));
                    }

                } else {
                    if (!transport.equals("ST")) {
                        IMSRP730 sp = new IMSRP730();
                        sp.setTransport(transport + " Total" + "+ULINE");
                        sp.setVen("+ULINE");
                        sp.setInvNo("+ULINE");
                        sp.setCcy("+ULINE");
                        sp.setCustRate("+ULINE");
                        sp.setSum(Integer.toString(cin3) + "+ULINE");
                        sp.setForAmt(formatDou.format(FA3) + "+ULINE");
                        sp.setBillAmt(formatDou.format(BA3) + "+ULINE");
                        ul.add(sp);

                        cin3 = 0;
                        FA3 = 0.00;
                        BA3 = 0.00;
                    }

                    if (!prod.equals("ST")) {
                        IMSRP730 sp = new IMSRP730();
                        sp.setProd(prod + " Total" + "+ULINE");
                        sp.setTransport("+ULINE");
                        sp.setVen("+ULINE");
                        sp.setInvNo("+ULINE");
                        sp.setCcy("+ULINE");
                        sp.setCustRate("+ULINE");
                        sp.setSum(Integer.toString(cin) + "+ULINE+CENTER");
                        sp.setForAmt(formatDou.format(FA) + "+ULINE");
                        sp.setBillAmt(formatDou.format(BA) + "+ULINE");
                        ul.add(sp);

                        cin = 0;
                        FA = 0.00;
                        BA = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    transport = result.getString("IMSRPTPCODE");
                    p.setTransport(result.getString("IMSRPTPCODE"));
                    ven = result.getString("IMSRPVENDER");
                    p.setVen(result.getString("IMSRPVENDER"));

                }

                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setCcy(result.getString("IMSRPCUR"));
                p.setCustRate(result.getString("IMSRPCUSTRATE"));
                p.setForAmt(result.getString("IMSRPFORAMT"));
                p.setBillAmt(result.getString("IMSRPBILLAMT"));

                cin += 1;
                cin2 += 1;
                cin3 += 1;
                FA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                BA += Double.parseDouble(result.getString("IMSRPBILLAMT").replace(",", ""));
                FA2 += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                BA2 += Double.parseDouble(result.getString("IMSRPBILLAMT").replace(",", ""));
                FA3 += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                BA3 += Double.parseDouble(result.getString("IMSRPBILLAMT").replace(",", ""));

                ul.add(p);

            }

            IMSRP730 sp3 = new IMSRP730();
            sp3.setTransport(transport + " Total" + "+ULINE");
            sp3.setVen("+ULINE");
            sp3.setInvNo("+ULINE");
            sp3.setCcy("+ULINE");
            sp3.setCustRate("+ULINE");
            sp3.setSum(Integer.toString(cin3) + "+ULINE");
            sp3.setForAmt(formatDou.format(FA3) + "+ULINE");
            sp3.setBillAmt(formatDou.format(BA3) + "+ULINE");
            ul.add(sp3);

            IMSRP730 sp = new IMSRP730();
            sp.setProd(prod + " Total" + "+ULINE");
            sp.setTransport("+ULINE");
            sp.setVen("+ULINE");
            sp.setInvNo("+ULINE");
            sp.setCcy("+ULINE");
            sp.setCustRate("+ULINE");
            sp.setSum(Integer.toString(cin) + "+ULINE+CENTER");
            sp.setForAmt(formatDou.format(FA) + "+ULINE");
            sp.setBillAmt(formatDou.format(BA) + "+ULINE");
            ul.add(sp);

            IMSRP730 sp2 = new IMSRP730();
            sp2.setPeriod(ym + " Total" + "+ULINE");
            sp2.setProd("+ULINE");
            sp2.setTransport("+ULINE");
            sp2.setVen("+ULINE");
            sp2.setInvNo("+ULINE");
            sp2.setCcy("+ULINE");
            sp2.setCustRate("+ULINE");
            sp2.setSum(Integer.toString(cin2) + "+ULINE+CENTER");
            sp2.setForAmt(formatDou.format(FA2) + "+ULINE");
            sp2.setBillAmt(formatDou.format(BA2) + "+ULINE");
            ul.add(sp2);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCode22Print(String ym, String imc) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT CCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFORAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBILLAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMBA\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSRPCUR] AS CCY\n"
                + "FROM [IMSRP730]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            IMSRP730 p1 = new IMSRP730();
            p1.setCcy("");
            ul.add(p1);

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setCcy(result.getString("CCY") + "+TLB");
                p.setCustRate("+TB");
                p.setSum("+TB");
                p.setForAmt(result.getString("SUMFA") + "+TLRB");
                p.setBillAmt(result.getString("SUMBA") + "+TLRB");

                ul.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCode22(String ym, String imc) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT CCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFORAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBILLAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMBA\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSRPCUR] AS CCY\n"
                + "FROM [IMSRP730]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            IMSRP730 p1 = new IMSRP730();
            p1.setCcy("");
            ul.add(p1);

            int i = 0;

            while (result.next()) {

                i++;

                IMSRP730 p = new IMSRP730();

                if (i == 1) {
                    p.setProd("<b>GRAND TOTAL BY CCY</b>");
                }

                p.setCcy("<b>" + result.getString("CCY") + "</b>");
                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setBillAmt("<b>" + result.getString("SUMBA") + "</b>");

                ul.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findForPrint(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT]\n"
                + "      ,[IMSVDNAME]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSTRNAME] as [IMSRPTRANSPORT]\n"
                + "      ,FORMAT([IMSRPINVDATE],'dd-MM-yyy') as [IMSRPINVDATE]\n"
                + "      ,FORMAT([IMSRPARRDATE],'dd-MM-yyy') as [IMSRPARRDATE]\n"
                + "      ,FORMAT([IMSRPDEBITDATE],'dd-MM-yyy') as [IMSRPDEBITDATE]\n"
                + "  FROM [IMSRP740]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP740].[IMSRPVENDEPT]\n"
                + "  LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP740].[IMSRPTRANSPORT]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  ORDER BY [IMSRPPERIOD], [IMSRPIMPNO], [IMSRPVENDEPT], [IMSRPINVNO], [IMSRPPROD], [IMSRPTRANSPORT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setProd(result.getString("IMSRPIMPNO"));
                p.setVen(result.getString("IMSRPVENDEPT"));
                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setCcy(result.getString("IMSRPPROD"));
                p.setCustRate(result.getString("IMSRPTRANSPORT"));
                p.setForAmt(result.getString("IMSRPINVDATE"));
                p.setBillAmt(result.getString("IMSRPARRDATE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findForPrint2(String year) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT (SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD) AS MF\n"
                + ",(SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD DESC) AS MT\n"
                + ",*\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPINVAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPPAYAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPDIFFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE LEFT(IMSRPPERIOD,4) = '" + year + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod("M." + result.getString("MF") + "-M." + result.getString("MT"));
                p.setCcy(result.getString("CCY"));

                p.setForAmt(result.getString("SUMFA"));
                p.setInvAmt(result.getString("SUMIA"));
                p.setPayAmt(result.getString("SUMPA"));
                p.setDiffAmt(result.getString("SUMDA"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP730 ([IMSRPCOM]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPTPCODE]\n"
                + "      ,[IMSRPVENDER]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPCUSTRATE]\n"
                + "      ,[IMSRPFORAMT]\n"
                + "      ,[IMSRPBILLAMT]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER]\n"
                + "      ,[IMSRPIMC])\n"
                + "SELECT 'TWC', FORMAT(IMSHTRDT,'yyyyMM'), IMSSDPROD, IMSHTPCODE, IMSSDVCODE, IMSSDINVNO, IMSSDCUR, \n"
                + "IMSSDEXRATE, IMSSDFINVAMT, IMSSDINVAMT, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "', IMSHIMC\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') = '" + ym + "' AND IMSHIMC = '" + imc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String imc) {

        String sql = "DELETE FROM IMSRP730 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
