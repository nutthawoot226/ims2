/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP760;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP760Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSRP760> findByCode(String imc, String ym) {

        List<IMSRP760> ul = new ArrayList<IMSRP760>();

        String sql = "SELECT *\n"
                + ",(SELECT (SELECT top 1[IMSADADAMT] FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as ADV\n"
                + ",(SELECT (SELECT SUM([IMSADESAMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as EST\n"
                + ",(SELECT (SELECT SUM([IMSADBLAMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as BILL\n"
                + ",(SELECT (SELECT SUM([IMSADTX3AMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as TAX3\n"
                + ",(SELECT (SELECT SUM([IMSADTX1AMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as TAX1\n"
                + ",(SELECT (SELECT SUM([IMSADNETAMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as NET\n"
                //                + ",(SELECT (SELECT MAX([IMSADTNTAMT]) FROM [IMSAPDETAIL] \n"
                //                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                //                + "FROM(\n"
                //                + "SELECT distinct [IMSADCQNO]\n"
                //                + "FROM [IMSAPDETAIL]\n"
                //                + "where [IMSADPDNO] = PDNO\n"
                //                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                //                + ")ADV) as SUMNET\n"
                //                + ",(SELECT (SELECT MIN([IMSADBALAMT]) FROM [IMSAPDETAIL] \n"
                //                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                //                + "FROM(\n"
                //                + "SELECT distinct [IMSADCQNO]\n"
                //                + "FROM [IMSAPDETAIL]\n"
                //                + "where [IMSADPDNO] = PDNO\n"
                //                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                //                + ")ADV) as REM\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSADIMC]\n"
                + "      ,[IMSIMNAME]\n"
                + "       ,[IMSAPSHCD]\n"
                + "	  ,[IMSSCNAME]\n"
                + "	  ,[IMSADPDNO] AS PDNO\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADBCODE]\n"
                + "	  ,FORMAT([IMSADCQDT],'dd-MM-yyyy') as [IMSADCQDT]\n"
                + "  FROM [IMSAPDETAIL]\n"
                + "  LEFT JOIN [IMSAPHEAD] ON [IMSAPHEAD].[IMSAPPDNO] = [IMSAPDETAIL].[IMSADPDNO]\n"
                + "  LEFT JOIN [IMSSHC] ON [IMSSHC].[IMSSCCODE] = [IMSAPHEAD].[IMSAPSHCD]\n"
                + "  LEFT JOIN [IMSIMC] ON [IMSIMC].[IMSIMCODE] = [IMSAPDETAIL].IMSADIMC\n"
                + "  WHERE [IMSADIMC] = '" + imc + "' AND LEFT([IMSADPDNO],4) = '" + ym + "'\n"
                + ")LEX\n"
                + "ORDER BY [IMSADIMC], [IMSIMNAME], [IMSSCNAME], PDNO, [IMSADCQNO], [IMSADBCODE], [IMSADCQDT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String ship = "ST";
            String ship2 = "ST";

            Double sumAdv = 0.00;
            Double sumEst = 0.00;
            Double sumBill = 0.00;
            Double sumTax3 = 0.00;
            Double sumTax1 = 0.00;
            Double sumNet = 0.00;
            Double sumSumNet = 0.00;
            Double sumRem = 0.00;
            Double sumSumNet2 = 0.00;
            Double sumRem2 = 0.00;

            Double sumAdva = 0.00;
            Double sumEsta = 0.00;
            Double sumBilla = 0.00;
            Double sumTax3a = 0.00;
            Double sumTax1a = 0.00;
            Double sumNeta = 0.00;
            Double sumSumNeta = 0.00;
            Double sumRema = 0.00;
            Double sumSumNeta2 = 0.00;
            Double sumRema2 = 0.00;

            while (result.next()) {

                IMSRP760 p = new IMSRP760();

                p.setImc(result.getString("IMSADIMC"));
                p.setPdno(result.getString("PDNO"));
                p.setCqno(result.getString("IMSADCQNO"));
                p.setBank(result.getString("IMSADBCODE"));
                p.setCqdt(result.getString("IMSADCQDT"));

                p.setAdv(formatDou.format(result.getDouble("ADV")));
                p.setEst(formatDou.format(result.getDouble("EST")));
                p.setBill(formatDou.format(result.getDouble("BILL")));
                p.setTax3(formatDou.format(result.getDouble("TAX3")));
                p.setTax1(formatDou.format(result.getDouble("TAX1")));
                p.setNet(formatDou.format(result.getDouble("NET")));

                if (ship2.equals("ST")) {
                    sumSumNet2 += result.getDouble("NET");
                } else {
                    if (ship2.equals(result.getString("IMSAPSHCD"))) {
                        sumSumNet2 += result.getDouble("NET");
                    } else {
                        sumSumNet2 = 0.00;
                        sumSumNet2 += result.getDouble("NET");
                    }
                }
                ship2 = result.getString("IMSAPSHCD");
                p.setSumnet(formatDou.format(sumSumNet2));

                p.setRem(formatDou.format(result.getDouble("ADV") - sumSumNet2));

                if (ship.equals(result.getString("IMSAPSHCD"))) {
                    ship = result.getString("IMSAPSHCD");
                    p.setShip("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSSCNAME") + "</b>");

                } else {
                    if (!ship.equals("ST")) {
                        IMSRP760 sp = new IMSRP760();
                        sp.setPdno("<b>Total</b>");
                        sp.setAdv("<b>" + formatDou.format(sumAdv) + "</b>");
                        sp.setEst("<b>" + formatDou.format(sumEst) + "</b>");
                        sp.setBill("<b>" + formatDou.format(sumBill) + "</b>");
                        sp.setTax3("<b>" + formatDou.format(sumTax3) + "</b>");
                        sp.setTax1("<b>" + formatDou.format(sumTax1) + "</b>");
                        sp.setNet("<b>" + formatDou.format(sumNet) + "</b>");
                        sp.setSumnet("<b>" + formatDou.format(sumNet) + "</b>");
                        sp.setRem("<b>" + formatDou.format(sumAdv - sumNet) + "</b>");
                        ul.add(sp);

                        sumAdv = 0.00;
                        sumEst = 0.00;
                        sumBill = 0.00;
                        sumTax3 = 0.00;
                        sumTax1 = 0.00;
                        sumNet = 0.00;
                        sumSumNet = 0.00;
                        sumRem = 0.00;
                    }

                    ship = result.getString("IMSAPSHCD");
                    p.setShip(result.getString("IMSSCNAME"));
                }

                sumAdv += Double.parseDouble(result.getString("ADV"));
                sumEst += Double.parseDouble(result.getString("EST"));
                sumBill += Double.parseDouble(result.getString("BILL"));
                sumTax3 += Double.parseDouble(result.getString("TAX3"));
                sumTax1 += Double.parseDouble(result.getString("TAX1"));
                sumNet += Double.parseDouble(result.getString("NET"));
                sumSumNet += Double.parseDouble(result.getString("NET"));
                sumRem += Double.parseDouble(result.getString("NET"));

                sumAdva += Double.parseDouble(result.getString("ADV"));
                sumEsta += Double.parseDouble(result.getString("EST"));
                sumBilla += Double.parseDouble(result.getString("BILL"));
                sumTax3a += Double.parseDouble(result.getString("TAX3"));
                sumTax1a += Double.parseDouble(result.getString("TAX1"));
                sumNeta += Double.parseDouble(result.getString("NET"));
                sumSumNeta += Double.parseDouble(result.getString("NET"));
                sumRema += Double.parseDouble(result.getString("NET"));

                ul.add(p);

            }

            IMSRP760 sp = new IMSRP760();
            sp.setPdno("<b>Total</b>");
            sp.setAdv("<b>" + formatDou.format(sumAdv) + "</b>");
            sp.setEst("<b>" + formatDou.format(sumEst) + "</b>");
            sp.setBill("<b>" + formatDou.format(sumBill) + "</b>");
            sp.setTax3("<b>" + formatDou.format(sumTax3) + "</b>");
            sp.setTax1("<b>" + formatDou.format(sumTax1) + "</b>");
            sp.setNet("<b>" + formatDou.format(sumNet) + "</b>");
            sp.setSumnet("<b>" + formatDou.format(sumNet) + "</b>");
            sp.setRem("<b>" + formatDou.format(sumAdv - sumNet) + "</b>");
            ul.add(sp);

            IMSRP760 sp2 = new IMSRP760();
            sp2.setPdno("<b>Grand Total</b>");
            sp2.setAdv("<b>" + formatDou.format(sumAdva) + "</b>");
            sp2.setEst("<b>" + formatDou.format(sumEsta) + "</b>");
            sp2.setBill("<b>" + formatDou.format(sumBilla) + "</b>");
            sp2.setTax3("<b>" + formatDou.format(sumTax3a) + "</b>");
            sp2.setTax1("<b>" + formatDou.format(sumTax1a) + "</b>");
            sp2.setNet("<b>" + formatDou.format(sumNeta) + "</b>");
            sp2.setSumnet("<b>" + formatDou.format(sumNeta) + "</b>");
            sp2.setRem("<b>" + formatDou.format(sumAdva - sumNeta) + "</b>");
            ul.add(sp2);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP760> findByCodePT(String imc, String ym) {

        List<IMSRP760> ul = new ArrayList<IMSRP760>();

        String sql = "SELECT *\n"
                + ",(SELECT (SELECT top 1[IMSADADAMT] FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as ADV\n"
                + ",(SELECT (SELECT SUM([IMSADESAMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as EST\n"
                + ",(SELECT (SELECT SUM([IMSADBLAMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as BILL\n"
                + ",(SELECT (SELECT SUM([IMSADTX3AMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as TAX3\n"
                + ",(SELECT (SELECT SUM([IMSADTX1AMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as TAX1\n"
                + ",(SELECT (SELECT SUM([IMSADNETAMT]) FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                + "FROM(\n"
                + "SELECT distinct [IMSADCQNO]\n"
                + "FROM [IMSAPDETAIL]\n"
                + "where [IMSADPDNO] = PDNO\n"
                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                + ")ADV) as NET\n"
                //                + ",(SELECT (SELECT MAX([IMSADTNTAMT]) FROM [IMSAPDETAIL] \n"
                //                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                //                + "FROM(\n"
                //                + "SELECT distinct [IMSADCQNO]\n"
                //                + "FROM [IMSAPDETAIL]\n"
                //                + "where [IMSADPDNO] = PDNO\n"
                //                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                //                + ")ADV) as SUMNET\n"
                //                + ",(SELECT (SELECT MIN([IMSADBALAMT]) FROM [IMSAPDETAIL] \n"
                //                + "  WHERE [IMSADPDNO] = PDNO AND [IMSADCQNO] = ADV.[IMSADCQNO]) AS ADV\n"
                //                + "FROM(\n"
                //                + "SELECT distinct [IMSADCQNO]\n"
                //                + "FROM [IMSAPDETAIL]\n"
                //                + "where [IMSADPDNO] = PDNO\n"
                //                + "AND [IMSADCQNO] = LEX.[IMSADCQNO]\n"
                //                + ")ADV) as REM\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSADIMC]\n"
                + "      ,[IMSIMNAME]\n"
                + "       ,[IMSAPSHCD]\n"
                + "	  ,[IMSSCNAME]\n"
                + "	  ,[IMSADPDNO] AS PDNO\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADBCODE]\n"
                + "	  ,FORMAT([IMSADCQDT],'dd-MM-yyyy') as [IMSADCQDT]\n"
                + "  FROM [IMSAPDETAIL]\n"
                + "  LEFT JOIN [IMSAPHEAD] ON [IMSAPHEAD].[IMSAPPDNO] = [IMSAPDETAIL].[IMSADPDNO]\n"
                + "  LEFT JOIN [IMSSHC] ON [IMSSHC].[IMSSCCODE] = [IMSAPHEAD].[IMSAPSHCD]\n"
                + "  LEFT JOIN [IMSIMC] ON [IMSIMC].[IMSIMCODE] = [IMSAPDETAIL].IMSADIMC\n"
                + "  WHERE [IMSADIMC] = '" + imc + "' AND LEFT([IMSADPDNO],4) = '" + ym + "'\n"
                + ")LEX\n"
                + "ORDER BY [IMSADIMC], [IMSIMNAME], [IMSSCNAME], PDNO, [IMSADCQNO], [IMSADBCODE], [IMSADCQDT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String ship = "ST";
            String ship2 = "ST";

            Double sumAdv = 0.00;
            Double sumEst = 0.00;
            Double sumBill = 0.00;
            Double sumTax3 = 0.00;
            Double sumTax1 = 0.00;
            Double sumNet = 0.00;
            Double sumSumNet = 0.00;
            Double sumRem = 0.00;
            Double sumSumNet2 = 0.00;
            Double sumRem2 = 0.00;

            Double sumAdva = 0.00;
            Double sumEsta = 0.00;
            Double sumBilla = 0.00;
            Double sumTax3a = 0.00;
            Double sumTax1a = 0.00;
            Double sumNeta = 0.00;
            Double sumSumNeta = 0.00;
            Double sumRema = 0.00;
            Double sumSumNeta2 = 0.00;
            Double sumRema2 = 0.00;

            while (result.next()) {

                IMSRP760 p = new IMSRP760();

                p.setImc(result.getString("IMSADIMC"));
                p.setPdno(result.getString("PDNO"));
                p.setCqno(result.getString("IMSADCQNO"));
                p.setBank(result.getString("IMSADBCODE"));
                p.setCqdt(result.getString("IMSADCQDT"));

                p.setAdv(formatDou.format(result.getDouble("ADV")));
                p.setEst(formatDou.format(result.getDouble("EST")));
                p.setBill(formatDou.format(result.getDouble("BILL")));
                p.setTax3(formatDou.format(result.getDouble("TAX3")));
                p.setTax1(formatDou.format(result.getDouble("TAX1")));
                p.setNet(formatDou.format(result.getDouble("NET")));

                if (ship2.equals("ST")) {
                    sumSumNet2 += result.getDouble("NET");
                } else {
                    if (ship2.equals(result.getString("IMSAPSHCD"))) {
                        sumSumNet2 += result.getDouble("NET");
                    } else {
                        sumSumNet2 = 0.00;
                        sumSumNet2 += result.getDouble("NET");
                    }
                }
                ship2 = result.getString("IMSAPSHCD");
                p.setSumnet(formatDou.format(sumSumNet2));

                p.setRem(formatDou.format(result.getDouble("ADV") - sumSumNet2));

                if (ship.equals(result.getString("IMSAPSHCD"))) {
                    ship = result.getString("IMSAPSHCD");
                    p.setShip("");

                } else {
                    if (!ship.equals("ST")) {
                        IMSRP760 sp = new IMSRP760();
                        sp.setPdno("Total");
                        sp.setAdv(formatDou.format(sumAdv));
                        sp.setEst(formatDou.format(sumEst));
                        sp.setBill(formatDou.format(sumBill));
                        sp.setTax3(formatDou.format(sumTax3));
                        sp.setTax1(formatDou.format(sumTax1));
                        sp.setNet(formatDou.format(sumNet));
                        sp.setSumnet(formatDou.format(sumNet));
                        sp.setRem(formatDou.format(sumAdv - sumNet));
                        ul.add(sp);

                        sumAdv = 0.00;
                        sumEst = 0.00;
                        sumBill = 0.00;
                        sumTax3 = 0.00;
                        sumTax1 = 0.00;
                        sumNet = 0.00;
                        sumSumNet = 0.00;
                        sumRem = 0.00;
                    }

                    ship = result.getString("IMSAPSHCD");
                    p.setShip(result.getString("IMSSCNAME"));
                }

                sumAdv += Double.parseDouble(result.getString("ADV"));
                sumEst += Double.parseDouble(result.getString("EST"));
                sumBill += Double.parseDouble(result.getString("BILL"));
                sumTax3 += Double.parseDouble(result.getString("TAX3"));
                sumTax1 += Double.parseDouble(result.getString("TAX1"));
                sumNet += Double.parseDouble(result.getString("NET"));
                sumSumNet += Double.parseDouble(result.getString("NET"));
                sumRem += Double.parseDouble(result.getString("NET"));

                sumAdva += Double.parseDouble(result.getString("ADV"));
                sumEsta += Double.parseDouble(result.getString("EST"));
                sumBilla += Double.parseDouble(result.getString("BILL"));
                sumTax3a += Double.parseDouble(result.getString("TAX3"));
                sumTax1a += Double.parseDouble(result.getString("TAX1"));
                sumNeta += Double.parseDouble(result.getString("NET"));
                sumSumNeta += Double.parseDouble(result.getString("NET"));
                sumRema += Double.parseDouble(result.getString("NET"));

                ul.add(p);

            }

            IMSRP760 sp = new IMSRP760();
            sp.setPdno("Total");
            sp.setAdv(formatDou.format(sumAdv));
            sp.setEst(formatDou.format(sumEst));
            sp.setBill(formatDou.format(sumBill));
            sp.setTax3(formatDou.format(sumTax3));
            sp.setTax1(formatDou.format(sumTax1));
            sp.setNet(formatDou.format(sumNet));
            sp.setSumnet(formatDou.format(sumNet));
            sp.setRem(formatDou.format(sumAdv - sumNet));
            ul.add(sp);

            IMSRP760 sp2 = new IMSRP760();
            sp2.setPdno("Grand Total");
            sp2.setAdv(formatDou.format(sumAdva));
            sp2.setEst(formatDou.format(sumEsta));
            sp2.setBill(formatDou.format(sumBilla));
            sp2.setTax3(formatDou.format(sumTax3a));
            sp2.setTax1(formatDou.format(sumTax1a));
            sp2.setNet(formatDou.format(sumNeta));
            sp2.setSumnet(formatDou.format(sumNeta));
            sp2.setRem(formatDou.format(sumAdva - sumNeta));
            ul.add(sp2);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }
}
