/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSAPHEADDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSAPHEAD> findAll() {

        List<IMSAPHEAD> UAList = new ArrayList<IMSAPHEAD>();

        String sql = "SELECT IMSAPPDNO, IMSAPSHCD + ' : ' + IMSSCNAME AS IMSAPSHCD, \n"
                + "	   IMSAPUSER + ' : ' + USERS AS IMSAPUSER\n"
                + "FROM IMSAPHEAD\n"
                + "LEFT JOIN IMSSHC ON IMSSHC.IMSSCCODE = IMSAPHEAD.IMSAPSHCD\n"
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = IMSAPHEAD.IMSAPUSER\n"
                + "ORDER BY IMSAPPDNO";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSAPHEAD p = new IMSAPHEAD();
//
                p.setPaymentNo(result.getString("IMSAPPDNO"));
                p.setShipCom(result.getString("IMSAPSHCD"));
//                p.setCheckNo(result.getString("CHEQUENO"));
//                p.setBank(result.getString("BANK"));
//
//                p.setDnNo(result.getString("DNNO"));
//
//                if (result.getString("DNDATE") != null) {
//                    p.setDnDate(result.getString("DNDATE").split(" ")[0]);
//                }
                p.setUser(result.getString("IMSAPUSER"));
//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSAPHEAD findByCode(String code) {

        IMSAPHEAD p = new IMSAPHEAD();

        String sql = "SELECT IMSAPPDNO, IMSAPSHCD, \n"
                + "	   IMSAPUSER + ' : ' + USERS AS IMSAPUSER\n"
                + "FROM IMSAPHEAD\n"
                + "LEFT JOIN IMSSHC ON IMSSHC.IMSSCCODE = IMSAPHEAD.IMSAPSHCD\n"
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = IMSAPHEAD.IMSAPUSER\n"
                + "WHERE IMSAPPDNO = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//
                p.setPaymentNo(result.getString("IMSAPPDNO"));
                p.setShipCom(result.getString("IMSAPSHCD"));
//                p.setCheckNo(result.getString("CHEQUENO"));
//                p.setBank(result.getString("BANK"));
//
//                p.setDnNo(result.getString("DNNO"));
//
//                if (result.getString("DNDATE") != null) {
//                    p.setDnDate(result.getString("DNDATE").split(" ")[0]);
//                }
                p.setUser(result.getString("IMSAPUSER"));
//

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSAPHEAD] where [IMSAPPDNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String advPaidNo, String shipCom, String userid) {

        boolean result = false;

        String sql = "INSERT INTO IMSAPHEAD"
                + " VALUES(?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, shipCom);

            ps.setString(3, advPaidNo);

            ps.setString(4, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String advPaidNo, String shipCom, String userid) {

        boolean result = false;

        String sql = "UPDATE IMSAPHEAD "
                + "SET IMSAPSHCD = ?, IMSAPEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSAPPDNO = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, shipCom);

            ps.setString(2, advPaidNo);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }
}
