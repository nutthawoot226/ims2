/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP710Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSPDETAIL\n"
                + "  LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHIMNO = IMSPDETAIL.IMSPDIMNO\n"
                + "  WHERE FORMAT(IMSPDPDDT, 'yyyyMM') = '" + ym + "'\n"
                + "  AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP710> findByCode(String ym, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPDEPT]\n"
                + "      ,[IMSRPBCODE]\n"
                + "      ,FORMAT([IMSRPPAYDATE],'dd-MM-yyyy') AS [IMSRPPAYDATE]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT(ISNULL([IMSRPPAYRATE],0),'#,#0.000000') AS [IMSRPPAYRATE]\n"
                + "      ,FORMAT(ISNULL([IMSRPIMPRATE],0),'#,#0.000000') AS [IMSRPIMPRATE]\n"
                + "      ,FORMAT(ISNULL([IMSRPFAMT],0),'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPINVAMT],0),'#,#0.00') AS [IMSRPINVAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPPAYAMT],0),'#,#0.00') AS [IMSRPPAYAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPDIFFAMT],0),'#,#0.00') AS [IMSRPDIFFAMT]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP710]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPERIOD],[IMSRPDEPT],[IMSRPBCODE],[IMSRPPAYDATE],[IMSRPCCY],[IMSRPPAYRATE],[IMSRPIMPRATE]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String dept = "";
            String bank = "";
            String payDate = "";
            String ccy = "";
            String payRate = "";
            String impRate = "";

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod(result.getString("IMSRPPERIOD"));

                if (dept.equals(result.getString("IMSRPDEPT"))) {
                    dept = result.getString("IMSRPDEPT");
                    p.setDept("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPDEPT") + "</b>");

                    if (bank.equals(result.getString("IMSRPBCODE"))) {
                        bank = result.getString("IMSRPBCODE");
                        p.setBank("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPBCODE") + "</b>");

                        if (payDate.equals(result.getString("IMSRPPAYDATE"))) {
                            payDate = result.getString("IMSRPPAYDATE");
                            p.setPayDate("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPPAYDATE") + "</b>");

                            if (ccy.equals(result.getString("IMSRPCCY"))) {
                                ccy = result.getString("IMSRPCCY");
                                p.setCcy("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPCCY") + "</b>");

                                if (payRate.equals(result.getString("IMSRPPAYRATE"))) {
                                    payRate = result.getString("IMSRPPAYRATE");
                                    p.setPayRate("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPPAYRATE") + "</b>");

                                    if (impRate.equals(result.getString("IMSRPIMPRATE"))) {
                                        impRate = result.getString("IMSRPIMPRATE");
                                        p.setImpRate("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPIMPRATE") + "</b>");
                                    } else {
                                        impRate = result.getString("IMSRPIMPRATE");
                                        p.setImpRate(result.getString("IMSRPIMPRATE"));
                                    }
                                } else {
                                    payRate = result.getString("IMSRPPAYRATE");
                                    p.setPayRate(result.getString("IMSRPPAYRATE"));
                                    impRate = result.getString("IMSRPIMPRATE");
                                    p.setImpRate(result.getString("IMSRPIMPRATE"));
                                }
                            } else {
                                ccy = result.getString("IMSRPCCY");
                                p.setCcy(result.getString("IMSRPCCY"));
                                payRate = result.getString("IMSRPPAYRATE");
                                p.setPayRate(result.getString("IMSRPPAYRATE"));
                                impRate = result.getString("IMSRPIMPRATE");
                                p.setImpRate(result.getString("IMSRPIMPRATE"));
                            }
                        } else {
                            payDate = result.getString("IMSRPPAYDATE");
                            p.setPayDate(result.getString("IMSRPPAYDATE"));
                            ccy = result.getString("IMSRPCCY");
                            p.setCcy(result.getString("IMSRPCCY"));
                            payRate = result.getString("IMSRPPAYRATE");
                            p.setPayRate(result.getString("IMSRPPAYRATE"));
                            impRate = result.getString("IMSRPIMPRATE");
                            p.setImpRate(result.getString("IMSRPIMPRATE"));
                        }
                    } else {
                        bank = result.getString("IMSRPBCODE");
                        p.setBank(result.getString("IMSRPBCODE"));
                        payDate = result.getString("IMSRPPAYDATE");
                        p.setPayDate(result.getString("IMSRPPAYDATE"));
                        ccy = result.getString("IMSRPCCY");
                        p.setCcy(result.getString("IMSRPCCY"));
                        payRate = result.getString("IMSRPPAYRATE");
                        p.setPayRate(result.getString("IMSRPPAYRATE"));
                        impRate = result.getString("IMSRPIMPRATE");
                        p.setImpRate(result.getString("IMSRPIMPRATE"));
                    }

                } else {
                    dept = result.getString("IMSRPDEPT");
                    p.setDept(result.getString("IMSRPDEPT"));
                    bank = result.getString("IMSRPBCODE");
                    p.setBank(result.getString("IMSRPBCODE"));
                    payDate = result.getString("IMSRPPAYDATE");
                    p.setPayDate(result.getString("IMSRPPAYDATE"));
                    ccy = result.getString("IMSRPCCY");
                    p.setCcy(result.getString("IMSRPCCY"));
                    payRate = result.getString("IMSRPPAYRATE");
                    p.setPayRate(result.getString("IMSRPPAYRATE"));
                    impRate = result.getString("IMSRPIMPRATE");
                    p.setImpRate(result.getString("IMSRPIMPRATE"));
                }

                p.setForAmt(result.getString("IMSRPFAMT"));
                p.setInvAmt(result.getString("IMSRPINVAMT"));
                p.setPayAmt(result.getString("IMSRPPAYAMT"));
                p.setDiffAmt(result.getString("IMSRPDIFFAMT"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findForPrint(String year, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT LEFT(IMSRPPERIOD,4)+'/'+RIGHT(IMSRPPERIOD,2) AS IMSRPPERIOD\n"
                + "      ,[IMSRPDEPT]\n"
                + "      ,isnull(IMSRPBCODE,'') as [IMSRPBCODE]\n"
                + "      ,FORMAT([IMSRPPAYDATE],'dd-MM-yyyy') AS [IMSRPPAYDATE]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT(ISNULL([IMSRPPAYRATE],0),'#,#0.000000') AS [IMSRPPAYRATE]\n"
                + "      ,FORMAT(ISNULL([IMSRPIMPRATE],0),'#,#0.000000') AS [IMSRPIMPRATE]\n"
                + "      ,FORMAT(ISNULL([IMSRPFAMT],0),'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPINVAMT],0),'#,#0.00') AS [IMSRPINVAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPPAYAMT],0),'#,#0.00') AS [IMSRPPAYAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPDIFFAMT],0),'#,#0.00') AS [IMSRPDIFFAMT]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP710]\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPERIOD],[IMSRPDEPT],[IMSRPBCODE],[IMSRPPAYDATE],[IMSRPCCY],[IMSRPPAYRATE],[IMSRPIMPRATE]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod(result.getString("IMSRPPERIOD").split("/")[0] + "/" + Integer.parseInt(result.getString("IMSRPPERIOD").split("/")[1]));
                p.setDept(result.getString("IMSRPDEPT"));
                p.setBank(result.getString("IMSRPBCODE"));
                p.setPayDate(result.getString("IMSRPPAYDATE"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setPayRate(result.getString("IMSRPPAYRATE"));
                p.setImpRate(result.getString("IMSRPIMPRATE"));

                p.setForAmt(result.getString("IMSRPFAMT"));
                p.setInvAmt(result.getString("IMSRPINVAMT"));
                p.setPayAmt(result.getString("IMSRPPAYAMT"));
                p.setDiffAmt(result.getString("IMSRPDIFFAMT"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findForPrint2(String year, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT (SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD) AS MF\n"
                + ",(SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD DESC) AS MT\n"
                + ",*\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPINVAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPPAYAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPDIFFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE LEFT(IMSRPPERIOD,4) = '" + year + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod("M." + result.getString("MF") + "-M." + result.getString("MT"));
                p.setCcy(result.getString("CCY"));

                p.setForAmt(result.getString("SUMFA"));
                p.setInvAmt(result.getString("SUMIA"));
                p.setPayAmt(result.getString("SUMPA"));
                p.setDiffAmt(result.getString("SUMDA"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym, String imc) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "AND [IMSRPIMC] = '" + imc + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP710 ([IMSRPCOM]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPDEPT]\n"
                + "      ,[IMSRPBCODE]\n"
                + "      ,[IMSRPPAYDATE]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPPAYRATE]\n"
                + "      ,[IMSRPIMPRATE]\n"
                + "      ,[IMSRPFAMT]\n"
                + "      ,[IMSRPINVAMT]\n"
                + "      ,[IMSRPPAYAMT]\n"
                + "      ,[IMSRPDIFFAMT]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER]\n"
                + "      ,[IMSRPIMC])\n"
                + "SELECT 'TWC' AS COM, '" + ym + "' AS PERIOD, IMSPDPROD, IMSPDBCODE, IMSPDPDDT\n"
                + ",IMSPDCUR, IMSPDEXRATE\n"
                + ",(SELECT TOP 1 IMSSDEXRATE FROM IMSSDETAIL WHERE IMSSDPIMNO = IMSPDIMNO \n"
                + "AND IMSSDINVNO = IMSPDINVNO) AS IMPRATE, IMSPDFINVAMT, IMSPDINVAMT\n"
                + ",IMSPDPDAMT, (ISNULL(IMSPDINVAMT,0) - ISNULL(IMSPDPDAMT,0)) AS DIFF\n"
                + ",CURRENT_TIMESTAMP AS EDT, CURRENT_TIMESTAMP AS CDT, '" + userid + "' AS USERID, IMSHIMC\n"
                + "FROM IMSPDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSPDETAIL.IMSPDIMNO\n"
                + "WHERE FORMAT(IMSPDPDDT, 'yyyyMM') = '" + ym + "' AND IMSHIMC = '" + imc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String imc) {

        String sql = "DELETE FROM IMSRP710 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
