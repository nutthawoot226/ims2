<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=button] {
                width: 100%;
                background-color: #c10000;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [15]},
//                        {orderable: false, targets: [6]},
//                        {orderable: false, targets: [7]},
//                        {orderable: false, targets: [8]},
//                        {orderable: false, targets: [9]},
//                        {orderable: false, targets: [10]},
                        {"width": "6%", "targets": 3},
                        {"width": "6%", "targets": 7}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function Validate() {
                var f = document.getElementById("checkNo").value;
                var sc = document.getElementById("shipcom").value;

                if (f === "" || sc === "") {
                    document.getElementById('myModal-6').style.display = 'block';
                    return false;
                }
            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        </script>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;"> 
                    <center>
                        <form action="edit" method="post">
                            <table width="70%">
                                <tr>
                                    <th><b>CHEQUE NO. :</b></th>
                                    <th>
                                        <input type="text" id="checkNo" name="checkNo" value="${checkNo}" style=" height: 30px; width: 70%;" disabled>
                                        <input type="hidden" id="allCheckNo" name="allCheckNo" value="${allCheckNo}">
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><b>ADV PAID NO.</b>
                                        <input type="text" id="no" name="no" value="${cur}" style=" height: 30px; width: 150px;" onfocus="this.blur();">
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="1"><b>SHIPPING COMPANY :</b></th>
                                    <th colspan="3">
                                        <select id="shipcom" name="shipcom" style=" height: 30px;" disabled>
                                            <option value="${shipcode}" selected hidden>${shipcode} : ${shipname} (${shipnamet})</option>
                                            <c:forEach items="${shipList}" var="x">
                                                <option value="${x.code}">${x.code} : ${x.name} (${x.namet})</option>
                                            </c:forEach>
                                        </select>
                                    </th>
                                    <th style=" width: 100px;"></th>
                                    <th>
                                        <!--<button type="submit" style=" width: 115px;" onclick="return Validate();">Enter</button>-->
                                        <button type="button" style=" width: 115px;" onclick="window.location.href = 'display'">Back</button>
                                    </th>
                                </tr>
                            </table>
                        </form>
                    </center>
                    <form id="formData" name="formData" action="add" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" id="advPaidNo" name="advPaidNo" value="${cur}">
                        <input type="hidden" id="checkNo" name="checkNo" value="${checkNo}">
                        <input type="hidden" id="allCheckNo" name="allCheckNo" value="${allCheckNo}">
                        <input type="hidden" id="shipCom" name="shipCom" value="${shipcode}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">NO.</th>
                                    <th style="text-align: center;">CHEQUE NO.</th>
                                    <th style="text-align: center;">BANK</th>
                                    <th style="text-align: center;">CHEQUE DATE</th>
                                    <th style="text-align: center;">ADVANCE</th>
                                    <th style="text-align: center;">ESTIMATE ADVANCE</th>
                                    <th style="text-align: center;">D/N NO.</th>
                                    <th style="text-align: center;">D/N DATE</th>
                                    <th style="text-align: center;">IMP COMP</th>
                                    <th style="text-align: center;">INVOICE NO. LIST</th>
                                    <th style="text-align: center;">BILL AMOUNT</th>
                                    <th style="text-align: center;">TAX 3%</th>
                                    <th style="text-align: center;">TAX 1%</th>
                                    <th style="text-align: center;">AMOUNT</th>
                                    <th style="text-align: center;">(TOT CHQ AMT) ADVANCE</th>
                                    <th style="text-align: center;">(REMAIN CHQ AMT) BALANCE</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot> 
                            <tbody>
                                <c:forEach items="${detList}" var="x">
                                    <tr>
                                        <td style="text-align: center;">${x.no}<input type="hidden" id="no-${x.no}" name="no" value="${x.no}"><input type="hidden" name="pimno" value="${x.pimno}"></td>
                                        <td>${x.checkNo}<input type="hidden" id="checkNoData-${x.no}" name="checkNoData" value="${x.checkNoD}"></td>
                                        <td>${x.bank}<input type="hidden" id="bank-${x.no}" name="bank" value="${x.bankD}"></td>
                                        <td>${x.checkDate}<input type="hidden" id="checkDate-${x.no}" name="checkDate" value="${x.checkDateD}"></td>
                                        <td style="text-align: right;">${x.advance}<input type="hidden" id="advance-${x.no}" name="advance" value="${x.advanceD}"></td>
                                        <td style="text-align: right;">${x.estAdv}<input type="hidden" id="estAdv-${x.no}" name="estAdv" value="${x.estAdv}"></td>
                                        <td>${x.dnNo}<input type="hidden" id="dnNo-${x.no}" name="dnNo" value="${x.dnNo}"></td>
                                        <td>${x.dnDate}<input type="hidden" id="dnDate-${x.no}" name="dnDate" value="${x.dnDate}"></td>
                                        <td>${x.imcD}<input type="hidden" id="imcD-${x.no}" name="imcD" value="${x.imcD}"></td>
                                        <td>${x.invNoList}<input type="hidden" id="invNoList-${x.no}" name="invNoList" value="${x.invNoList}"></td>
                                        <td style="text-align: right;">${x.billAmt}<input type="hidden" id="billAmt-${x.no}" name="billAmt" value="${x.billAmt}"></td>
                                        <td style="text-align: right;">${x.tax3}<input type="hidden" id="tax3-${x.no}" name="tax3" value="${x.tax3}"></td>
                                        <td style="text-align: right;">${x.tax1}<input type="hidden" id="tax1-${x.no}" name="tax1" value="${x.tax1}"></td>
                                        <td style="text-align: right;">${x.amount}<input type="hidden" id="amount-${x.no}" name="amount" value="${x.amount}"></td>
                                        <td style="text-align: right;">${x.totAdv}<input type="hidden" id="totAdv-${x.no}" name="totAdv" value="${x.totAdv}"></td>
                                        <td style="text-align: right;">${x.balance}<input type="hidden" id="balance-${x.no}" name="balance" value="${x.balance}"></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${detList2}" var="x">
                                    <tr>
                                        <td style="text-align: center;">${x.no}<input type="hidden" id="no-${x.no}" name="no" value="${x.no}"><input type="hidden" name="pimno" value="${x.pimno}"></td>
                                        <td>${x.checkNo}<input type="hidden" id="checkNoData-${x.no}" name="checkNoData" value="${x.checkNoD}"></td>
                                        <td>${x.bank}<input type="hidden" id="bank-${x.no}" name="bank" value="${x.bankD}"></td>
                                        <td>${x.checkDate}<input type="hidden" id="checkDate-${x.no}" name="checkDate" value="${x.checkDateD}"></td>
                                        <td style="text-align: right;">${x.advance}<input type="hidden" id="advance-${x.no}" name="advance" value="${x.advanceD}"></td>
                                        <td style="text-align: right;">${x.estAdv}<input type="hidden" id="estAdv-${x.no}" name="estAdv" value="${x.estAdv}"></td>
                                        <td>${x.dnNo}<input type="hidden" id="dnNo-${x.no}" name="dnNo" value="${x.dnNo}"></td>
                                        <td>${x.dnDate}<input type="hidden" id="dnDate-${x.no}" name="dnDate" value="${x.dnDate}"></td>
                                        <td>${x.imcD}<input type="hidden" id="imcD-${x.no}" name="imcD" value="${x.imcD}"></td>
                                        <td>${x.invNoList}<input type="hidden" id="invNoList-${x.no}" name="invNoList" value="${x.invNoList}"></td>
                                        <td style="text-align: right;">${x.billAmt}<input type="hidden" id="billAmt-${x.no}" name="billAmt" value="${x.billAmt}"></td>
                                        <td style="text-align: right;">${x.tax3}<input type="hidden" id="tax3-${x.no}" name="tax3" value="${x.tax3}"></td>
                                        <td style="text-align: right;">${x.tax1}<input type="hidden" id="tax1-${x.no}" name="tax1" value="${x.tax1}"></td>
                                        <td style="text-align: right;">${x.amount}<input type="hidden" id="amount-${x.no}" name="amount" value="${x.amount}"></td>
                                        <td style="text-align: right;">${x.totAdv}<input type="hidden" id="totAdv-${x.no}" name="totAdv" value="${x.totAdv}"></td>
                                        <td style="text-align: right;">${x.balance}<input type="hidden" id="balance-${x.no}" name="balance" value="${x.balance}"></td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <th><b style="opacity: 0;">999</b></th>
                                    <th></th>
                                    <th></th>
                                    <th>${grt}</th>
                                    <th style="text-align: right;">${sumadv}</th>
                                    <th style="text-align: right;">${sumestAdv}</th>
                                    <th></th> 
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align: right;">${sumbillAmt}</th>
                                    <th style="text-align: right;">${sumtax3}</th>
                                    <th style="text-align: right;">${sumtax1}</th>
                                    <th style="text-align: right;">${sumamount}</th>
                                    <th style="text-align: right;">${sumtotAdv}</th>
                                    <th style="text-align: right;">${sumbalance}</th>
                                </tr>
                            </tbody>
                        </table>
                        <center>
                            ${save}
                        </center>
                    </form>
                    <div id="myModal-3" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Are you sure ?</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="document.getElementById('formData').submit();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-6" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 220px; width: 800px;">
                            <span id="span-6" class="close" onclick="document.getElementById('myModal-6').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please fill out</font></b>
                                        <br>
                                        <b style="color: #00399b;"><font size="5">CHEQUE NO. and SHIPPING COMPANY !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-6').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="header" id="myHeader">
                        <table style="width:98%; border-bottom: 1px solid black;">
                            <tr>
                                <th style="text-align: center; width: 1%;">NO.</th>
                                <th style="text-align: center; width: 4%;">CHEQUE NO.</th>
                                <th style="text-align: center; width: 4%;">BANK</th>
                                <th style="text-align: center; width: 5%;">CHEQUE DATE</th>
                                <th style="text-align: center; width: 4%;">ADVANCE</th>
                                <th style="text-align: center; width: 3%;">ESTIMATE ADVANCE</th>
                                <th style="text-align: center; width: 3%;">D/N NO.</th>
                                <th style="text-align: center; width: 3%;">D/N DATE</th>
                                <th style="text-align: center; width: 3%;">INVOICE NO. LIST</th>
                                <th style="text-align: right; width: 5%;">BILL AMOUNT</th>
                                <th style="text-align: center; width: 5%;">TAX 3%</th>
                                <th style="text-align: left; width: 3%;">TAX 1%</th>
                                <th style="text-align: left; width: 3%;">AMOUNT</th>
                                <th style="text-align: left; width: 5%;">(TOT CHQ AMT) ADVANCE</th>
                                <th style="text-align: left; width: 5%;">(REMAIN CHQ AMT) BALANCE</th>
                            </tr>
                        </table>
                    </div>
                    <script>
                        window.onscroll = function () {
                            myFunction();
                        };

                        var header = document.getElementById("myHeader");
                        var sticky = header.offsetTop;

                        function myFunction() {
                            if (window.pageYOffset > sticky) {
                                header.classList.add("sticky");
                            } else {
                                header.classList.remove("sticky");
                            }
                        }
                    </script>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>