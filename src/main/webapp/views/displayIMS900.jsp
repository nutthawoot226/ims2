<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            #sts0 {
                width: 40px;
                background-color: #0e50ba;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts1 {
                width: 40px;
                background-color: #002c72;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts2 {
                width: 40px;
                background-color: #28d65f;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts5 {
                width: 40px;
                background-color: #5e00b7;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts6 {
                width: 40px;
                background-color: #00af89;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts7 {
                width: 40px;
                background-color: #002aa0;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts8 {
                width: 40px;
                background-color: #d61717;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #sts9 {
                width: 40px;
                background-color: #d61717;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsC {
                width: 40px;
                background-color: #000000;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsD {
                width: 40px;
                background-color: #ff9400;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsH {
                width: 40px;
                background-color: #12a9b7;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            #stsR {
                width: 40px;
                background-color: #ff9400;
                color: white;
                padding: 5px 5px;
                /*margin: 8px 0;*/
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
                    fixedColumns: true,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [3]},
//                        {orderable: false, targets: [4]},
//                        {orderable: false, targets: [5]},
                        {orderable: false, targets: [6]},
                        {orderable: false, targets: [7]},
                        {orderable: false, targets: [8]},
                        {orderable: false, targets: [9]},
                        {orderable: false, targets: [10]},
                        {orderable: false, targets: [11]},
                        {orderable: false, targets: [12]},
                        {orderable: false, targets: [13]},
                        {orderable: false, targets: [14]},
//                        {"width": "12%", "targets": 0},
//                        {"width": "12%", "targets": 1},
//                        {"width": "30%", "targets": 2},
//                        {"width": "9%", "targets": 3},
                        {"width": "5%", "targets": 6},
//                        {"width": "9%", "targets": 5},
                        {"width": "8%", "targets": 8},
//                        {"width": "9%", "targets": 7},
//                        {"width": "9%", "targets": 8},
//                        {"width": "9%", "targets": 9},
//                        {"width": "9%", "targets": 10},
//                        {"width": "9%", "targets": 11}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)")
                        .not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function checkSortBy() {

                var sortBy = document.getElementById("sortBy");
                var options = "";
                options += '<option value="" />';

                if (sortBy.value === "impCom") {
            <c:forEach items="${ImpComList}" var="x">
                    options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                            } else if (sortBy.value === "venCom") {
            <c:forEach items="${VenComList}" var="x">
                                options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                                        } else if (sortBy.value === "venDep") {
            <c:forEach items="${VenDepList}" var="x">
                                            options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                                                    } else if (sortBy.value === "shipCom") {
            <c:forEach items="${ShipComList}" var="x">
                                                        options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                                                                } else if (sortBy.value === "proGrp") {
            <c:forEach items="${ProGrpList}" var="x">
                                                                    options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                                                                            } else if (sortBy.value === "prv") {
            <c:forEach items="${PrvList}" var="x">
                                                                                options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                                                                                        } else if (sortBy.value === "sts") {
            <c:forEach items="${StsList}" var="x">
                                                                                            options += '<option value="' + "${x.code} : ${x.name}" + '" >' + "${x.code} : ${x.name}" + '</option>';
            </c:forEach>
                                                                                                    }

                                                                                                    document.getElementById("dataL").innerHTML = options;
                                                                                                }
        </script>
        <script>
            function subRow(qno, head, pimno) {
                var tr = document.getElementsByTagName('tr');
                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].style.display === "") {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "none";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                            document.getElementById('pimno-' + pimno).style.cssText = "color: black;";
                            document.getElementById('imno-' + pimno).style.cssText = "color: black;";
                        }
                    } else {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                            document.getElementById('pimno-' + pimno).style.cssText = "color: white;";
                            document.getElementById('imno-' + pimno).style.cssText = "color: white;";
                        }
                    }
                }
            }

            function expand() {
                document.getElementById("exp").style.display = "none";
                document.getElementById("com").style.display = "";
                var tr = document.getElementsByTagName('tr');
                var a = document.getElementsByTagName('a');
                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #003682; color: white;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "";
                    }
                }

                for (var i = 0; i < a.length; i++) {
                    if (a[i].id.toString().split("-")[0] === "pimno" || a[i].id.toString().split("-")[0] === "imno") {
                        a[i].style.cssText = "color: white;";
                    }
                }
            }

            function compress() {
                document.getElementById("exp").style.display = "";
                document.getElementById("com").style.display = "none";
                var tr = document.getElementsByTagName('tr');
                var a = document.getElementsByTagName('a');
                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "none";
                    }
                }

                for (var i = 0; i < a.length; i++) {
                    if (a[i].id.toString().split("-")[0] === "pimno" || a[i].id.toString().split("-")[0] === "imno") {
                        a[i].style.cssText = "color: black;";
                    }
                }
            }

            function hide() {
                document.getElementById("hide").style.display = "none";
                document.getElementById("show").style.display = "";
                document.getElementById("frm").style.display = "none";
            }

            function show() {
                document.getElementById("hide").style.display = "";
                document.getElementById("show").style.display = "none";
                document.getElementById("frm").style.display = "";
            }
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>

    </head>    
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMS900.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left">
                                <a style=" width: 100px; height: 35px;" class="btn btn-danger" id="hide" name="hide" onclick="hide();">
                                    <i class="fa fa-window-close-o" style="font-size:15px;"></i>&nbsp;&nbsp; Hide</a>
                                <a style=" width: 100px; height: 35px; display: none;" class="btn btn-success" id="show" name="show" onclick="show();">
                                    <i class="fa fa-window-maximize" style="font-size:15px;"></i>&nbsp;&nbsp; Show</a>
                            </td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <table width="100%">
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Year / Month : </b>
                                    <input style="width:100px;" type="text" placeholder="เช่น 201906" name="ym" id="ym" value="${ym}" maxlength="6" onchange="window.location.href = '/IMS2/IMS900/display?ym=' + this.value + '&ym2=' + document.getElementById('ym2').value;">
                                    -
                                    <input style="width:100px;" type="text" placeholder="เช่น 201906" name="ym2" id="ym2" value="${ym2}" maxlength="6" onchange="window.location.href = '/IMS2/IMS900/display?ym=' + document.getElementById('ym').value + '&ym2=' + this.value;">
                                </td>
                                <td align="right"><b style="color: #00399b;">Selected By : </b>
                                    <select name="sortBy" id="sortBy" style="width:250px; height: 34px;" onchange="document.getElementById('data').value = '';
                                            checkSortBy();">
                                        <option value="${sortByValue}" selected hidden>${sortBy}</option>
                                        <option value="impCom">1 : IMPORT COMPANY</option>
                                        <option value="venCom">2 : VENDOR COMPANY</option>
                                        <option value="venDep">3 : VENDOR DEPARTMENT</option>
                                        <option value="shipCom">4 : SHIPPING COMPANY</option>
                                        <option value="proGrp">5 : PRODUCT GROUP</option>
                                        <option value="prv">6 : PRIVILEGE</option>
                                        <option value="sts">7 : STATUS</option>
                                    </select>
                                </td>
                                <td align="right" width="360px"> 
                                    <input type="text" name="data" id="data" value="${data}" list="dataL" style="width:350px; height: 34px;" onchange="this.form.submit();">
                                    <datalist id="dataL">
                                        <c:forEach items="${MainList}" var="x">
                                            <option value="${x.code} : ${x.name}">${x.code} : ${x.name}</option>
                                        </c:forEach>
                                    </datalist>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr id="dont">
                                <th style="text-align: center;">PRE-IMP</th>
                                <th style="text-align: left;">IMPORT NO.</th>
                                <th style="text-align: left;">INVOICE NO.</th>
                                <th style="text-align: left;">PRODUCT</th>
                                <th style="text-align: left;">AWB</th>
                                <th style="text-align: left;">IMP ENTRY NO.</th>
                                <th style="text-align: center;">
                                    <a style=" width: 150px;" class="btn btn-primary" id="exp" name="exp" onclick="expand();">
                                        <i class="fa fa-expand" style="font-size:20px;"></i>&nbsp;&nbsp; 
                                        Expand All</a>
                                    <a style=" width: 150px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress();">
                                        <i class="fa fa-compress" style="font-size:20px;"></i>&nbsp;&nbsp; 
                                        Collapse All</a>
                                </th>
                                <th style="text-align: center;"></th>
                                <th style="text-align: center;"></th>
                                <th style="text-align: center; background-color: #fcfcfc;">IMPORT ENTRY</th>
                                <th style="text-align: center;">COST CALCULATION</th>
                                <th style="text-align: center; background-color: #fcfcfc;">ADVANCE SETTING</th>
                                <th style="text-align: center;">ADVANCE PAYMENT</th>
                                <th style="text-align: center; background-color: #fcfcfc;">IMPORT SETTING</th>
                                <th style="text-align: center;">IMPORT PAYMENT</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr id="dont">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${headList}" var="x">
                                <tr id="H-${x.pimNo}" style="cursor: pointer; background-color: white;">
                                    <td style="text-align: center;"><a id="pimno-${x.pimNo}" style="color: black;" href="/IMS/IMS100/edit?code=${x.pimNo}" target="_blank">${x.pimNo}</a></td>
                                    <td><a id="imno-${x.pimNo}" style="color: black;" href="/IMS/IMS100/edit?code=${x.pimNo}" target="_blank">${x.impNo}</a></td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">
                                        <div style=" opacity: 0; font-size: 1px;">
                                            <c:forEach items="${x.detailList}" var="p">
                                                ${p.invNo};
                                            </c:forEach>
                                        </div>
                                    </td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">
                                        <div style=" opacity: 0; font-size: 1px;">
                                            <c:forEach items="${x.detailList}" var="p">
                                                ${p.prod};
                                            </c:forEach>
                                        </div>
                                    </td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.awb}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.enno}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.imc}&nbsp;${x.imcD}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');" style=" text-align: right;">${x.prv}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.prvD}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');" style=" text-align: right;">${x.ven}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.venD}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');" style=" text-align: right;">${x.ship}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.shipD}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');" style=" text-align: right;">${x.user}</td>
                                    <td onclick="subRow('D-${x.pimNo}', 'H-${x.pimNo}', '${x.pimNo}');">${x.userD}</td>
                                </tr>
                                <c:forEach items="${x.detailList}" var="p">
                                    <tr id="D-${x.pimNo}" style="display: none;">
                                        <td><div style=" opacity: 0;">${x.pimNo}</div></td>
                                        <td><div style=" opacity: 0;">${x.impNo}</div></td>
                                        <td style=" width: 150px;">${p.invNo}</td>
                                        <td style=" width: 150px;">${p.prod}</td>
                                        <td><div style=" opacity: 0;">${x.awb}</div></td>
                                        <td><div style=" opacity: 0;">${x.enno}</div></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="text-align: center;"><i title="Import Entry" class="fa fa-sign-in" aria-hidden="true" style="font-size:30px; color: #9a00a8; ${p.sts100}"></i></td>
                                        <td style="text-align: center;"><i title="Cost Calculation" class="fa fa-calculator" aria-hidden="true" style="font-size:30px; color: #008cff; ${p.sts101}"></i></td>
                                        <td style="text-align: center;"><i title="Advance Setting" class="fa fa-cog" aria-hidden="true" style="font-size:30px; color: #2f363d; ${p.sts110}"></i></td>
                                        <td style="text-align: center;"><i title="Advance Payment" class="fa fa-money" aria-hidden="true" style="font-size:30px; color: #147c0b; ${p.sts200}"></i></td>
                                        <td style="text-align: center;"><i title="Import Setting" class="fa fa-cogs" aria-hidden="true" style="font-size:30px; color: #2f363d; ${p.sts300}"></i></td>
                                        <td style="text-align: center;"><i title="Import Payment" class="fa fa-usd" aria-hidden="true" style="font-size:30px; color: #009623; ${p.sts301}"></i></td>
                                    </tr>
                                </c:forEach>
                            </c:forEach>
                        </tbody>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
            <div class="header" id="myHeader">
                <table style="width:100%; border-bottom: 1px solid black;">
                    <tr>
                        <th style="text-align: center; width: 103px;">PRE-IMP</th>
                        <th style="text-align: center; width: 148px;">IMPORT NO.</th>
                        <th style="text-align: center; width: 155px;">INVOICE NO.</th>
                        <th style="text-align: center; width: 470px;"></th>
                        <th style="text-align: center; background-color: #fcfcfc; height: 65px;">IMPORT<br>ENTRY</th>
                        <th style="text-align: center;">COST<br>CALCULATION</th>
                        <th style="text-align: center; background-color: #fcfcfc;">ADVANCE<br>SETTING</th>
                        <th style="text-align: center;">ADVANCE<br>PAYMENT</th>
                        <th style="text-align: center; background-color: #fcfcfc;">IMPORT<br>SETTING</th>
                        <th style="text-align: center;">IMPORT<br>PAYMENT</th>
                    </tr>
                </table>
            </div>
        </div> <!-- end #wrapper -->
        <script>
            window.onscroll = function () {
                myFunction();
            };
            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;
            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        </script>
    </body>
</html>