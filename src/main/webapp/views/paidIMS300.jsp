<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=button] {
                width: 100%;
                background-color: #c10000;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [6]},
//                        {orderable: false, targets: [6]},
//                        {orderable: false, targets: [7]},
//                        {orderable: false, targets: [8]},
//                        {orderable: false, targets: [9]},
//                        {orderable: false, targets: [10]},
//                        {"width": "6%", "targets": 3},
//                        {"width": "6%", "targets": 7}
                    ]
                });

                $('#tableDIS').DataTable({
                    "paging": false,
                    "ordering": false,
                    columnDefs: [
                        {targets: 0, className: 'dt-body-right'},
                        {targets: 1, className: 'dt-body-right'},
                        {targets: 2, className: 'dt-body-right'}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(6)").each(function () {
                    var title = $(this).text();
                    if (title !== "") {
                        $(this).html('<input type="text" id="' + title + '" style="text-align: right;" onfocus="this.blur();"/>');
                    }
//                    else {
//                        $(this).html('<input type="text" />');
//                    }

                });

                // DataTable
//                var table = $('#showTable').DataTable();

                // Apply the search
//                table.columns().every(function () {
//                    var that = this;
//                    $('input', this.footer()).on('keyup change', function () {
//                        if (that.search() !== this.value) {
//                            that
//                                    .search(this.value)
//                                    .draw();
//                        }
//                    });
//                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }

            Cornfirm = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                        }
                    }
                }

                if (document.getElementById('PRT').value === "") {
                    document.getElementById('myModal-5').style.display = 'block';
                } else {
                    if (nodataSelect) {
                        document.getElementById('formData').action = 'paid';
                        document.getElementById('formData').method = 'post';
                        document.getElementById('formData').submit();
                    } else {
                        document.getElementById('myModal-4').style.display = 'block';
                    }
                }


            };

            function currencyFormat(num) {
                num = Math.round(num * 100) / 100;
                return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function SUM() {
                var selectAll = document.getElementById("selectAll");

                if (selectAll.checked === true) {
                    var ia = document.getElementsByName("ia");
                    var sumia = 0;
                    for (var i = 0; i < ia.length; i++) {
                        if (ia[i].value.toString().trim() === "") {
                            sumia += 0;
                        } else {
                            sumia += parseFloat(ia[i].value.replace(/,/g, ''));
                        }

                        document.getElementById("bia-" + i).style.cssText = "opacity: 1;";
                    }

                    var fia = document.getElementsByName("fia");
                    var sumfia = 0;
                    for (var i = 0; i < fia.length; i++) {
                        if (fia[i].value.toString().trim() === "") {
                            sumfia += 0;
                        } else {
                            sumfia += parseFloat(fia[i].value.replace(/,/g, ''));
                        }

                        document.getElementById("bfia-" + i).style.cssText = "opacity: 1;";
                    }

                    document.getElementById('sumInvAmt').value = currencyFormat(parseFloat(sumia));
                    document.getElementById('sumForInvAmt').value = currencyFormat(parseFloat(sumfia));
                    document.getElementById('sumIA').value = currencyFormat(parseFloat(sumia));
                    document.getElementById('sumFIA').value = currencyFormat(parseFloat(sumfia));

                    var est = document.getElementById('INIT').value.replace(/,/g, '');
                    document.getElementById('remainingAmountTMP').value = currencyFormat(parseFloat(est) - parseFloat(sumfia));
                    document.getElementById('remainingAmount').value = currencyFormat(parseFloat(est) - parseFloat(sumfia));

                } else {
                    var ia = document.getElementsByName("ia");
                    for (var i = 0; i < ia.length; i++) {
                        document.getElementById("bia-" + i).style.cssText = "opacity: 0;";
                    }

                    var fia = document.getElementsByName("fia");
                    for (var i = 0; i < fia.length; i++) {
                        document.getElementById("bfia-" + i).style.cssText = "opacity: 0;";
                    }

                    document.getElementById('sumInvAmt').value = currencyFormat(0);
                    document.getElementById('sumForInvAmt').value = currencyFormat(0);
                    document.getElementById('sumIA').value = currencyFormat(0);
                    document.getElementById('sumFIA').value = currencyFormat(0);

                    var est = document.getElementById('INIT').value.replace(/,/g, '');
                    document.getElementById('remainingAmountTMP').value = currencyFormat(parseFloat(est));
                    document.getElementById('remainingAmount').value = currencyFormat(parseFloat(est));

                }

            }

            function subSUM(index) {
                var ck = document.getElementById("ck-" + index);
                var bia = document.getElementById("bia-" + index);
                var bfia = document.getElementById("bfia-" + index);

                if (ck.checked === true) {
                    bia.style.cssText = "opacity: 1;";
                    bfia.style.cssText = "opacity: 1;";

                } else {
                    bia.style.cssText = "opacity: 0;";
                    bfia.style.cssText = "opacity: 0;";

                }
            }

            function RateIn() {
                var rate = document.getElementById("PRT").value;

                var fia = document.getElementsByName("ofia");
                for (var i = 0; i < fia.length; i++) {
                    var ind = fia[i].id.toString().split("-")[1];

                    document.getElementById("bia-" + ind).innerHTML = currencyFormat(parseFloat(fia[i].value.replace(/,/g, '')) * parseFloat(rate));
                    document.getElementById("ia-" + ind).value = parseFloat(fia[i].value.replace(/,/g, '')) * parseFloat(rate);

                    document.getElementById("ck-" + ind).value = document.getElementById("ckt-" + ind).value + ("x" + (parseFloat(fia[i].value.replace(/,/g, '')) * parseFloat(rate)));

                    document.getElementById("bfia-" + ind).innerHTML = currencyFormat(parseFloat(fia[i].value.replace(/,/g, '')));
                    document.getElementById("fia-" + ind).value = currencyFormat(parseFloat(fia[i].value.replace(/,/g, '')));

                    document.getElementById("ck-" + ind).value += ("x" + parseFloat(fia[i].value.replace(/,/g, '')));
                }
            }

            function SUM2() {
                var ck = document.getElementsByName("selectCk");
                var sumia = 0;
                var sumfia = 0;
                for (var i = 0; i < ck.length; i++) {
                    if (ck[i].checked === true) {
                        var ia = document.getElementById("ia-" + i);
                        if (ia.value.toString().trim() === "") {
                            sumia += 0;
                        } else {
                            sumia += parseFloat(ia.value.replace(/,/g, ''));
                        }

                        var fia = document.getElementById("fia-" + i);
                        if (fia.value.toString().trim() === "") {
                            sumfia += 0;
                        } else {
                            sumfia += parseFloat(fia.value.replace(/,/g, ''));
                        }
                    }
                }

                document.getElementById('sumInvAmt').value = currencyFormat(parseFloat(sumia));
                document.getElementById('sumForInvAmt').value = currencyFormat(parseFloat(sumfia));
                document.getElementById('sumIA').value = currencyFormat(parseFloat(sumia));
                document.getElementById('sumFIA').value = currencyFormat(parseFloat(sumfia));

                var est = document.getElementById('INIT').value.replace(/,/g, '');
                document.getElementById('remainingAmountTMP').value = currencyFormat(parseFloat(est) - parseFloat(sumfia));
                document.getElementById('remainingAmount').value = currencyFormat(parseFloat(est) - parseFloat(sumfia));
            }

            var lineNo = ${lineNo};
            function addDIS() {
                lineNo += 1;
                var table = $('#tableDIS').DataTable();
                table.row.add([
                    '<input type="text" style="text-align: right;" id="famt-' + lineNo + '" name="famt" onchange="CalAmt(' + lineNo + ');">'
                            , '<input type="text" style="text-align: right;" id="rate-' + lineNo + '" name="rate" onchange="CalAmt(' + lineNo + ');">'
                            , '<input type="text" style="text-align: right;" id="amt-' + lineNo + '" name="amt" readonly>'
                            , '<a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="DeleteDIS(this)">Delete</a>'
                ]).draw(false);
            }

            function CalAmt(line) {
                var famt = document.getElementById('famt-' + line).value.replace(/,/g, '');
                document.getElementById('famt-' + line).value = currencyFormat(parseFloat(famt));

                var rate = document.getElementById('rate-' + line).value.replace(/,/g, '');

                document.getElementById('amt-' + line).value = currencyFormat(parseFloat(famt) * parseFloat(rate));
            }

            function DeleteDIS(x) {
                var table = $('#tableDIS').DataTable();
                table.row($(x).parents('tr')).remove().draw();
            }
        </script>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid'); document.getElementById('userid2').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;"> 
                    <form action="edit" method="post">
                        <input type="hidden" id="feeTMP" name="feeTMP" value="${fee}">
                        <table>
                            <tr width="100%">
                                <th width="12%" style="text-align: left;"><b>TOTAL EST PAID AMOUNT : </b></th>
                                <th width="13%" style="text-align: left;">
                                    <input type="text" id="totalEstPaidAmountTMP" name="totalEstPaidAmountTMP" value="${totalEstPaidAmount}" style=" height: 30px; width: 250px; text-align: right;" onchange="this.value = currencyFormat(parseFloat(this.value.replace(/,/g, '')));
                                            document.getElementById('totalEstPaidAmount').value = this.value;" disabled>
                                </th>
                                <th width="3%" style="text-align: left;"><b>CCY : </b></th>
                                <th width="8%" style="text-align: left;">
                                    <select id="ccyTMP" name="ccyTMP" style=" height: 30px; width: 100px;" onchange="document.getElementById('ccy').value = this.value;" disabled>
                                        <option value="${ccy}" selected hidden>${ccy}</option>
                                        <c:forEach items="${ccyList}" var="x">
                                            <option value="${x.code}">${x.code}</option>
                                        </c:forEach>
                                    </select>
                                </th>
                                <th width="5%" style="text-align: left;"><b>PAID BY : </b></th>
                                <th width="10%" style="text-align: left;">
                                    <select id="paidByTMP" name="paidByTMP" style=" height: 30px; width: 200px;" onchange="document.getElementById('paidBy').value = this.value;" disabled>
                                        <option value="${pbcode}" selected hidden>${pbcode} : ${pbname}</option>
                                        <c:forEach items="${pbList}" var="x">
                                            <option value="${x.code}">${x.code} : ${x.name}</option>
                                        </c:forEach>
                                    </select>                               
                                </th>
                                <th width="5%" style="text-align: left;"><b>PAID DATE : </b></th>
                                <th width="10%" style="text-align: left;">
                                    <input type="date" id="paidDateTMP" name="paidDateTMP" value="${paidDate}" style=" height: 30px; width: 150px;" onchange="document.getElementById('paidDate').value = this.value;">
                                </th>
                                <th width="3%" style="text-align: left;"><b>BANK : </b></th>
                                <th width="10%" style="text-align: left;">
                                    <input type="text" id="bankTMP" name="bankTMP" list="bankTMPList" value="${bankcode}" onchange="document.getElementById('bank').value = this.value;">
                                    <datalist id="bankTMPList">       
                                        <c:forEach items="${bankList}" var="x">
                                            <option value="${x.code}">${x.code} : ${x.name}</option>
                                        </c:forEach>
                                    </datalist>  
                                </th>
                                <th width="10%" style="text-align: center;">
                                    <button type="button" style=" width: 115px;" onclick="window.location.href = 'display'">Close</button>
                                </th>
                            </tr>

                            <tr width="100%">
                                <th style="text-align: left;"><b>VENDOR : </b></th>
                                <th colspan="3" style="text-align: left;">
                                    <select id="vendorTMP" name="vendorTMP" style=" height: 30px; width: 400px;" onchange="document.getElementById('vendor').value = this.value;" disabled> 
                                        <option value="${vendorcode}" selected hidden>${vendorcode} : ${vendorname}</option>
                                        <c:forEach items="${vendorList}" var="x">
                                            <option value="${x.code}">${x.code} : ${x.name}</option>
                                        </c:forEach>
                                    </select>
                                </th>
                                <th width="5%" style="text-align: left;"><b>REMAINING AMOUNT : </b></th>
                                <th width="10%" style="text-align: left;">
                                    <input type="text" id="remainingAmountTMP" name="remainingAmountTMP" value="${remainingAmount}" onfocus="this.blur();" style=" height: 30px; width: 150px; text-align: right;" >
                                </th>
                                <th width="5%" style="text-align: left;">
                                    <!--<button type="button" style=" width: 115px; background-color: #008220;" onclick="this.form.submit();">Enter</button>-->
                                </th>
                                <th colspan="2" style="text-align: right;"><b>IMPORT PAID NO. : </b></th>
                                <th width="10%" style="text-align: left;">
                                    <input type="text" id="importPaidNoTMP" name="importPaidNoTMP" value="${importPaidNo}" style=" height: 30px; width: 200px;" onfocus="this.blur();" onchange="document.getElementById('importPaidNo').value = this.value;">
                                </th>
                                <th width="10%" style="text-align: center;">
                                    <button type="button" style=" width: 115px; background-color: #001384;" onclick="Cornfirm();">Paid</button>
                                </th>
                            </tr>
                        </table>
                    </form>
                    <hr>
                    <div style="${sb}">
                        <table style=" width: 100%;">
                            <tr>
                                <td width="90%">
                                    <b>Selected By : </b><select id="curVen" name="curVen" style=" height: 30px; width: 500px;" onchange="window.location.href = 'paid?impPaidNo=${importPaidNo}&ven=' + this.value">
                                        <option value="${vcode}" selected hidden>${vcode} : ${vname}</option>
                                        <c:forEach items="${venList}" var="x">
                                            <option value="${x.code}">${x.code} : ${x.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <th style="text-align: center;" width="10%">
                                    <button type="button" style="width: 150px; background-color: #00b53c;" onclick="document.getElementById('myModal-6').style.display = 'block';">Distribute Paid</button>
                                </th>
                            </tr>
                        </table>
                    </div>
                    <form id="formData" name="formData" action="add" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" id="totalEstPaidAmount" name="totalEstPaidAmount" value="${totalEstPaidAmount}">
                        <input type="hidden" id="INIT" name="INIT" value="${remainingAmount}">
                        <input type="hidden" id="ccy" name="ccy" value="${ccy}">
                        <input type="hidden" id="paidBy" name="paidBy" value="${pbcode}">
                        <input type="hidden" id="paidDate" name="paidDate" value="${paidDate}">
                        <input type="hidden" id="bank" name="bank" value="${bankcode}">
                        <input type="hidden" id="vendor" name="vendor" value="${vendorcode}">
                        <input type="hidden" id="remainingAmount" name="remainingAmount" value="${remainingAmount}">
                        <input type="hidden" id="importPaidNo" name="importPaidNo" value="${importPaidNo}">
                        <input type="hidden" id="sumIA" name="sumIA">
                        <input type="hidden" id="sumFIA" name="sumFIA">
                        <input type="hidden" id="vendel" name="vendel" value="${vcode}">
                        <p id="gg"></p>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th colspan="7" style="border-bottom: none; text-align: right;">
                                        <a href="print2?impPaidNo=${importPaidNo}" target="_blank"><i class="fa fa-print" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                    </th>
                                    <th colspan="2" style="text-align: center;">TOTAL PAID</th> 
                                </tr>
                                <tr>
                                    <th style="text-align: center;">IMPORT NO.</th>
                                    <th style="text-align: center;">INVOICE NO.</th>
                                    <th style="text-align: center;">CCY</th>
                                    <th style="text-align: center;">PRODUCT</th>
                                    <th style="text-align: center;">INV AMOUNT (฿)</th>
                                    <th style="text-align: center;">FOREIGN INV AMOUNT</th>
                                    <th style="text-align: center;">
                                        ALL
                                        <input style="width: 20px; height: 20px;" type="checkbox" id="selectAll" name="selectAll" onchange="checkAll(this);
                                                SUM();">
                                    </th>
                                    <th style="text-align: center;">INV AMOUNT (฿)</th>
                                    <th style="text-align: center;">FOREIGN INV AMOUNT</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style=" text-align: center;">PAID RATE : <input type="text" id="PRT" name="PRT" value="${PRT}" style=" height: 30px; width: 150px; text-align: right;" onchange="RateIn();
                                            SUM2();"></th> 
                                    <th>sumInvAmt</th>
                                    <th>sumForInvAmt</th>
                                </tr>
                            </tfoot> 
                            <tbody>
                                <c:forEach varStatus="ii" items="${detList}" var="x">
                                    <tr>
                                        <td>${x.importNo}<input type="hidden" name="importNoL" value="${x.imp}"></td>
                                        <td>${x.invoiceNo}<input type="hidden" name="invoiceNoL" value="${x.invoiceNo}"></td>
                                        <td>${x.ccy}<input type="hidden" name="ccyL" value="${x.ccy}"></td>
                                        <td>${x.product}<input type="hidden" name="productL" value="${x.product}"></td>
                                        <td style=" text-align: right;">${x.invAmount}<input type="hidden" name="oia" value="${x.invAmount}">&nbsp;&nbsp;&nbsp;</td>
                                        <td style=" text-align: right;">${x.foreignInvAmount}<input type="hidden" id="ofia-${ii.index}" name="ofia" value="${x.foreignInvAmount}">&nbsp;&nbsp;&nbsp;</td>
                                        <td style=" text-align: center;">
                                            <input style="width: 20px; height: 20px;" type="checkbox" id="ck-${ii.index}" name="selectCk" value="${ii.index}-${x.line}-${x.imp}" onchange="subSUM(${ii.index});
                                                    SUM2();"><input type="hidden" id="ckt-${ii.index}" name="ckt" value="${ii.index}-${x.line}-${x.imp}">
                                        </td>
                                        <td style=" text-align: right;"><b id="bia-${ii.index}" style=" opacity: 0;"></b><input type="hidden" id="ia-${ii.index}" name="ia">&nbsp;&nbsp;&nbsp;</td>
                                        <td style=" text-align: right;"><b id="bfia-${ii.index}" style=" opacity: 0;"></b><input type="hidden" id="fia-${ii.index}" name="fia">&nbsp;&nbsp;&nbsp;<input type="hidden" name="INDB" value="YES"><input type="hidden" name="bankYES" value="${x.bank}"><input type="hidden" name="paidDateYES" value="${x.paidDate}"><input type="hidden" name="vendorYES" value="${x.vendor}"></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <table style="width:96%;">
                            <tr>
                                <th style="text-align: right; width: 75%;">Transfer Fees :</th>
                                <th style="text-align: left; width: 25%;"> 
                                    &nbsp;&nbsp;
                                    <input type="text" name="fee" id="fee" value="${fee}" style=" width: 45%; text-align: right;" onchange="this.value = currencyFormat(parseFloat(this.value.replace(/,/g, '')));
                                            document.getElementById('feeTMP').value = this.value;">
                                </th>
                            </tr>
                        </table>
                    </form>
                    <div id="myModal-3" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Are you sure ?</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="document.getElementById('formData').submit();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-4" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please select the data !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-5" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please fill in PAID RATE !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-5').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="myModal-6" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: fit-content; width: fit-content;">
                            <span id="span-6" class="close" onclick="document.getElementById('myModal-6').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <center>
                                <b style="color: #00399b;"><font size="5">Distribute Paid</font></b>
                            </center>
                            <form id="formDIS" target="_blank" action="print3" method="post">
                                <input type="hidden" name="impPaidNo" value="${importPaidNo}">
                                <input type="hidden" id="userid2" name="userid">
                                <table id="tableDIS" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: right;">FOREIGN INV AMOUNT</th>
                                            <th style="text-align: right;">PAID RATE</th>
                                            <th style="text-align: right;">TOTAL AMOUNT</th>
                                            <th style="text-align: center;">
                                                <a onclick="addDIS();" style=" cursor: pointer;"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${DISlist}" var="x">
                                            <tr>
                                                <td>
                                                    <input type="text" style="text-align: right;" id="famt-${x.line}" name="famt" value="${x.famt}" onchange="CalAmt(${x.line});">
                                                </td>
                                                <td>
                                                    <input type="text" style="text-align: right;" id="rate-${x.line}" name="rate" value="${x.rate}" onchange="CalAmt(${x.line});">
                                                </td>
                                                <td>
                                                    <input type="text" style="text-align: right;" id="amt-${x.line}" name="amt" value="${x.amt}" readonly>
                                                </td>
                                                <td>
                                                    <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="DeleteDIS(this)">Delete</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </form>
                            <center>
                                <a style=" width: 100px;" class="btn btn btn-outline btn-success" onclick="document.getElementById('formDIS').submit(); window.location.reload();">
                                    Finish
                                </a>
                            </center>
                        </div>
                    </div>
                    <div class="header" id="myHeader">
                        <table style="width:95%; border-bottom: 1px solid black;">
                            <tr>
                                <th colspan="7" style="border-bottom: none;"></th>
                                <th colspan="2" style="text-align: center;">TOTAL PAID</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;">IMPORT NO.</th>
                                <th style="text-align: center;">INVOICE NO.</th>
                                <th style="text-align: center;">CCY</th>
                                <th style="text-align: center;">PRODUCT</th>
                                <th style="text-align: center;">INV AMOUNT (฿)</th>
                                <th style="text-align: center;">FOREIGN INV AMOUNT</th>
                                <th style="text-align: center;">
                                    ALL
                                    <input style="width: 20px; height: 20px;" type="checkbox" name="selectAll" onchange="checkAll(this);
                                            SUM();">
                                </th>
                                <th style="text-align: center;">INV AMOUNT (฿)</th>
                                <th style="text-align: center;">FOREIGN INV AMOUNT</th>
                            </tr>
                        </table>
                    </div>
                    <script>
                        window.onscroll = function () {
                            myFunction();
                        };

                        var header = document.getElementById("myHeader");
                        var sticky = header.offsetTop;

                        function myFunction() {
                            if (window.pageYOffset > sticky) {
                                header.classList.add("sticky");
                            } else {
                                header.classList.remove("sticky");
                            }
                        }
                    </script>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>