<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            i[id=ic]:hover {
                background-color: #042987;
                border-radius: 15px;
            }

            i[id=ic2]:hover {
                background-color: #165910;
                border-radius: 15px;
            }

            button[type=button] {
                width: 100%;
                background-color: #c10000;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    fixedColumns: true,
//                    "order": [[0, "desc"]],
//                    "columnDefs": [
//                        {orderable: false, targets: [3]},
//                        {orderable: false, targets: [4]},
//                        {orderable: false, targets: [5]},
//                        {orderable: false, targets: [6]},
//                        {orderable: false, targets: [7]},
//                        {orderable: false, targets: [8]},
//                        {orderable: false, targets: [9]},
//                        {orderable: false, targets: [10]},
//                        {orderable: false, targets: [11]},
//                        {"width": "12%", "targets": 0},
//                        {"width": "12%", "targets": 1},
//                        {"width": "30%", "targets": 2},
//                        {"width": "9%", "targets": 3},
//                        {"width": "5%", "targets": 4},
//                        {"width": "9%", "targets": 5},
//                        {"width": "8%", "targets": 6},
//                        {"width": "9%", "targets": 7},
//                        {"width": "9%", "targets": 8},
//                        {"width": "9%", "targets": 9},
//                        {"width": "9%", "targets": 10},
//                        {"width": "9%", "targets": 11}
//                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function currencyFormat(num) {
                return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function ccy(x) {
                x.value = currencyFormat(parseFloat(x.value.replace(/,/g, '')));
            }

            function CalRate(rate, ccy) {
                var ipTIA = document.getElementById('input-totalImpAmt-' + ccy);
                var ipTIAB = document.getElementById('input-totalImpAmtB-' + ccy);
                var TIAB = document.getElementById('totalImpAmtB-' + ccy);

                var ACC = document.getElementById('accInvAmt-' + ccy);
                var ipACC = document.getElementById('input-accInvAmt-' + ccy);
                var ipBAL = document.getElementById('input-balAmtPrevMonth-' + ccy);

                var NOP = document.getElementById('noPaymentAmt-' + ccy);
                var ipNOP = document.getElementById('input-noPaymentAmt-' + ccy);
                var ipTOT = document.getElementById('input-total-' + ccy);

                ipTIAB.value = parseFloat(ipTIA.value.replace(/,/g, '')) * parseFloat(rate.value.replace(/,/g, ''));
                TIAB.innerHTML = currencyFormat(parseFloat(ipTIA.value.replace(/,/g, '')) * parseFloat(rate.value.replace(/,/g, '')));

                ipACC.value = parseFloat(ipTIA.value.replace(/,/g, '')) + parseFloat(ipBAL.value.replace(/,/g, ''));
                ACC.innerHTML = currencyFormat(parseFloat(ipTIA.value.replace(/,/g, '')) + parseFloat(ipBAL.value.replace(/,/g, '')));

                ipNOP.value = parseFloat(ipACC.value.replace(/,/g, '')) - parseFloat(ipTOT.value.replace(/,/g, ''));
                NOP.innerHTML = currencyFormat(parseFloat(ipACC.value.replace(/,/g, '')) - parseFloat(ipTOT.value.replace(/,/g, '')));

                SUM();
            }

            function SUM() {
                var input = document.getElementsByTagName('input');
                var sumTiab = 0;

                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === "totalImpAmtB") {
                        if (input[i].value.toString().trim() === "") {
                            sumTiab += 0;
                        } else {
                            sumTiab += parseFloat(input[i].value);
                        }
                    }
                }

                document.getElementById('sumTIAB').innerHTML = currencyFormat(sumTiab);
            }
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid'); SUM();">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <table width="100%">
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Import Company : </b>
                                    <input style="width:400px;" type="text" name="imc" id="imc" value="${imc} : ${imcName}">
                                </td>
                                <td align="right"></td>
                                <td align="right" width="360px">
                                    <button type="button" style=" width: 115px;" onclick="window.location.href = 'display'">Back</button>
                                </td>
                            </tr>
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Year / Month : </b>
                                    <input style="width:100px;" type="text" placeholder="เช่น 201906" name="ym" id="ym" value="${ym}" maxlength="6">
                                </td>
                                <td align="right"></td>
                                <td align="right" width="360px">
                                    <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'print'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                                        <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #0b8c00"></i>
                                        <i id="ic2" class="fa fa-print fa-stack-1x fa-inverse"></i>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <form id="formData" name="formData" action="edit" method="post">
                        <input type="hidden" id="ym" name="ym" value="${ym}">
                        <input type="hidden" id="imc" name="imc" value="${imc}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr id="dont">
                                    <th style="text-align: center;">CCY</th>
                                    <th style="text-align: center;">MACHINE IMPORT AMOUNT</th>
                                    <th style="text-align: center;">MATERIALS IMPORT AMOUNT</th>
                                    <th style="text-align: center;">TOTAL IMPORT AMOUNT</th>
                                    <th style="text-align: center;">EXCHANGE RATE CUSTOMS</th>
                                    <th style="text-align: center;">TOTAL IMPORT AMOUNT(฿)</th>
                                    <th style="text-align: center;">BALANCE AMOUNT (${pym})</th>
                                    <th style="text-align: center;">ACCUMULATE INVOICE AMOUNT</th>
                                    <th style="text-align: center;">(ALL BANK) BALANCE PAYMENT (${pym})</th>
                                    <th style="text-align: center;">SUMITOMO</th>
                                    <th style="text-align: center;">HSB</th>
                                    <th style="text-align: center;">SCB</th>
                                    <th style="text-align: center;">BBL</th>
                                    <th style="text-align: center;">MIZUHO</th>
                                    <th style="text-align: center;">KRUNGSRI-TOKYO</th>
                                    <th style="text-align: center;">TOTAL</th>
                                    <th style="text-align: center;">NO PAYMENT AMOUNT</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr id="dont">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${rp700List}" var="x">
                                    <tr>
                                        <td>
                                            <div id="ccy-${x.ccy}">${x.ccy}</div>
                                            <input type="hidden" name="ccy" value="${x.ccy}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="macImpAmt-${x.ccy}"><fmt:formatNumber value="${x.macImpAmt}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="macImpAmt" value="${x.macImpAmt}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="matImpAmt-${x.ccy}"><fmt:formatNumber value="${x.matImpAmt}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="matImpAmt" value="${x.matImpAmt}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="totalImpAmt-${x.ccy}"><fmt:formatNumber value="${x.totalImpAmt}" type="currency" currencySymbol=""/></div>
                                            <input id="input-totalImpAmt-${x.ccy}" type="hidden" name="totalImpAmt" value="${x.totalImpAmt}">
                                        </td>
                                        <td>
                                            <input id="rate-${x.ccy}" type="text" name="rate" style="text-align: right;" onchange="CalRate(this, '${x.ccy}');" value="${x.rate}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="totalImpAmtB-${x.ccy}"><fmt:formatNumber value="${x.totalImpAmtB}" type="currency" currencySymbol=""/></div>
                                            <input id="input-totalImpAmtB-${x.ccy}" type="hidden" name="totalImpAmtB" value="${x.totalImpAmtB}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="balAmtPrevMonth-${x.ccy}"><fmt:formatNumber value="${x.balAmtPrevMonth}" type="currency" currencySymbol=""/></div>
                                            <input id="input-balAmtPrevMonth-${x.ccy}" type="hidden" name="balAmtPrevMonth" value="${x.balAmtPrevMonth}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="accInvAmt-${x.ccy}"><fmt:formatNumber value="${x.accInvAmt}" type="currency" currencySymbol=""/></div>
                                            <input id="input-accInvAmt-${x.ccy}" type="hidden" name="accInvAmt" value="${x.accInvAmt}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="allBankBalPayment-${x.ccy}"><fmt:formatNumber value="${x.allBankBalPayment}" type="currency" currencySymbol=""/></div>
                                            <input id="input-allBankBalPayment-${x.ccy}" type="hidden" name="allBankBalPayment" value="${x.allBankBalPayment}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="sumitomo-${x.ccy}"><fmt:formatNumber value="${x.sumitomo}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="sumitomo" value="${x.sumitomo}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="hsb-${x.ccy}"><fmt:formatNumber value="${x.hsb}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="hsb" value="${x.hsb}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="scb-${x.ccy}"><fmt:formatNumber value="${x.scb}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="scb" value="${x.scb}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="bbl-${x.ccy}"><fmt:formatNumber value="${x.bbl}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="bbl" value="${x.bbl}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="mizuho-${x.ccy}"><fmt:formatNumber value="${x.mizuho}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="mizuho" value="${x.mizuho}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="k_tokyo-${x.ccy}"><fmt:formatNumber value="${x.k_tokyo}" type="currency" currencySymbol=""/></div>
                                            <input type="hidden" name="k_tokyo" value="${x.k_tokyo}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="total-${x.ccy}"><fmt:formatNumber value="${x.total}" type="currency" currencySymbol=""/></div>
                                            <input id="input-total-${x.ccy}" type="hidden" name="total" value="${x.total}">
                                        </td>
                                        <td style="text-align: right;">
                                            <div id="noPaymentAmt-${x.ccy}"><fmt:formatNumber value="${x.noPaymentAmt}" type="currency" currencySymbol=""/></div>
                                            <input id="input-noPaymentAmt-${x.ccy}" type="hidden" name="noPaymentAmt" value="${x.noPaymentAmt}">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>   
                    <table style=" width: 100%;">
                        <tr style=" width: 100%;">
                            <th style=" width: 30%; text-align: right;">TOTAL IMPORT AMOUNT (BAHT)</th>
                            <th style="width: 9%; text-align: right;">
                                <div id="sumTIAB">
                                    <fmt:formatNumber value="${tiab}" type="currency" currencySymbol=""/>
                                </div>
                            </th>
                            <th></th>
                        </tr>
                    </table>
                    <center>
                        <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'edit'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                            <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf"></i>
                            <i id="ic" class="fa fa-save fa-stack-1x fa-inverse"></i>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'print'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                            <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #0b8c00"></i>
                            <i id="ic2" class="fa fa-print fa-stack-1x fa-inverse"></i>
                        </span>
                    </center>
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
            <!--            <div class="header" id="myHeader">
                            <table style="width:100%; border-bottom: 1px solid black;">
                                <tr id="dont">
                                    <th style="text-align: center; width: 100px;">CCY</th>
                                    <th style="text-align: center; width: 100px;">MACHINE IMPORT AMOUNT</th>
                                    <th style="text-align: center; width: 100px;">MATERIALS IMPORT AMOUNT</th>
                                    <th style="text-align: center; width: 100px;">TOTAL IMPORT AMOUNT</th>
                                    <th style="text-align: center; width: 100px;">EXCHANGE RATE CUSTOMS</th>
                                    <th style="text-align: center; width: 100px;">TOTAL IMPORT AMOUNT(฿)</th>
                                    <th style="text-align: center; width: 100px;">BALANCE AMOUNT (${pym})</th>
                                    <th style="text-align: center; width: 100px;">ACCUMULATE INVOICE AMOUNT</th>
                                    <th style="text-align: center; width: 100px;">(ALL BANK) BALANCE PAYMENT (${pym})</th>
                                    <th style="text-align: center; width: 100px;">SUMITOMO</th>
                                    <th style="text-align: center; width: 100px;">HSB</th>
                                    <th style="text-align: center; width: 100px;">SCB</th>
                                    <th style="text-align: center; width: 100px;">BBL</th>
                                    <th style="text-align: center; width: 100px;">MIZUHO</th>
                                    <th style="text-align: center; width: 100px;">KRUNGSRI-TOKYO</th>
                                    <th style="text-align: center; width: 100px;">TOTAL</th>
                                    <th style="text-align: center; width: 100px;">NO PAYMENT AMOUNT</th>
                                </tr>
                            </table>
                        </div>-->
        </div> <!-- end #wrapper -->
        <script>
            window.onscroll = function () {
                myFunction();
            };

            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;

            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        </script>
    </script>
</body>
</html>