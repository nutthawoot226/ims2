<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            i[id=ic]:hover {
                background-color: #042987;
                border-radius: 15px;
            }
        </style>
        <script>
            function ReCreate() {
//                var ym = document.getElementById('ym');
//                if (parseInt(ym.value) >= 201906) {
                document.getElementById('reload').style.display = '';
                document.getElementById('res').style.display = 'none';
                document.getElementById('frm').submit();
//                }
            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            Array.prototype.remove = function () {
                var what, a = arguments, L = a.length, ax;
                while (L && this.length) {
                    what = a[--L];
                    while ((ax = this.indexOf(what)) !== -1) {
                        this.splice(ax, 1);
                    }
                }
                return this;
            };

            var aList = [];

            function ToSelect() {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'avaiCheck') {
                            if (input[i].checked === true) {
                                var ck = '<label style="cursor: pointer;">'
                                        + '<input type="checkbox" name="selectCheck" value="' + input[i].value + '" style="width: 15px; height: 15px;">  '
                                        + input[i].value
                                        + '</label><br>';

                                if (!aList.includes(input[i].value)) {
                                    aList.push(input[i].value);
                                    document.getElementById('sinv').innerHTML += ck;
                                }
                            }
                        }
                    }
                }

                var checkboxes = document.getElementsByTagName('input');
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox') {
                        if (checkboxes[i].name === 'avaiCheck') {
                            checkboxes[i].checked = false;
                        }
                    }
                }

            }

            function ToAvai() {
                var input = document.getElementsByTagName('input');
                var sinv = document.getElementById('sinv').innerHTML;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            if (input[i].checked === true) {
                                var ck = '<label style="cursor: pointer;">'
                                        + '<input type="checkbox" name="selectCheck" value="' + input[i].value + '" style="width: 15px; height: 15px;">  '
                                        + input[i].value
                                        + '</label><br>';

                                aList.remove(input[i].value);
                                sinv = sinv.toString().replace(ck, '');
                            }
                        }
                    }
                }

                document.getElementById('sinv').innerHTML = sinv;

            }

            function ToAvaiAll() {
                document.getElementById('sinv').innerHTML = '';
                aList = [];

            }

            function AllSelect() {
                var input = document.getElementsByTagName('input');
                var isNotEmpty = false;
                document.getElementById('sinv2').innerHTML = document.getElementById('sinv').innerHTML;
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type === 'checkbox') {
                        if (input[i].name === 'selectCheck') {
                            input[i].checked = true;
                            isNotEmpty = true;
                        }
                    }
                }

                if (isNotEmpty) {
                    document.getElementById('reload').style.display = '';
                    document.getElementById('res').style.display = 'none';
                    document.getElementById('frm').submit();
                }
            }
        </script>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'avaiCheck') {
                                checkboxes[i].checked = true;
                            }
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'avaiCheck') {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }
        </script>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMS790.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <div id="sinv2" style=" display: none;">

                        </div>
                        <table width="100%">
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Import Company : </b>
                                    <select style="width:400px;" name="imc" id="imc">
                                        <option value="${imc}" selected hidden>${imc} : ${imcName}</option>
                                        <c:forEach var="x" items="${imcList}">
                                            <option value="${x.code}">${x.code} : ${x.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td align="right"></td>
                                <td align="right" width="360px"></td>
                            </tr>
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Year / Month : </b>
                                    <input style="width:100px;" type="text" placeholder="เช่น 201906" name="ym" id="ym" value="${ym}" maxlength="6">
                                    -
                                    <input style="width:100px;" type="text" placeholder="เช่น 201907" name="ym2" id="ym2" value="${ym2}" maxlength="6">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('myModal-add').style.display = 'block';" style=" cursor: pointer;">
                                        <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf;"></i>
                                        <i id="ic" class="fa fa-refresh fa-stack-1x fa-inverse"></i>
                                    </span>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <span class="fa-stack fa-lg fa-2x" onclick="window.location.href = '/IMS2/IMS790/edit?ym=' + document.getElementById('ym').value + '&ym2=' + document.getElementById('ym2').value + '&imc=' + document.getElementById('imc').value;" style=" cursor: pointer;">
                                        <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf"></i>
                                        <i id="ic" class="fa fa-eye fa-stack-1x fa-inverse"></i>
                                    </span>
                                </td>
                                <td align="right"></td>
                                <td align="right" width="360px"></td>
                            </tr>
                        </table>
                    </form>
                    <center>
                        <div id="res">${result}</div>
                        <i id="reload" class="fa fa-refresh fa-spin fa-5x fa-fw" style=" font-size: 500px; color: #042987; display: none;"></i>
                    </center>
                    <br><br><br>
                    <div id="myModal-3" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Select Product !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="document.getElementById('myModal-3').style.display = 'none'; ReCreate();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-add" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 600px; width: 1000px;">
                            <span id="span-add" class="close" onclick="document.getElementById('myModal-add').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr style=" background-color: white;">
                                    <td align="center">
                                        <b style="color: #00399b;">
                                            <font size="5">Select Product</font><hr>
                                            <table style=" width: 80%;">
                                                <tr>
                                                    <td style=" text-align: center;">
                                                        <font size="4">Available Product</font>
                                                    </td>
                                                    <td></td>
                                                    <td style=" text-align: center;">
                                                        <font size="4">Selected Product</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=" text-align: center;">
                                                    </td>
                                                    <td style=" opacity: 0;">XXX</td>
                                                    <td style=" text-align: center;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=" text-align: left; width: 40%;">
                                                        <div id="ainv" style=" padding-left: 20px; padding-top: 20px; border: 1px solid #ccc; border-radius: 5px; width: 100%; height: 300px; overflow: auto;">
                                                            <label style="cursor: pointer;">
                                                                <input type="checkbox" name="selectAll" onchange="checkAll(this);" style="width: 15px; height: 15px;">
                                                                All
                                                            </label>
                                                            <br>
                                                            <label style="cursor: pointer;">
                                                                <input type="checkbox" name="avaiCheck" value="" style="width: 15px; height: 15px;">
                                                                : Blank
                                                            </label>
                                                            <br>
                                                            <c:forEach items="${pdList}" var="p" varStatus="i">
                                                                <label style="cursor: pointer;">
                                                                    <input type="checkbox" name="avaiCheck" value="${p.code}" style="width: 15px; height: 15px;">
                                                                    ${p.code} : ${p.name}
                                                                </label>
                                                                <br>
                                                            </c:forEach>
                                                        </div>
                                                    </td>
                                                    <td style=" text-align: center;">
                                                        <br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToSelect();"><i class="fa fa-angle-right" style="font-size:20px;"></i></button><br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToAvai();"><i class="fa fa-angle-left" style="font-size:20px;"></i></button><br><br>
                                                        <button class="btn btn btn-outline btn-info" onclick="ToAvaiAll();"><i class="fa fa-angle-double-left" style="font-size:20px;"></i></button>
                                                    </td>
                                                    <td style=" text-align: left; width: 40%;">
                                                        <form id="add-inv" action="create" method="post">
                                                            <div id="sinv" style=" padding-left: 20px; padding-top: 20px; border: 1px solid #ccc; border-radius: 5px; width: 100%; height: 300px; overflow: auto;">

                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </table>
                                        </b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-add').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="AllSelect();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </script>
</body>
</html>